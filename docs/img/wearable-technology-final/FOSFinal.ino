/**
  3: B - Motor 1 (C->B)
  4: C - Motor 2 (B->C)
  6: B - BPM LEDs (C->B) + GSR LEDs
  7: C - GSR LEDs
  9: C - Breathing LEDs
  11: C - BPM
  13: C - BPM
  A0: C - Breath Pressure
  A1: C - GSR 1
  A2: C - Button 1
  A3: B - Button 2
  A6: B - GSR 2
**/

#include <SparkFun_Bio_Sensor_Hub_Library.h>
#include <Wire.h>

uint8_t resPin = 11;
uint8_t mfioPin = 13;
SparkFun_Bio_Sensor_Hub bioHub(resPin, mfioPin);
uint8_t failureMax = 80;
uint8_t failureCount = 0;

bioData body;

#include <FastLED.h>
FASTLED_USING_NAMESPACE
#define PRESSURE_LED_PIN    9
#define B_BPM_GSR_LED_PIN 6
#define GSR_LED_PIN 7
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB
#define PRESSURE_NUM_LEDS    8
#define B_BPM_GSR_NUM_LEDS 3
#define GSR_NUM_LEDS 3
CRGB pressureLEDs[PRESSURE_NUM_LEDS];
CRGB B_BPMLEDs[B_BPM_GSR_NUM_LEDS];
CRGB GSRLEDs[GSR_NUM_LEDS];
#define BRIGHTNESS          30
#define FRAMES_PER_SECOND  120
#define PRESSURE_PIN 0
#define GSR_PIN A1
#define BUTTON_PIN A2
#define B_BUTTON_PIN A3
#define B_MOTOR_PIN 3
#define MOTOR_PIN2 4
#define B_GSR_PIN A6
long currentMillis = 0;

int pressure;
int minPressure;
int maxPressure;
byte rainbowSkip;
byte rainbowLevel;
long pPrevMillis = 0;
long pPrevLEDMillis = 0;
uint8_t pInterval = 50;
uint8_t pDuration = 50;

int buttonReading;
bool buttonFlag = false;

int B_buttonReading;
bool B_buttonFlag = false;

int bpm;
uint8_t startingBPM;
float bpmAvg;
float bDuration;
long bPrevMillis = 0;
long bPrevLEDMillis = 0;
uint8_t bInterval = 250;
int bLEDState = -1;

float B_startingBPM;
float B_Duration;
//int B_MotorFlag = -1;
bool B_MotorFlag = false;
long B_PrevMillis = 0;

#include <movingAvg.h>

movingAvg movingBPMAvg(5);

movingAvg rawMGSRAvg (100);
float rawGSRAvg;
uint8_t rGSRInterval = 10;
long rGSRPrevMillis = 0;

movingAvg pastMGSRAvg(60);
float pastGSRAvg;
int pGSRInterval = 1000;
long pGSRPrevMillis = 0;

movingAvg newMGSRAvg(10);
float newGSRAvg;

float zScore;
float stdDev;

int newSenseVal;

movingAvg B_rawMGSRAvg (100);
float B_rawGSRAvg;
uint8_t B_rGSRInterval = 10;
long B_rGSRPrevMillis = 0;

movingAvg B_pastMGSRAvg(60);
float B_pastGSRAvg;
int B_pGSRInterval = 1000;
long B_pGSRPrevMillis = 0;

movingAvg B_newMGSRAvg(10);
float B_newGSRAvg;

float B_zScore;
float B_stdDev;

int B_newSenseVal;

void setup() {
  delay(500);
  Serial.begin(115200);
  randomSeed(analogRead(0));
  startingBPM = random(60, 120);
  B_startingBPM = random(60, 120);
  bpmAvg = startingBPM;
  initializeHeartrateSensor();
  initializePressureLEDs();
  B_initializeBPMLEDs();
  initializeGSRLEDs();
  FastLED.setBrightness(BRIGHTNESS);
  FastLED.show();
  initializePressureSensor();
  initializeMovingBPMAvg();
  initializeGSRAvg();
  B_initializeGSRAvg();
  pinMode (B_MOTOR_PIN, OUTPUT);
  pinMode (MOTOR_PIN2, OUTPUT);
  delay(1000);
}
void loop() {
  currentMillis = millis();
  readPressureAndButtons();
  updatePressureLEDs();
  readBPM();
  B_updateBPMLED();
  updateBPM2Motor();
  readGSR();
  B_readGSR();
  updateGSR();
  B_updateGSR();
}
void initializeHeartrateSensor() {
  Wire.begin();
  int result = bioHub.begin();
  if (result == 0) {
    Serial.println("Sensor started!");
  }
  else {
    Serial.println("Could not communicate with the sensor!!!");
  }
  Serial.println("Configuring sensor...");
  int error = bioHub.configBpm(MODE_ONE); // Configuring just the BPM settings.
  if (error == 0) { // Zero errors!
    Serial.println("Sensor configured.");
  }
  else {
    Serial.println("Error configuring sensor.");
    delay(10);
    //    Serial.print("Error: ");
    //    Serial.println(error);
  }
}

void initializePressureLEDs() {
  FastLED.addLeds< LED_TYPE, PRESSURE_LED_PIN, COLOR_ORDER>(pressureLEDs, PRESSURE_NUM_LEDS).setCorrection(TypicalLEDStrip);
  fill_solid(pressureLEDs, PRESSURE_NUM_LEDS, CHSV(0, 0, 255));
  rainbowSkip = 255 / PRESSURE_NUM_LEDS;
}

void B_initializeBPMLEDs() {
  FastLED.addLeds< LED_TYPE, B_BPM_GSR_LED_PIN, COLOR_ORDER>(B_BPMLEDs, B_BPM_GSR_NUM_LEDS).setCorrection(TypicalLEDStrip);
  fill_solid(B_BPMLEDs, B_BPM_GSR_NUM_LEDS, CHSV(0, 0, 255));
}
void initializeGSRLEDs() {
  FastLED.addLeds< LED_TYPE, GSR_LED_PIN, COLOR_ORDER>(GSRLEDs, GSR_NUM_LEDS).setCorrection(TypicalLEDStrip);
  fill_solid(GSRLEDs, GSR_NUM_LEDS, CHSV(0, 0, 255));
}
void initializePressureSensor() {
  pressure = analogRead(PRESSURE_PIN);
  minPressure = pressure;
  maxPressure = pressure;
  for (uint8_t i = 0; i < 250; ++i) {
    pressure = analogRead(PRESSURE_PIN);
    if (pressure < minPressure && pressure > 0) {
      minPressure = pressure;
    }
    if (pressure > maxPressure) {
      maxPressure = pressure;
    }
    Serial.println(pressure);
    delay(10);
  }
  Serial.print("min: ");
  Serial.println(minPressure);
  Serial.print("max: ");
  Serial.println(maxPressure);
}

void initializeMovingBPMAvg() {
  movingBPMAvg.begin();
  movingBPMAvg.reading(startingBPM);
}
void initializeGSRAvg() {
  rawMGSRAvg.begin();
  pastMGSRAvg.begin();
  newMGSRAvg.begin();
  newSenseVal = analogRead(GSR_PIN);
  rawGSRAvg = rawMGSRAvg.reading(newSenseVal);
  pastGSRAvg = pastMGSRAvg.reading(newSenseVal);
  newGSRAvg = newMGSRAvg.reading(newSenseVal);
}
void B_initializeGSRAvg() {
  B_rawMGSRAvg.begin();
  B_pastMGSRAvg.begin();
  B_newMGSRAvg.begin();
  B_newSenseVal = analogRead(B_GSR_PIN);
  B_rawGSRAvg = B_rawMGSRAvg.reading(B_newSenseVal);
  B_pastGSRAvg = B_pastMGSRAvg.reading(B_newSenseVal);
  B_newGSRAvg = B_newMGSRAvg.reading(B_newSenseVal);
}
//void readPressure() {
//  if (currentMillis - pPrevMillis >= pInterval) {
//    pressure = analogRead(PRESSURE_PIN);
//    pPrevMillis += pInterval;
//    pressure = constrain (pressure, minPressure, maxPressure);
//    rainbowLevel = map(pressure, minPressure, maxPressure, 0, PRESSURE_NUM_LEDS + 1);
//  }
//}

void readPressureAndButtons() {
  if (currentMillis - pPrevMillis >= pInterval) {
    pressure = analogRead(PRESSURE_PIN);
    buttonReading = analogRead(BUTTON_PIN);
    B_buttonReading = analogRead(B_BUTTON_PIN);
    if (buttonReading > 800) {
      buttonFlag = true;
      //      Serial.print("BUTTON: ");
      //      Serial.println(buttonFlag);
    }
    else {
      buttonFlag = false;
    }
    if (B_buttonReading > 1000) {
      B_buttonFlag = true;
      //      Serial.print("BRACELET BUTTON: ");
      //      Serial.println(B_buttonFlag);
    }
    else {
      B_buttonFlag = false;
    }
    pPrevMillis += pInterval;
    pressure = constrain (pressure, minPressure, maxPressure);
    rainbowLevel = map(pressure, minPressure, maxPressure, 0, PRESSURE_NUM_LEDS + 1);
  }
}

void updatePressureLEDs() {
  if (currentMillis - pPrevLEDMillis >= pDuration) {
    for (uint8_t i = 0; i < PRESSURE_NUM_LEDS; ++i) {
      if (i < rainbowLevel) {
        pressureLEDs[i] = CHSV(i * rainbowSkip, 255, 255);
      }
      else {
        pressureLEDs[i] = CHSV(0, 0, 0);
      }
    }
    FastLED.show();
  }
}

void readBPM() {
  if (currentMillis - bPrevMillis >= bInterval) {
    body = bioHub.readBpm();
    bpm = body.heartRate;
    bPrevMillis += bInterval;
    if (bpm != 0) {
      bpmAvg = movingBPMAvg.reading(bpm);
    }
    Serial.print("BPM: ");
    Serial.println(bpm);
    Serial.print("BPMAvg: ");
    Serial.println(bpmAvg);
    Serial.print("Status: ");
    Serial.println(body.status);
    //    if (body.status < 1 || body.status > 3) {
    //      failureCount++;
    //      if (failureCount >= failureMax) {
    //        Serial.println("FAILED");
    //        failureCount = 0;
    //        Wire.begin();
    //        int result = bioHub.begin();
    //        if (re sult == 0) // Zero errors!
    //          Serial.println("Sensor started!");
    //        //  else
    //        Serial.println("Could not communicate with the sensor!!!");
    //
    //        Serial.println("Configuring Sensor....");
    //        int error = bioHub.configBpm(MODE_ONE); // Configuring just the BPM settings.
    //        if (error == 0) { // Zero errors!
    //          Serial.println("Sensor configured.");
    //        }
    //        else {
    //          Serial.println("Error configuring sensor.");
    //          Serial.print("Error: ");
    //          Serial.println(error);
    //        }
    //      }
    //    }
  }
}

void B_updateBPMLED() {
  bDuration = 60 / bpmAvg * 1000;
  if (bLEDState == -1) {
    if (currentMillis - bPrevLEDMillis >= bDuration) {
      bLEDState = 1;
      B_BPMLEDs[0] = CHSV(0, 0, 0);
      B_BPMLEDs[2] = CHSV(0, 0, 0);
      // fill_solid(BPMLEDs, B_BPM_GSR_NUM_LEDS, CHSV(0, 0, 0));
      digitalWrite(B_MOTOR_PIN, LOW);
      bPrevLEDMillis += bDuration;
    }
  }
  else if (bLEDState == 1) {
    if (currentMillis - bPrevLEDMillis >= bDuration) {
      bLEDState = -1;
      B_BPMLEDs[0] = CHSV(0, 255, 255);
      B_BPMLEDs[2] = CHSV(0, 255, 255);
      // fill_solid(BPMLEDs, B_BPM_GSR_NUM_LEDS, CHSV(0, 255, 255));
      digitalWrite(B_MOTOR_PIN, HIGH);
      bPrevLEDMillis += bDuration;
    }
  }
  FastLED.show();
}

void updateBPM2Motor() {
  B_Duration = 60 / B_startingBPM * 1000;
  if (currentMillis - B_PrevMillis >= B_Duration) {
    if (B_MotorFlag == false) {
      //    if (B_MotorFlag == -1) {
      digitalWrite(MOTOR_PIN2, LOW);
      //      Serial.println(digitalRead(MOTOR_PIN2));
      //      B_MotorFlag *= -1;
    }
    else {
      //    else if (B_MotorFlag == 1) {
      //     Serial.print("BPM2: ");
      //     Serial.println(B_startingBPM);
      digitalWrite(MOTOR_PIN2, HIGH);
      //      Serial.println(digitalRead(MOTOR_PIN2));
      //      B_MotorFlag *= -1;
    }
    Serial.println(digitalRead(MOTOR_PIN2));
    //    B_MotorFlag *= -1;
    B_MotorFlag = !B_MotorFlag;
    B_PrevMillis += B_Duration;
  }
}

void readGSR() {
  if (currentMillis - rGSRPrevMillis >= rGSRInterval) {
    newSenseVal = analogRead(GSR_PIN);
    rawMGSRAvg.reading(newSenseVal);
    newMGSRAvg.reading(newSenseVal);
    rGSRPrevMillis += rGSRInterval;
  }
}

void B_readGSR() {
  if (currentMillis - B_rGSRPrevMillis >= B_rGSRInterval) {
    B_newSenseVal = analogRead(B_GSR_PIN);
    B_rawMGSRAvg.reading(B_newSenseVal);
    B_newMGSRAvg.reading(B_newSenseVal);
    B_rGSRPrevMillis += B_rGSRInterval;
  }
}

void updateGSR() {
  if (currentMillis - pGSRPrevMillis >= pGSRInterval) {
    pastGSRAvg = pastMGSRAvg.reading(rawMGSRAvg.getAvg());
    //    Serial.print("PASTGSRAVG: ");
    //    Serial.println(pastGSRAvg);
    float sqDevSum = 0.0;
    int* s_val = pastMGSRAvg.getReadings();
    //    Serial.print("S_val first: ");
    //    Serial.println(s_val[0]);
    for (uint8_t i = 0; i < pastMGSRAvg.getCount(); ++i) {
      sqDevSum += pow((pastGSRAvg - (s_val[i])), 2);
    }
    //    Serial.println(sqDevSum);
    stdDev = sqrt(sqDevSum / pastMGSRAvg.getCount());
    //    Serial.println(stdDev);
    newGSRAvg = newMGSRAvg.getAvg();
    zScore = (newGSRAvg - pastGSRAvg) / stdDev;
    //    Serial.println(zScore);
    int newZScore = zScore * 100;
    newZScore = constrain(newZScore, -200, 300);
    uint8_t LEDColor = map(newZScore, -200, 300, 0, 255);
    fill_solid(GSRLEDs, GSR_NUM_LEDS, CHSV(LEDColor, 255, 255));
    FastLED.show();
    //    Serial.print("GSR Color: ");
    //    Serial.println(LEDColor);
    pGSRPrevMillis += pGSRInterval;
  }
}

void B_updateGSR() {
  if (currentMillis - B_pGSRPrevMillis >= B_pGSRInterval) {
    B_pastGSRAvg = B_pastMGSRAvg.reading(B_rawMGSRAvg.getAvg());
    float B_sqDevSum = 0.0;
    //    Serial.println(B_sqDevSum);
    int* B_s_val = B_pastMGSRAvg.getReadings();
    for (uint8_t i = 0; i < B_pastMGSRAvg.getCount(); ++i) {
      B_sqDevSum += pow((B_pastGSRAvg - (B_s_val[i])), 2);
    }
    B_stdDev = sqrt(B_sqDevSum / B_pastMGSRAvg.getCount());
    B_newGSRAvg = B_newMGSRAvg.getAvg();

    B_zScore = (B_newGSRAvg - B_pastGSRAvg) / B_stdDev;

    float B_newZScore = B_zScore * 100;
    B_newZScore = constrain(B_newZScore, -200, 300);
    uint8_t B_LEDColor = map(B_newZScore, -200, 300, 0, 255);
    B_BPMLEDs[1] = CHSV(B_LEDColor, 255, 255);
    // fill_solid(GSRLEDs, GSR_NUM_LEDS, CHSV(B_LEDColor, 255, 255));
    FastLED.show();
    //    Serial.print("BRACELET GSR Color: ");
    //    Serial.println(B_LEDColor);
    B_pGSRPrevMillis += B_pGSRInterval;
  }
}
