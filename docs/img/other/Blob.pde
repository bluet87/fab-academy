color rectColor = color (255, 255, 0);

class Blob {

  //bounding rectangle for blob - there'll be one for head and one for body, I think? Maybe a person Class that includes two blobs, one for head and one for lower
  float minX, minY, maxX, maxY, centerX, centerY;

  PVector center;

  ArrayList<PVector> points;

  float bubbleSize;

  int id;

  boolean taken = false;

  int lifespan = maxLife;
  
  int bubbleChangeSpeed = 3;
  color bubbleColor = 255;
  float bubbleSizeX;
  float bubbleSizeY;
  float bubbleSizeMax;
  float bubbleSizeMaxX;
  float bubbleSizeMaxY;
  
  //creating a new blob
  Blob(float x, float y) {
    minX = x;
    minY = y;
    maxX = x;
    maxY = y;

    //calculates center
    centerX = (maxX-minX)/2+minX;
    centerY = (maxY-minY)/2+minY;
    center = new PVector(centerX, centerY);

    //starts the point cloud
    //points = new ArrayList<PVector>();

    ////adds the new point to the point cloud
    //points.add(new PVector(x, y));
    bubbleSize = 75;
    bubbleColor=color (random(255), random(255), random(255), 128);

  }


  void show() {
    stroke (0);
    strokeWeight(2);
    noStroke();
    float cX = (maxX-minX)/2+minX; //bizarre thing where centerX != cX, but not in the stored blob data?
    float cY = (maxY-minY)/2+minY; 

    //draws the bubble, bubble is the size of the largest 'side' of the rect
    fill (bubbleColor);
    //bubbleSizeMax = max(maxX-minX, maxY-minY);
    bubbleSizeMaxX = maxX-minX;
    bubbleSizeMaxY = maxY-minY;
    if (bubbleSizeMaxX > bubbleSizeX) {
      bubbleSizeX+=bubbleChangeSpeed;
    } else if (bubbleSizeMaxX<bubbleSizeX) {
      bubbleSizeX-=bubbleChangeSpeed;
    }
    if (bubbleSizeMaxY > bubbleSizeY) {
      bubbleSizeY+=bubbleChangeSpeed;
    } else if (bubbleSizeMaxY<bubbleSizeY) {
      bubbleSizeY-=bubbleChangeSpeed;
    }
    //ellipse(cX, cY, bubbleSize, bubbleSize);
    ellipse(cX,cY,bubbleSizeX,bubbleSizeY);
    //draws the person marker on top of the bubble
    fill (230);
    ellipse(cX, cY, 30, 30);

    textAlign(CENTER);
    textSize(15);
    fill(50);
    text(id, cX, cY);
    text(lifespan, cX, cY-15);
  }

  boolean checkLife() {
    lifespan--;
    if (lifespan<0) {
      return true;
    } else {
      return false;
    }
  }

  PVector getCenter() {
    float x = (maxX - minX)* 0.5 + minX;
    float y = (maxY - minY)* 0.5 + minY;    
    return new PVector(x, y);
  }
  //adds the new point to the cloud
  void add(float x, float y) {
    //points.add(new PVector(x, y));
    minX = min(minX, x);
    minY = min(minY, y);
    maxX = max(maxX, x);
    maxY = max(maxY, y);
  }

  //returns area of the bounding rect.
  float size() {
    return (maxX-minX)*(maxY-minY);
  }

  boolean isNear(float x, float y) {
    float d = 100000000;

    //not doing this bc it's hella slow
    /*
    //for every point in the cloud
     for (PVector v : points) {
     
     //distance between given existing point and the new point
     float tempD=distSq(x, y, v.x, v.y);
     
     //if the new distance is smaller than the prev. distance between the prev. points, take the new distance
     //aka the new point is [this smaller distance] from a given point in the cloud
     if (tempD < d) {
     d = tempD;
     }
     }
     */

    //using the rectangle 'clamping' strategy
    //https://github.com/CodingTrain/website/blob/master/Tutorials/Processing/11_video/sketch_11_8_BlobTracking_improved/Blob.pde
    float xC = max(min(x, maxX), minX);
    float yC = max(min(y, maxY), minY);
    d = distSq(xC, yC, x, y);
    if (d < distThreshold * distThreshold) {
      return true;
    } else {
      return false;
    }
  }

  void become(Blob other) {
    minX = other.minX;
    minY = other.minY;
    maxX = other.maxX;
    maxY = other.maxY;
    lifespan = maxLife;
  }
}
