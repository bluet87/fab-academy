import org.openkinect.freenect.*;
import org.openkinect.processing.*;

//declare the kinect
Kinect kinect;

//Depth image
PImage depthImg;

//thresholds
int top = 60;
int bottom = 860; //head only for now, move to 860 to include arms

int blobCounter = 0;

float threshold = 25;
float distThreshold = 25;

ArrayList<Blob> blobs = new ArrayList<Blob>();

int maxLife = 6;

float blobMinSize = 800;

void setup() {
  size(1280, 480);
  kinect = new Kinect(this);
  kinect.initDepth();
  depthImg = new PImage(kinect.width, kinect.height);
}
int headLow = 245;
int headHigh = 255;

void draw() {

  //preps list to store visible blobs
  ArrayList<Blob> currentBlobs = new ArrayList<Blob>();

  //processes raw image into depth thresholded image
  int[] rawDepth = kinect.getRawDepth();
  for (int x = 0; x < kinect.width; x++) {
    for (int y = 0; y < kinect.height; y++) {
      int offset = x+y*kinect.width;
      int d = rawDepth[offset];
      if (d >= top && d<= bottom && x > 200) {
        float c = map(d, top, bottom, 100, 255);
        depthImg.pixels[offset]=color(c);
      } else {
        depthImg.pixels[offset]=color(0);
      }
    }
  }

  depthImg.updatePixels();
  ///now we have a thresholded image

  for (int x = 0; x < depthImg.width; x++) {
    for (int y = 0; y < depthImg.height; y++) {

      //location of current pixel
      int loc = x + y * depthImg.width; 

      //color of current pixel
      color d = depthImg.pixels[loc];

      //translating color to grayscale
      int r=(d&0x00FF0000)>>16; // red part
      int g=(d&0x0000FF00)>>8; // green part
      int blue=(d&0x000000FF); // blue part
      int grey=(r+g+blue)/3; 

      //checking if the gray color is within the threshold for the blob color
      if (grey>=headLow && grey <= headHigh) {

        boolean found = false;

        //walk through existing blobs
        for (Blob b : currentBlobs) {

          //if a blob is near to the current point
          if (b.isNear(x, y)) {

            //add point to the blob
            b.add(x, y);
            found = true;
            break;
          }
        }

        //if this is new
        if (!found) {

          //starts new blob at point x,y
          Blob b = new Blob(x, y);

          //adds blob to the list of existing blobs
          currentBlobs.add(b);
        }
      }
      //we've checked the pixel, accounted for it, and can move onto the next pixel
    }
  }



  for (int i = currentBlobs.size()-1; i >= 0; i--) {
    if (currentBlobs.get(i).size() < blobMinSize) {
      currentBlobs.remove(i);
    }
  }

  // There are no blobs!
  if (blobs.isEmpty() && currentBlobs.size() > 0) {
    for (Blob b : currentBlobs) {
      b.id = blobCounter;
      blobs.add(b);
      blobCounter++;
    }
  } else if (blobs.size() <= currentBlobs.size()) {
    
    // Match whatever blobs you can match
    for (Blob b : blobs) {
      float recordD = 1000;
      Blob matched = null;
      for (Blob cb : currentBlobs) {
        PVector centerB = b.getCenter();
        PVector centerCB = cb.getCenter();         
        float d = PVector.dist(centerB, centerCB);
        if (d < recordD && !cb.taken) {
          recordD = d; 
          matched = cb;
        }
      }
      matched.taken = true;
      b.become(matched);
    }

    // Whatever is leftover make new blobs
    for (Blob b : currentBlobs) {
      if (!b.taken) {
        b.id = blobCounter;
        blobs.add(b);
        blobCounter++;
      }
    }
  } else if (blobs.size() > currentBlobs.size()) {
    for (Blob b : blobs) {
      b.taken = false;
    }


    // Match whatever blobs you can match
    for (Blob cb : currentBlobs) {
      float recordD = 1000;
      Blob matched = null;
      for (Blob b : blobs) {
        PVector centerB = b.getCenter();
        PVector centerCB = cb.getCenter();         
        float d = PVector.dist(centerB, centerCB);
        if (d < recordD && !b.taken) {
          recordD = d; 
          matched = b;
        }
      }
      if (matched != null) {
        matched.taken = true;
        matched.become(cb);
      }
    }

    for (int i = blobs.size() - 1; i >= 0; i--) {
      Blob b = blobs.get(i);
      if (!b.taken) {
        if (b.checkLife()) {
          blobs.remove(i);
        }
      }
    }
  }
  background(0);
  image(depthImg, 0, 0, 640, 480);
  filter(INVERT);
  displayBlobs(blobs);
  translate(640,0);
  displayBlobs(blobs);
  delay(50);
}

void displayBlobs(ArrayList<Blob> currentBlobs) {
  //displays remaining blobs
  for (Blob b : currentBlobs) {

    b.show();
  }
}

float distSq(float x1, float y1, float x2, float y2) {
  float d = (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1);
  return d;
}
