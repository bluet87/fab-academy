#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      bluet87
#
# Created:     16/02/2020
# Copyright:   (c) bluet87 2020
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    pass

if __name__ == '__main__':
    main()
outside = 75.0
inside = 10.0
offset = 2
gap = 2.5
numLines = (outside/2-inside/2)/offset
gapNumLines = int(round(numLines/2))
shortNumLines = int(numLines - gapNumLines)
xGapLeft=[]
xGapLeftMid=[]
xGapRightMid=[]
xGapRight=[]
yGap=[]


xShortLeft=[]
xShortRight=[]
yShort=[]

for i in range(gapNumLines):
    leftX=(2*i-1)*offset
    if leftX < 0:
        leftX = 0
    xGapLeft.append(leftX)
    leftMidX = outside/2-gap/2
    xGapLeftMid.append(leftMidX)
    rightMidX = outside/2+gap/2
    xGapRightMid.append(rightMidX)
    rightX=outside-leftX
    xGapRight.append(rightX)
    y=2*i
    yGap.append(y)

for i in range(shortNumLines):
    leftX=(2*i*offset)+gap
    xShortLeft.append(leftX)
    rightX=outside-leftX
    xShortRight.append(rightX)
    y=2*i+1
    yShort.append(y)

print(xGapLeft)
print(xGapLeftMid)
print(xGapRightMid)
print(xGapRight)
print(yGap)
print
print(xShortLeft)
print(xShortRight)
print(yShort)
