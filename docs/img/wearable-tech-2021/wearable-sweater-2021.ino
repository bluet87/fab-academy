#include <Bounce.h>

// GUItool: begin automatically generated code

#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

AudioInputI2S            i2s2;           //xy=80,124
AudioEffectDelay         delay1;         //xy=208,292
AudioMixer4              mixer1;         //xy=226,135
AudioEffectGranular      granular1;      //xy=390,248
AudioFilterStateVariable filter1;        //xy=390,508
AudioEffectBitcrusher    bitcrusher1;    //xy=395,436
AudioEffectChorus        chorus1;        //xy=413,352
AudioOutputI2S           i2s1;           //xy=546,576
AudioConnection          patchCord1(i2s2, 0, mixer1, 0);
AudioConnection          patchCord2(delay1, 0, mixer1, 3);
AudioConnection          patchCord3(delay1, 0, granular1, 0);
AudioConnection          patchCord4(mixer1, delay1);
AudioConnection          patchCord5(granular1, chorus1);
AudioConnection          patchCord6(filter1, 0, i2s1, 0);
AudioConnection          patchCord7(filter1, 0, i2s1, 1);
AudioConnection          patchCord8(bitcrusher1, 0, filter1, 0);
AudioConnection          patchCord9(chorus1, bitcrusher1);
// GUItool: end automatically generated code

AudioControlSGTL5000     sgtl5000_1;     //xy=265,212

#define GRANULAR_MEMORY_SIZE 12800  // enough for 290 ms at 44.1 kHz

int16_t granularMemory[GRANULAR_MEMORY_SIZE];

#define CHORUS_DELAY_LENGTH (16*AUDIO_BLOCK_SAMPLES)
short delayline[CHORUS_DELAY_LENGTH];
int n_chorus = 10;

int soundstate = 1;
int switchstate = 1;
int prevLeft = 1;
int prevRight = 3;
int prevSwitch = 1;
int prevSwitch_value = 1;

Bounce button1 = Bounce(1, 5); //POMPOM 1
Bounce button2 = Bounce(3, 5); //POMPOM 2

Bounce Switch = Bounce(4, 5); //STROKE SWITCH

Bounce button3 = Bounce(5, 5); //POMPOM 3
Bounce button4 = Bounce(8, 5); //POMPOM 4

float smoothed_button = 1;
float smooth = 0.01; //how fast is the change

void setup() {
  Serial.begin(9600);
  
  pinMode(1, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  pinMode(8, INPUT_PULLUP);
  pinMode(14, INPUT);

  AudioMemory(324);

  granular1.begin(granularMemory, GRANULAR_MEMORY_SIZE);

  sgtl5000_1.enable();
  sgtl5000_1.volume(0.55); //0.55
  sgtl5000_1.inputSelect(AUDIO_INPUT_MIC);
  sgtl5000_1.micGain(56);

  if (!chorus1.begin(delayline, CHORUS_DELAY_LENGTH, n_chorus)) {
    Serial.println("AudioEffectChorus - left channel begin failed");
    while (1);
  }

  chorus1.voices(0);

  Serial.println("setup done");
  AudioProcessorUsageMaxReset();
  AudioMemoryUsageMaxReset();

}

void loop() {

  button1.update(); // RIGHT HIGH 
  button2.update(); // RIGHT LOW
  Switch.update(); // NORMAL -> STROKE
  button3.update(); // LEFT HIGH
  button4.update(); // LEFT LOW

  int button1_value = button1.read();
  int button2_value = button2.read();
  int Switch_value = Switch.read();
  int button3_value = button3.read();
  int button4_value = button4.read();

  if (Switch_value == 1) {
    switchstate = prevSwitch;
  }
  else { //Switch_value == 0, currently pressed
    if (prevSwitch_value == 1) { //newly pressed

      if (switchstate == 1) {
        switchstate = 2;
        prevSwitch = switchstate;
      }
      else { //still being pressed
        switchstate = 1;
        prevSwitch = switchstate;
      }
    }
  }
  prevSwitch_value = Switch_value;

  if (switchstate == 1) {
    if ((button1_value == 1 && button2_value == 1) || (button1_value == 0 && button2_value == 0)) { //NOBODY TOUCHING !!
      soundstate = prevLeft;
    }
    else if (button1_value == 0) {
      soundstate = 1;
    }
    else if (button2_value == 0) {
      soundstate = 2;
    }
    prevLeft = soundstate;
  }
  else if (switchstate == 2) {
    if ((button3_value == 1 && button4_value == 1) || (button3_value == 0 && button4_value == 0)) { //NOBODY TOUCHING !!
      soundstate = prevRight;
    }
    else if (button3_value == 0) {
      soundstate = 3;
    }
    else if (button4_value == 0) {
      soundstate = 4;
    }
    prevRight = soundstate;
  }

  //DEFINE WHAT HAPPENS IN THE SOUND STATES

  if (soundstate == 1) {
    delay1.delay(0, 400);
    granular1.setSpeed(2.25);
    mixer1.gain(0, 0.7);
    mixer1.gain(3, 0.7);
    chorus1.voices(0);
  }

  if (soundstate == 2) {
    delay1.delay(0, 800);
    granular1.setSpeed(0.75);
    mixer1.gain(0, 0.9);
    mixer1.gain(3, 0.7);
    chorus1.voices(0);
  }

  if (soundstate == 3) {
    granular1.setSpeed(1);
    delay1.delay(0, 600);
    mixer1.gain(0, 1.1);
    mixer1.gain(3, 0.7);
    chorus1.voices(10);
  }

  if (soundstate == 4) {
    granular1.setSpeed(1);
    delay1.delay(0, 600);
    mixer1.gain(0, 0.9);
    mixer1.gain(3, 0.7);
    chorus1.voices(3);
    bitcrusher1.bits(10);
  }

  granular1.beginFreeze(290.0);
  granular1.beginPitchShift(300.0);

  bitcrusher1.bits(16);

  int bend = analogRead(A0);
  bend = map(bend, 800, 1000, 4000, 40);
  bend = constrain(bend, 40, 4000);

  filter1.frequency(bend);

  Serial.print(F("button1: \t"));
  Serial.print(button1_value);
  Serial.print(F("\t button2: \t"));
  Serial.print(button2_value);
  Serial.print(F("\t Switch \t"));
  Serial.print(Switch_value);
  Serial.print(F("\t button3: \t"));
  Serial.print(button3_value);
  Serial.print(F("\t button4: \t"));
  Serial.print(button4_value);
  Serial.print(F("\t soundstate: \t"));
  Serial.println(soundstate);
  delay(50);
}
