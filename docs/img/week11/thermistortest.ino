#define LEDB 10
#define LEDG 7
#define LEDR 6

float R1 = 10000;
float logR2, R2, T;
float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;

int thermistor = 2;
void setup() {
Serial.begin(19200);
pinMode(thermistor, INPUT);
pinMode(LEDR, OUTPUT);
pinMode(LEDG, OUTPUT);
pinMode(LEDB, OUTPUT);
delay(1000);

}
int Vo;
int r = 0;
int g = 45;
int b = 30;

void loop() {
  Vo = analogRead(thermistor);
  R2 = R1 * (1023.0 / (float)Vo - 1.0);
  logR2 = log(R2);
  T = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));
  T = T - 273.15;
  T = (T * 9.0)/ 5.0 + 32.0; 
  Serial.println(T);

  analogWrite(LEDR,r);
  analogWrite(LEDG,g);
  analogWrite(LEDB,b);
  r--;
  g--;
  b--;

  if(r <=0){
    r=255;
  }

  if(g<=0){
    g=255;
  }

  if(b<=0){
    b=255;
  }
  delay(1000);
}
