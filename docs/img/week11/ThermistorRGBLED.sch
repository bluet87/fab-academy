EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L FabAcademy:C C1
U 1 1 5EC8373C
P 5700 2200
F 0 "C1" V 5448 2200 50  0000 C CNN
F 1 "1uF" V 5539 2200 50  0000 C CNN
F 2 "FabAcademy:fab-C1206" H 5738 2050 50  0001 C CNN
F 3 "" H 5700 2200 50  0001 C CNN
	1    5700 2200
	0    1    1    0   
$EndComp
$Comp
L FabAcademy:Thermistor_NTC TH1
U 1 1 5EC84375
P 3800 3950
F 0 "TH1" H 3898 3996 50  0000 L CNN
F 1 "Thermistor_NTC" H 3898 3905 50  0000 L CNN
F 2 "FabAcademy:R_1206" H 3800 4000 50  0001 C CNN
F 3 "https://www.mouser.fi/datasheet/2/18/AAS-920-324F-Thermometrics-NTC-SMD-032717-web-1018802.pdf" H 3800 4000 50  0001 C CNN
	1    3800 3950
	-1   0    0    1   
$EndComp
$Comp
L FabAcademy:R R4
U 1 1 5EC84FCA
P 2300 4000
F 0 "R4" H 2370 4046 50  0000 L CNN
F 1 "10k" H 2370 3955 50  0000 L CNN
F 2 "FabAcademy:R_1206" V 2230 4000 50  0001 C CNN
F 3 "~" H 2300 4000 50  0001 C CNN
	1    2300 4000
	-1   0    0    1   
$EndComp
$Comp
L FabAcademy:R R6
U 1 1 5EC8538E
P 3800 2850
F 0 "R6" H 3870 2896 50  0000 L CNN
F 1 "10k" H 3870 2805 50  0000 L CNN
F 2 "FabAcademy:R_1206" V 3730 2850 50  0001 C CNN
F 3 "~" H 3800 2850 50  0001 C CNN
	1    3800 2850
	-1   0    0    1   
$EndComp
$Comp
L FabAcademy:R R3
U 1 1 5EC85493
P 2300 2900
F 0 "R3" H 2370 2946 50  0000 L CNN
F 1 "10k" H 2370 2855 50  0000 L CNN
F 2 "FabAcademy:R_1206" V 2230 2900 50  0001 C CNN
F 3 "~" H 2300 2900 50  0001 C CNN
	1    2300 2900
	-1   0    0    1   
$EndComp
$Comp
L FabAcademy:R R7
U 1 1 5EC8558E
P 3500 2250
F 0 "R7" V 3293 2250 50  0000 C CNN
F 1 "320" V 3384 2250 50  0000 C CNN
F 2 "FabAcademy:R_1206" V 3430 2250 50  0001 C CNN
F 3 "~" H 3500 2250 50  0001 C CNN
	1    3500 2250
	0    1    1    0   
$EndComp
$Comp
L FabAcademy:LED D2
U 1 1 5EC85A59
P 2300 2250
F 0 "D2" H 2293 1995 50  0000 C CNN
F 1 "PWR_LED" H 2293 2086 50  0000 C CNN
F 2 "FabAcademy:LED_1206" H 2300 2250 50  0001 C CNN
F 3 "https://optoelectronics.liteon.com/upload/download/DS-22-98-0002/LTST-C150CKT.pdf" H 2300 2250 50  0001 C CNN
	1    2300 2250
	-1   0    0    1   
$EndComp
$Comp
L FabAcademy:PINHD-1x06-HEADER M1
U 1 1 5EC86966
P 1950 5600
F 0 "M1" H 1950 5600 45  0001 C CNN
F 1 "PINHD-1x06-HEADER" H 1950 5600 45  0001 C CNN
F 2 "FabAcademy:fab-1X06" H 1980 5750 20  0001 C CNN
F 3 "" H 1950 5600 50  0001 C CNN
	1    1950 5600
	1    0    0    -1  
$EndComp
$Comp
L FabAcademy:PINHD-1x02-HEADER M2
U 1 1 5EC8705F
P 4050 5750
F 0 "M2" H 4050 5750 45  0001 C CNN
F 1 "PINHD-1x02-HEADER" H 4050 5750 45  0001 C CNN
F 2 "FabAcademy:fab-1X02" H 4080 5900 20  0001 C CNN
F 3 "" H 4050 5750 50  0001 C CNN
	1    4050 5750
	1    0    0    -1  
$EndComp
$Comp
L FabAcademy:LED_RGB_CLV1A-FKB D1
U 1 1 5EC8757C
P 7000 5300
F 0 "D1" H 7000 5797 50  0000 C CNN
F 1 "LED_RGB_CLV1A-FKB" H 7000 5706 50  0000 C CNN
F 2 "FabAcademy:LED_RGB_Cree_CLV1A-FKB_3.2x2.8mm" H 7000 5250 50  0001 C CNN
F 3 "https://www.cree.com/led-components/media/documents/ds-CLV1A-FKB.pdf" H 7000 5250 50  0001 C CNN
	1    7000 5300
	1    0    0    -1  
$EndComp
$Comp
L FabAcademy:R R2
U 1 1 5EC882A9
P 5750 5300
F 0 "R2" V 5543 5300 50  0000 C CNN
F 1 "90" V 5634 5300 50  0000 C CNN
F 2 "FabAcademy:R_1206" V 5680 5300 50  0001 C CNN
F 3 "~" H 5750 5300 50  0001 C CNN
	1    5750 5300
	0    1    1    0   
$EndComp
$Comp
L FabAcademy:R R5
U 1 1 5EC88522
P 6300 5500
F 0 "R5" V 6093 5500 50  0000 C CNN
F 1 "90" V 6184 5500 50  0000 C CNN
F 2 "FabAcademy:R_1206" V 6230 5500 50  0001 C CNN
F 3 "~" H 6300 5500 50  0001 C CNN
	1    6300 5500
	0    1    1    0   
$EndComp
$Comp
L FabAcademy:R R1
U 1 1 5EC880A2
P 5200 5100
F 0 "R1" V 4993 5100 50  0000 C CNN
F 1 "150" V 5084 5100 50  0000 C CNN
F 2 "FabAcademy:R_1206" V 5130 5100 50  0001 C CNN
F 3 "~" H 5200 5100 50  0001 C CNN
	1    5200 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	5350 5100 6800 5100
Wire Wire Line
	5900 5300 6800 5300
Wire Wire Line
	6450 5500 6800 5500
Wire Wire Line
	6150 5500 5600 5500
Wire Wire Line
	5600 5300 5050 5300
Wire Wire Line
	5050 5100 4600 5100
Wire Wire Line
	1950 5600 1300 5600
Wire Wire Line
	1950 5500 1300 5500
Wire Wire Line
	1950 5400 1300 5400
Wire Wire Line
	1950 5200 1300 5200
Text Label 1300 5100 0    50   ~ 0
GND
Text Label 1300 5300 0    50   ~ 0
VCC
Text Label 1300 5400 0    50   ~ 0
TX
Text Label 1300 5500 0    50   ~ 0
RX
Wire Wire Line
	1950 5300 1800 5300
Wire Wire Line
	1950 5100 1650 5100
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5EC8D615
P 1800 5300
F 0 "#FLG02" H 1800 5375 50  0001 C CNN
F 1 "PWR_FLAG" H 1800 5473 50  0000 C CNN
F 2 "" H 1800 5300 50  0001 C CNN
F 3 "~" H 1800 5300 50  0001 C CNN
	1    1800 5300
	-1   0    0    1   
$EndComp
Connection ~ 1800 5300
Wire Wire Line
	1800 5300 1300 5300
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5EC8D95D
P 1650 5100
F 0 "#FLG01" H 1650 5175 50  0001 C CNN
F 1 "PWR_FLAG" H 1650 5273 50  0000 C CNN
F 2 "" H 1650 5100 50  0001 C CNN
F 3 "~" H 1650 5100 50  0001 C CNN
	1    1650 5100
	1    0    0    -1  
$EndComp
Connection ~ 1650 5100
Wire Wire Line
	1650 5100 1300 5100
Wire Wire Line
	7200 5300 7800 5300
Text Label 7800 5300 2    50   ~ 0
GND
Wire Wire Line
	4050 5250 3550 5250
Wire Wire Line
	4050 5350 3550 5350
Text Label 3550 5250 0    50   ~ 0
UPDI
Text Label 3550 5350 0    50   ~ 0
GND
NoConn ~ 1300 5200
NoConn ~ 1300 5600
Text Label 4600 5100 0    50   ~ 0
LED_R
Text Label 5050 5300 0    50   ~ 0
LED_G
Text Label 5600 5500 0    50   ~ 0
LED_B
Wire Wire Line
	3800 4100 3800 4400
Wire Wire Line
	3800 4400 3000 4400
Wire Wire Line
	2300 4400 2300 4150
Wire Wire Line
	2300 2750 2300 2650
Wire Wire Line
	2300 2650 2950 2650
Wire Wire Line
	3800 2650 3800 2700
$Comp
L power:GND #PWR02
U 1 1 5EC96774
P 3000 4400
F 0 "#PWR02" H 3000 4150 50  0001 C CNN
F 1 "GND" H 3005 4227 50  0000 C CNN
F 2 "" H 3000 4400 50  0001 C CNN
F 3 "" H 3000 4400 50  0001 C CNN
	1    3000 4400
	1    0    0    -1  
$EndComp
Connection ~ 3000 4400
Wire Wire Line
	3000 4400 2300 4400
$Comp
L power:VCC #PWR01
U 1 1 5EC969EF
P 2950 2650
F 0 "#PWR01" H 2950 2500 50  0001 C CNN
F 1 "VCC" H 2968 2823 50  0000 C CNN
F 2 "" H 2950 2650 50  0001 C CNN
F 3 "" H 2950 2650 50  0001 C CNN
	1    2950 2650
	1    0    0    -1  
$EndComp
Connection ~ 2950 2650
Wire Wire Line
	2950 2650 3800 2650
Wire Wire Line
	2150 2250 1500 2250
Text Label 1500 2250 0    50   ~ 0
VCC
Wire Wire Line
	3350 2250 2450 2250
Wire Wire Line
	3650 2250 4150 2250
Text Label 4150 2250 2    50   ~ 0
GND
Wire Wire Line
	5100 2850 4750 2850
Wire Wire Line
	4750 2850 4750 2200
Wire Wire Line
	4750 2200 5550 2200
Wire Wire Line
	6550 2200 6550 2850
Wire Wire Line
	6550 2850 6300 2850
Wire Wire Line
	5850 2200 6550 2200
Wire Wire Line
	4750 2850 4400 2850
Connection ~ 4750 2850
Wire Wire Line
	6550 2850 6800 2850
Connection ~ 6550 2850
Text Label 6800 2850 2    50   ~ 0
GND
Text Label 4400 2850 0    50   ~ 0
VCC
Wire Wire Line
	6300 3450 6750 3450
Text Label 6750 3450 2    50   ~ 0
UPDI
Wire Wire Line
	3800 3800 3800 3300
Wire Wire Line
	6300 3750 6750 3750
Wire Wire Line
	6300 3600 6750 3600
Wire Wire Line
	6300 3000 6750 3000
Text Label 6750 3750 2    50   ~ 0
LED_R
Text Label 6750 3600 2    50   ~ 0
LED_G
Text Label 6750 3000 2    50   ~ 0
LED_B
Wire Wire Line
	3800 3300 4300 3300
Wire Wire Line
	4300 3300 4300 3000
Wire Wire Line
	4300 3000 5100 3000
Connection ~ 3800 3300
Wire Wire Line
	3800 3300 3800 3000
$Comp
L FabAcademy:ATtiny1614 U1
U 1 1 5EC82FC9
P 5700 3300
F 0 "U1" H 5700 4067 50  0000 C CNN
F 1 "ATtiny1614" H 5700 3976 50  0000 C CNN
F 2 "FabAcademy:fab-SOIC-14_3.9x8.7mm_P1.27mm" H 5700 3300 50  0001 C CIN
F 3 "" H 5700 3300 50  0001 C CNN
	1    5700 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 3850 2300 3400
Wire Wire Line
	5100 3150 4400 3150
Wire Wire Line
	4400 3150 4400 4650
Wire Wire Line
	4400 4650 1800 4650
Wire Wire Line
	1800 4650 1800 3400
Wire Wire Line
	1800 3400 2300 3400
Connection ~ 2300 3400
Wire Wire Line
	2300 3400 2300 3050
Wire Wire Line
	5100 3600 4650 3600
Wire Wire Line
	5100 3750 4650 3750
Text Label 4650 3600 0    50   ~ 0
RX
Text Label 4650 3750 0    50   ~ 0
TX
NoConn ~ 5100 3450
NoConn ~ 5100 3300
NoConn ~ 6300 3300
NoConn ~ 6300 3150
NoConn ~ 8400 6000
$EndSCHEMATC
