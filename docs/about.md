# About
<img style="float: right" src="../img/cryptid2.jpg" alt="Profile Image" width="200" />
This site hosts my documentation for work done while at Aalto University. I am a student of New Media: Design and Production, and previously created wearable LED art and other new media pieces in ITP Camp at NYU, as well as for personal projects. My main interest in the field of New Media is physical/digital fabrication, as well as the following topics: wearables, body augmentation, LEDs, sensors, silicone, microcontrollers, soft robotics, 3D printing. 

A portfolio of past works can be found [here.](https://jxie19.wixsite.com/jasmine-xie)

My instagram is [@blueberrytea87](https://www.instagram.com/blueberrytea87/)

I can be reached at <mailto:jasmine.xie@aalto.fi>

<img style="float: right" src="../img/Cat01.jpg" alt="Cat" width="200" />Most recent project and tutorial (currently under revision): [Squish That Cat!](https://create.arduino.cc/projecthub/jyxie/squish-that-cat-c842c6)



