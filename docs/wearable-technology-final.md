# Flung Out of Space (F.O.S.)

## A sensor-enhanced neckpiece, paired with an enhanced bracelet

![electronics_19](../img/wearable-technology-final/electronics_19.jpg)

![electronics_20](../img/wearable-technology-final/electronics_20.jpg)

![electronics_21](../img/wearable-technology-final/electronics_21.jpg)

![electronics_22](../img/wearable-technology-final/electronics_22.jpg)

![electronics_23](../img/wearable-technology-final/electronics_23.jpg)

### Concept
A two-part set of wearables, F.O.S. is designed to increase intimacy between two people by adding an element of synchrony to their biology, without needing the touch or immediate presence of the other party.

The two-part system consists of:

* Neckpiece: primarily provides feedback to the Bracelet. Can transfer the wearer's heartbeat to the Bracelet in the form of a blinking light and vibrations.

* Bracelet: primarily receives information from the Neckpiece.

### Technology

#### Input:

* [DIY GSR Sensor](http://ftmedia.eu/diy-gsr-sensor/)
                
        // GSR sensor variables
        int sensorPin = 0; // select the input pin for the GSR
        int sensorValue; // variable to store the value coming from the sensor
        
        // Time variables
        unsigned long time;
        int secForGSR;
        int curMillisForGSR;
        int preMillisForGSR;

        void setup() {
            // Prepare serial port
            Serial.begin(9600);
            secForGSR = 1; // How often do we get a GSR reading
            curMillisForGSR = 0;
            preMillisForGSR = -1;
        }
        void loop() {
            time = millis();

            curMillisForGSR = time / (secForGSR * 1000);
            if(curMillisForGSR != preMillisForGSR) {
                // Read GSR sensor and send over Serial port
                sensorValue = analogRead(sensorPin);
                Serial.print(sensorValue, BYTE);
                preMillisForGSR = curMillisForGSR;
            }
        }

    * [Low-pass filter code](https://elvistkf.wordpress.com/2016/04/19/arduino-implementation-of-filters/)

            const float alpha = 0.5;
            double data_filtered[] = {0, 0};
            const int n = 1;
            const int analog_pin = 0;

            void setup(){
                Serial.begin(9600);
            }

            void loop(){
                // Retrieve Data
                data = analogRead(analog_pin);

                // Low Pass Filter
                data_filtered[n] = alpha * data + (1 - alpha) * data_filtered[n-1];

                // Store the last filtered data in data_filtered[n-1]
                data_filtered[n-1] = data_filtered[n];
                // Print Data
                Serial.println(data_filtered[n]);

                delay(100);
            }

* [SparkFun Pulse Oximeter and Heart Rate Monitor MAX30101](https://learn.sparkfun.com/tutorials/sparkfun-pulse-oximeter-and-heart-rate-monitor-hookup-guide)

    * [GitHub library](https://github.com/sparkfun/SparkFun_Bio_Sensor_Hub_Library)

* [DIY Soft Pressure Sensor](https://www.instructables.com/id/Flexible-Fabric-Pressure-Sensor/)

    * [Adafruit tutorial: Force Sensitive Resistor (FSR)](https://learn.adafruit.com/force-sensitive-resistor-fsr/using-an-fsr)
            
            /* FSR testing sketch. 
 
            Connect one end of FSR to power, the other end to Analog 0.
            Then connect one end of a 10K resistor from Analog 0 to ground 
            
            For more information see www.ladyada.net/learn/sensors/fsr.html */
            
            int fsrPin = 0;     // the FSR and 10K pulldown are connected to a0
            int fsrReading;     // the analog reading from the FSR resistor divider
            int fsrVoltage;     // the analog reading converted to voltage
            unsigned long fsrResistance;  // The voltage converted to resistance, can be very big so make "long"
            unsigned long fsrConductance; 
            long fsrForce;       // Finally, the resistance converted to force
            
            void setup(void) {
            Serial.begin(9600);   // We'll send debugging information via the Serial monitor
            }
            
            void loop(void) {
            fsrReading = analogRead(fsrPin);  
            Serial.print("Analog reading = ");
            Serial.println(fsrReading);
            
            // analog voltage reading ranges from about 0 to 1023 which maps to 0V to 5V (= 5000mV)
            fsrVoltage = map(fsrReading, 0, 1023, 0, 5000);
            Serial.print("Voltage reading in mV = ");
            Serial.println(fsrVoltage);  
            
            if (fsrVoltage == 0) {
                Serial.println("No pressure");  
            } else {
                // The voltage = Vcc * R / (R + FSR) where R = 10K and Vcc = 5V
                // so FSR = ((Vcc - V) * R) / V        yay math!
                fsrResistance = 5000 - fsrVoltage;     // fsrVoltage is in millivolts so 5V = 5000mV
                fsrResistance *= 10000;                // 10K resistor
                fsrResistance /= fsrVoltage;
                Serial.print("FSR resistance in ohms = ");
                Serial.println(fsrResistance);
            
                fsrConductance = 1000000;           // we measure in micromhos so 
                fsrConductance /= fsrResistance;
                Serial.print("Conductance in microMhos: ");
                Serial.println(fsrConductance);
            
                // Use the two FSR guide graphs to approximate the force
                if (fsrConductance <= 1000) {
                fsrForce = fsrConductance / 80;
                Serial.print("Force in Newtons: ");
                Serial.println(fsrForce);      
                } else {
                fsrForce = fsrConductance - 1000;
                fsrForce /= 30;
                Serial.print("Force in Newtons: ");
                Serial.println(fsrForce);            
                }
            }
            Serial.println("--------------------");
            delay(1000);
            }

    * [Sparkfun Force Sensitive Resistor Hookup Guide](https://learn.sparkfun.com/tutorials/force-sensitive-resistor-hookup-guide/all)

    * <s>[Flexible and stretchable fabric-based tactile sensor](https://www.sciencedirect.com/science/article/pii/S0921889014001821)
        <font size="2">

        * "we decided on a design that uses 4 layers of different plain and conductive fabrics, which ensured good elasticity of the compound sensor"

        * "By placing the piezoresistive fabric between two highly conductive materials, we can observe a change in the resistance measured at the two outer layers when pressure is applied to the compound. "

        * "an additional non-conductive meshed layer was added between the middle piezoresistive layer and one of the electrode layers. Sensor sensitivity was found to depend on the thickness of the meshed layer and on the size of the mesh openings, with larger openings and thinner layers producing better sensitivity to first touch"

        </font>

        * ![fabric_sensor](../img/wearable-technology-final/fabric_sensor.jpg)
    </s>


* [DIY Fabric Button](https://www.instructables.com/Three-Fabric-Buttons/)
    
    * [Programming Arduino for a button](https://arduinogetstarted.com/tutorials/arduino-button)

#### Output

* Mood indicator LED: on both pieces, linked to the GSR sensor.

* Heart rate synchronizer: link to a vibrating motor in the Bracelet, beats along with the Neckpiece-wearer's heartrate

    * Consider, vibrations back and forth - would require two sensors, which could get expensive quickly

    * [Vibration Motor Circuit](http://www.learningaboutelectronics.com/Articles/Vibration-motor-circuit.php)

    ![vibration_circuit](../img/wearable-technology-final/vibration_circuit.jpg)

* Pressure indicator LEDs: indicates when the pressure sensor has been pressed, for button activation.

    * [NeoPixel Stick - 8 x 5050 RGB LED with Integrated Drivers](https://www.digikey.fi/product-detail/fi/adafruit-industries-llc/1426/1528-1354-ND/5395892)

#### Other

* [NRF24L01 modules (2)](https://www.ebay.com/itm/NRF24L01-2-4GHz-Antenna-Wireless-Transceiver-Module-For-Arduino/330897231575?hash=item4d0b030ed7:g:I9EAAOSwE7JacQ7d) ($9.80) to be used for two-way wireless communication between the wearables, as per this tutorial: [Arduino Communication With the nrf24L01](https://tutorial45.com/arduino-with-nrf24l01/)

* [Arduino Pro Mini 3.3v](https://store.arduino.cc/arduino-pro-mini) ([$9.95](https://www.digikey.com/product-detail/en/sparkfun-electronics/DEV-11114/1568-1054-ND/5140819)) for the microcontroller

    * Arduino Pro Mini: 13 x 33mm, 3.3v, 14 digital pins, 6 analog

## Presentation:
[MUO-E1051_Wearable Technology_Final Presentation_Flung_Out_of_Space](../img/wearable-technology-final/Presentation.pdf)

## Pinout designations:
| Pin | Attribution (C = neckpiece, B = bracelet) | Variable Name     |
|-----|-------------------------------------------|-------------------|
| 0   | RXD                                       |                   |
| 1   | TXD                                       |                   |
| 3   | B: Motor (C -> B)                         | B_MOTOR_PIN       |
| 4   | C: Motor 2                                | MOTOR_PIN2        |
| 6   | B: BPM LEDs (C -> B)                      | B_BPM_GSR_LED_PIN |
| 7   | C: GSR LEDs                               | GSR_LED_PIN       |
| 9   | C: Breathing LEDs                         | PRESSURE_LED_PIN  |
| 11  | C: BPM Sensor RESET                       | resPin            |
| 13  | C: BPM Sensor MFIO                        | mfioPin           |
| A0  | C: Breath sensor                          | PRESSURE_PIN      |
| A1  | C: GSR sensor                             | GSR_PIN           |
| A2  | C: Button                                 | BUTTON_PIN        |
| A3  | B: Button                                 | B_BUTTON_PIN      |
| A6  | B: GSR sensor                             | B_GSR_PIN         |

## Code:
[Final Code](../img/wearable-technology-final/FOSFinal.ino)

<b>Features - Neckpiece:</b>

* BPM sensor that records heart rate, stabilizes the data, and sends the heart rate as a vibration pattern to the vibe motor in the bracelet

* Pressure sensor that maps pressure from breathing to LED lights 

* Pressure sensor that acts as a button (meant to trigger sending the heart rate data to the bracelet, never finished coding)

* GSR sensor mapped to LEDs, mood-ring style

* Vibration motor that reflects the BPM of the incoming heart rate signal 

<b>Features - Bracelet:</b>

* Faked bpm sensor that sends a randomly generated heart rate as a vibration pattern to the vibe motor in the neckpiece (due to lack of a second pulse sensor)

* Pressure sensor that acts as a button (meant to trigger sending the heart rate data to the neckpiece, never finished coding)

* GSR sensor mapped to an LED, mood-ring style

* LEDs that reflect the BPM of the incoming heart rate signal

* Vibration motor that reflects the BPM of the incoming heart rate signal 

## Course Conclusions
### What have I learned?
So many things! 

* Improved my technique for attaching wires to snap buttons / in general feel much more comfortable with making pressure sensors and working flexibly with e-textiles. 

* Learned how to program an Arduino Pro Mini, which requires an FTDI adapter, by using an Arduino Uno and also correctly timing the 'reset' button. This was an unexpected hurdle, but now I can use an even smaller microcontroller than the Arduino Nano!

* Learned how to program asynchronous processes / essentially the "blink without delay" strategy, which allowed me to run a bunch of different functions at approximately the same time, while on different timing cycles (so I could say, check the pressure sensor every 10ms, update the LEDs every 25ms, check the GSR sensors every 5ms, and update the heartrate indicators (LEDs and vibration motors) on a varied rate, to imitate a fluctuating BPM). This was super exciting, and took quite some hours but it was really worth it and I'm excited to use this technique going forward.

* Learned strategies for how to approach smoothing sensor data - in the past, I had primarily worked with calibrating sensors, but not filtering the incoming data to make meaningful conclusions. Through Valterri's guidance, I was able to use moving averages and z-scores to smooth out the GSR data, which was relatively jumpy, and I also learned how to use the heart rate sensor and how to adapt data collection to accomodate for the instability of the readings.

* Learned to pattern complex shapes, starting with paper patterns and moving to learning to accomodate for layering thick fabrics around curvatures - because I had to use at least four layers of the faux-suede for the neckpiece and the wrist Bracelet, resulting in increasingly smaller layers as the layers went inwards. 

### How did the process go?
The process went relatively well! I spent a lot of time working on the project, and feel like I had a good mix of learning new things / challenging myself and using skills I already had. I went into this course very interested in the wearables side of the course, and had prior experience with programming for the Arduino and reasonably extensive experience with working with Neopixels and capacitive sensors. However, I did not have a lot of experience with biometric sensors, and I found that my soldering / wire management technique has improved. I had also planned to implement wireless functionality, but ran out of time.

### What could be done differently?
I could make the base shape differently, and start with a different layering technique for a more accurate build. I could also custom make PCBs instead of using veroboard, and make a slimmer design by externalizing the battery or something. Going forward, I also now have the experience of making the first build, and have better odds for a good second build.

On a larger course level, I think there could've been more focus on the e-textile and electronics/wearable side - I feel like there was a relative lot of functional wear lectures, which were interesting, but I feel like peoples' experience with the electronics side could've been smoother.

### What succeeded well?
The empty-interior strategy for assembly worked out suprisingly well for a relatively flat final project - it was necessarily a certain thickness due to the li-po battery, but it avoided having a fifth layer through careful wire coordination.

The final aesthetics of it were also quite nice - the transformation between the vaguely medical white to the reflective hot pink fabric really set a strong look for it, and I feel that the added spikes really helped it have a distinctive accessory look, almost plausibly wearable.

Finally, the electronics and code part of the fabrication turned out surprisingly successful and stable after the final changes for the motor, and I learned a lot in all of the coding for this course.

### Personal credits suggestion
I would suggest a 5/5 score on this course. This is because I have dedicated a lot of time and care to this course, and I achieved the course goals well. My concept was a multifunctional, paired set of wearable art accessories, and I succeeded in actualizing a solid, functioning prototype of my concept.

## Learning Diary: Classes
### Week 37: Theory and introductions (7.9-10.9)
#### 7.9.2020 Introductions
The scope of this class is quite impressive and exciting.

#### 8.9.2020 Lectures and Demos
There are so many cool potentials for working with eTextiles, especially in weaving/knitting them directly, rather than working with pieces of conductive fabric. Very aspirational, but I just don’t know that much about textiles. Really interesting way to create soft sensors though.

Banana jump game - it is possible to do some interesting biofeedback without having to strap electrodes on your head, which is great. I am definitely planning on using biometric data in my project.

#### 10.9.2020 Presentations and Functional Materials and Team-building
Presented our interests today, and have teamed up with Joona to create some sort of wearable. Very excited about this, but also have only a week to come up with our concept, which is a very fast turnaround (but also, the remaining build time is quite short, so it’s good to figure things out quickly).

### Week 38: Coding and prototyping (14.9-17.9)
#### 14.9.2020 Programming Day 1
No real learning due to familiarity with programming for Arduinos and basic physical computing, ruminated on what my project should be. 

No conclusions had.

#### 15.9.2020 Programming Day 2 & Simple Circuit Building
Programming was repeating things I knew, but afternoon with the simple circuit was a very nice and needed refresher on working with eTextiles. Laminations and learning how to use the school button press and the heat press was also quite exciting.

#### 17.9.2020 Programming Day 3 & Sensor Building
Programming was not particularly useful, but building the sensors was fun and useful. Ran into weird bugs where my sensor would work when loosely stacked, but when glued together failed, but the principle is there.

### Week 39: Visiting lectures and concept presentations (21.9-24.9)
#### 21.9.2020 Waterproofing and Footwear

Morning: Waterproofing
* 90C = hydrophobic, 150C = superhydrophobic, 70C is pretty good already (contact angle increases with temp)

* Coat fabric with positively charged starch, then apply emulsified wax/water, then another layer, and another wax layer to create a waterproof layer. Starch must also be made positive, it’s naturally anionic

* This is not washable with detergents

* Coating can be applied by dipping, spraying, or brushing

* Pigments can be added to the wax dispersion

* Waterproofing vs Breathability

* Surface roughness adds to hydrophobicity (see, coarse knit sweaters vs machine knit sweaters)

![lectures_01](../img/wearable-technology-final/lectures_01.jpg)

Afternoon: Footwear
* Why do we use shoes? 
    * Protection, Function, Performance, Fashion

* Last is necessary to make a sole

* Midsole is where to fit all the cool technologies

* Waterproofing is always in the lining, sometimes in the outer as well

Conclusions

I’m not making shoes, but they’re very cool and incredibly complex. Waterproofing/material science is also very cool and out of my scope for this course.

#### 22.9.2020 Visiting lectures (VTT)
VTT:

* Printed electronics

* Stretchable conductive inks

* Stretchable substrates for the inks

* In-mold electronics

* Stretchable/miniaturized sensors

* Thermoformed polymeric actuators

* Energy storage: bendable, stretchable batteries and energy harvesting

* Ultra-thin flexible silicon chips

![lectures_02](../img/wearable-technology-final/lectures_02.jpg)

![lectures_03](../img/wearable-technology-final/lectures_03.jpg)

![lectures_04](../img/wearable-technology-final/lectures_04.jpg)

![lectures_05](../img/wearable-technology-final/lectures_05.jpg)

Jaana Vapaavuori:
![lectures_06](../img/wearable-technology-final/lectures_06.jpg)

* Thermal changing of nylon coils to make them expand and contract, currently using heat gun, could coat them with something conductive but resistive and run electricity to heat it up - can I use nichrome wire running through the coil and heat that up? 

#### 24.9.2020 Presentations & Tutoring
Received very good feedback on my initial concept (wireless anxiety gloves), realized that yes I’m focusing on the technical aspect rather than the, say, viability as a product / this is very much intended to be an art piece rather than something genuinely functional. Also should do more research into physiological synchrony / heartbeats synchronizing over time, as well as flesh out the artistic aspect of my project.

To do: more research, development of the concept 

### Week 40: CLO3D, Tutoring & Prototyping Begins
#### 29.9.2020 Clo3D
Learned how to use the basics of Clo3D - pattern creating and testing, adding texture & prints. Very complex but powerful software - not useful this time as I am creating an accessory rather than something with a larger pattern, and Ilona talked to me about using the tape method of pattern making for making the gloves, which I am familiar with. Interesting class, I believe I will learn Clo3D properly when I need to use it.

## Project Diary

### 29.9.2020 Change in plans
After discussion with Matti N., I have decided to experiment with the following sensors:

* [DIY GSR Sensor](http://ftmedia.eu/diy-gsr-sensor/)

* [SparkFun Pulse Oximeter and Heart Rate Monitor MAX30101](https://learn.sparkfun.com/tutorials/sparkfun-pulse-oximeter-and-heart-rate-monitor-hookup-guide)

* [DIY Soft Pressure Sensor](https://www.instructables.com/id/Flexible-Fabric-Pressure-Sensor/)

* [Button Switches](https://www.kobakant.at/DIY/?p=7349)

And experiment with alternative placements for the Heart Rate Monitor, as I don't particularly want to create yet another glove or earring to facilitate BPM readings. 

### 01.10.2020 Tutoring sessions
Discussed potentially changing the planned wearable with Ilona and Emmi, had positive response and also was reassured about the expected outcome of the final project.

Discussed using GSR sensors with Valtteri, but don't have much ground to ask questions because I haven't built any sensors / made any code.

### 02.10.2020 Package Arrival!
My [SparkFun Pulse Oximeter and Heart Rate Sensor - MAX30101 & MAX32664](https://www.sparkfun.com/products/15219) arrived, along with my two [Arduino Pro Mini 328 - 3.3V/8MHz](https://www.sparkfun.com/products/11114)s, which enabled me to start playing around with the sensor.

Goals/Accomplishments: 

Managed to accomplish building a GSR sensor circuit out of conductive fabric and a .1uf capacitor and a 1MOhm resistor, but experiencing bugginess with the readings - to be addressed later.

Also managed to set up the heart rate sensor, which was difficult until I realized that the breadboard pin connections were just too loose/unstable, so I stripped some wire and threaded it through the holes on the module, and alligator clipped the other ends to jumper wires to plug directly into the Arduino Uno.

Next steps:

Work on testing heart rate placement, as that will decide what kind of wearable I will be making. (Neck -> necklace, wrist -> bracelet, fingertip -> glove, in order of preference). Figure out what's wrong with my GSR sensor. Write test code for the sensors, and set up some LED indicators as well.

### 03.10.2020 Minor testing
Conclusion: Heart rate sensor does work on different points on the neck, wrist, and fingertip, though it is quite sensitive to movement/very particular about location - this will present as a challenge if I don't want to make a glove which straps the sensor to the fingertip.

### 04.10.2020 Sensor testing & wearable design
Successfully found two points on the neck where the sensor can detect heart rates; however, they're located outside of the typical neckpiece/choker placement - as a result, I am currently trying a high-necked, highly stylized wearable.

Patterned and cut out a mockup of the neckpiece in white synthetic suede (I think, it's quite thick), and (through a lot of fiddling) temporarily secured the heart rate sensor to it so when it's put on, it's likely to be located correctly. We will see if I can increase the accuracy, but I'm not feeling optimistic about it.

![collar_01](../img/wearable-technology-final/collar_01.jpg)

![bpmSensor_01](../img/wearable-technology-final/bpmSensor_01.jpg)

<video controls muted height = "480">
    <source src="../img/wearable-technology-final/bpmSensor_02.mp4" type ="video/mp4">
    A video of data coming from the bpm sensor.
</video>

Bought two second-hand leather jackets, to be used in the final product.

Tested the LEDs as well, and have come to the conclusion that I will use either a [Neopixel Stick](https://www.adafruit.com/product/3039), or hand-solder five neopixels together, as a strip is too long to fit in my desired location.

![leds_01](../img/wearable-technology-final/leds_01.jpg)

Finally, tried again at the GSR sensors, and I'm just struggling a lot with this one - I fixed my circuit, which improved the data by a lot, but I still struggle with inducing a response to test if the sensor is detecting anything meaningful.

Next on my to-do list includes getting some mesh so I can make a nice pressure sensor (the paper I read has an interesting way to make them) and consulting with Valtteri about the GSR sensors.

### 05.10.2020 Creating a Pressure Sensor
Today's goal was to create and test a soft pressure sensor, and I succeeded.

I first cut out the design based on the instructions from here: [Conductive Fabric Pressure Sensor](https://www.instructables.com/Flexible-Fabric-Pressure-Sensor/), and added a mesh layer, as per the research from a paper I read, to increase sensitivity.

![pressureSensor_01](../img/wearable-technology-final/pressureSensor_01.jpg)

The mesh proved to be too opaque to allow any conductivity, so I used the following structure to build my sensor: fabric-conductive fabric-Velostat-conductive fabric-fabric. I chose to use Velostat instead of the EeonTex fabric for the piezoresistive layer because: I didn't need the sensor to be stretchy, I already had a decent quantity of Velostat, and Velostat was flexible enough for my purposes. I sewed the layers together, and tested their resistance with successful results. 

Next steps: add button snaps to the pressure sensor, consult with Valtteri about the GSR sensors, and set up the vibe motor circuit.

### 06.10.2020 Clean-up and motors
I organized the wires coming out of the heart rate sensor and added headers to the ends of the wires, instead of alligator-clipping them to jumper wires. This makes the connection to the breadboard or Arduino much less prone to breakage, and also organizes the wires nicely. 

I also added a reinforcement outer layer to the neckpiece, as it will be at least three layers in the end, and taped the heart in.

![pressureSensor_02](../img/wearable-technology-final/pressureSensor_02.jpg)

I also implemented the vibe motor circuit. Took longer than it should've, but it works at least!

Current state of affairs:
![workstation_01](../img/wearable-technology-final/workstation_01.jpg)

To Do: add button snaps, consult with Valtteri about GSR sensors

### 07.10.2020 Finishing the pressure sensor

I added the button snaps, and also programmed the sensor output to a row of LEDs, so it has a calibration sequence in the beginning and then lights up in a rainbow sequence as pressure is applied.

<video controls muted height = "480">
    <source src="../img/wearable-technology-final/pressureSensor_04.mp4" type ="video/mp4">
    A video of LEDs reacting to the pressure sensor!
</video>

Spent hours trying to figure out how to program asynchronous timing, following [this guide on the Arduino forum](https://forum.arduino.cc/index.php?topic=223286.0), but failed. This is important to my project because I want the heartrate sensor to be checking in on a different time interval than the pressure sensor, and also the output for the heartrate sensor will be a variable based on the detected BPM, so it will be on its own time scale. 

To do: Figure async programming out!!

### 08.10.2020 Succeeded Programming Asynchronous Processes

I carefully rewrote the programs from yesterday, breaking the process into separate steps: first, change the pressure sensor to rely on computer time passing (millis() - prevMillis) rather than delay(). Then did the same for the LED indicator for the heartrate sensor, then again for the heartrate sensor itself. I eventually combined all of my progress so far to this:

<video controls muted height = "480">
    <source src="../img/wearable-technology-final/LEDOutput_01.mp4" type ="video/mp4">
    A video of LEDs reacting to the pressure sensor and a faked heartrate reading.
</video>

In which the pressure sensor is directly mapped to control the rainbow LEDs (on the matrix), but it is also linked to generate a stand-in BPM while I figure out how to stabilize the actual heart rate readings. The BPM is then translated to increasingly rapid flashing of the red LED strip.

I was also able to talk with Valtteri about my GSR sensor, and we verified that it works, the data is just fairly noisy, and he suggested some methods to smooth out the data that I will try to figure out in the future. This is the feature most likely to be trimmed out if I run out of time, I think.

### 09.10.2020 New components!

I got my Digikey order in, which consisted of two sticks of 8 Neopixels apiece, as well as a [SparkFun Photodetector Breakout - MAX30101](https://www.sparkfun.com/products/16474), which uses the same sensor as my current heartrate sensor, but doesn't have the same microchip. The Neopixel stick performs fabulously, the photodetector less so, so I might use it for the Bracelet but that's a consideration for future me.

### 10.10.2020 Stabilized BPM and improved other code

Wrote a program to create a buffer of existing heartrate data, which will rely on past data to return a heartrate even if the signal drops. It will update data accordingly if new data comes in, but this helps stabilize the unstable sensor readings.
collarFakingBPM.ino

collarAsyncPressureLEDsStableBPMLEDs.ino

            // Asynchronous processes
            // maps pressure readings to rainbow LEDs
            // REPLACES: maps pressure to a fake BPM
            // REPLACES: maps fake BPM to red LEDs
            // REPLACES: maps real BPM to red LEDs
            // NEW: maps stabilized BPM to red LEDs
            // NEW: converted setup sequences into functions

collarAsyncPressureLEDsStableBPMLEDsVibe.ino

            // NEW: maps stabilized BPM to vibe motor

collarAsyncPressureLEDsStableBPMLEDsVibeReset.ino
            
            // NEW: adds reset function to the BPM sensor after 10s of failures

### 11.10.2020 Planning transitioning to the Arduino Pro Mini
Did some serious logistics planning for how I'm going to wire everything when I go from an Arduino Uno and breadboard to an Arduino Pro Mini and perf-boards. Going to be a big process, which I should start pretty soon, but I need lab access first.

### 12.10.2020
Goals: 

* Begin transitioning from breadboard to perfboards and Arduino Uno to Arduino Pro Mini [SUCCESS]

Accomplishments:

* Figured out how to program the Pro Mini by using an Arduino Uno.

* Ported everything over to the Pro Mini

* Soldered female headers to the Pro Mini

* Ported everything over to perfboard


![electronics_01](../img/wearable-technology-final/electronics_01.jpg)

Next steps: 

* Make the GSR sensor

### 13.10.2020
Made the GSR sensor by covering two US pennies in conductive fabric and attaching snaps buttons to them.

Made a new pressure sensor.

Patterned and cut two more layers.

![electronics_04](../img/wearable-technology-final/electronics_04.jpg)

![electronics_05](../img/wearable-technology-final/electronics_05.jpg)

Made power/ground busses out of female headers.

![electronics_02](../img/wearable-technology-final/electronics_02.jpg)

Fit pressure sensor 1 & 2, vibration motor, GSR Sensor, Arduino Pro Mini inside the neckpiece.

![electronics_03](../img/wearable-technology-final/electronics_03.jpg)

### 14.10.2020
Coded the GSR sensor and reactive LED.

Made second vibration motor for bracelet.

Coded second heartbeat/motor relationship

### 15.10.2020
Cut out new layers for prototype v2.

![electronics_06](../img/wearable-technology-final/electronics_06.jpg)

Tested glue for bonding

Fit the heart sensor and pressure-reactive LEDs

Added a JST connector for the li-po battery

### 16.10.2020
Started inserting pieces into the new prototype, and started glueing together the prototype.

![electronics_07](../img/wearable-technology-final/electronics_07.jpg)

![electronics_08](../img/wearable-technology-final/electronics_08.jpg)

### 17.10.2020
Shortened and organized wires.

### 18.10.2020
Shortened more cables, added buckle, experimented with fabric coverings.

### 19.10.2020
Covered LEDs with heart cutouts, adhered outer fabric to the neckpiece, patterned and began assembling the bracelet, trimmed cables

![electronics_09](../img/wearable-technology-final/electronics_09.jpg)

![electronics_10](../img/wearable-technology-final/electronics_10.jpg)

![electronics_11](../img/wearable-technology-final/electronics_11.jpg)

![electronics_12](../img/wearable-technology-final/electronics_12.jpg)

### 20.10.2020
Completed the bracelet, majority of neckpiece. 

![electronics_13](../img/wearable-technology-final/electronics_13.jpg)

![electronics_14](../img/wearable-technology-final/electronics_14.jpg)

![electronics_15](../img/wearable-technology-final/electronics_15.jpg)

![electronics_16](../img/wearable-technology-final/electronics_16.jpg)

![electronics_17](../img/wearable-technology-final/electronics_17.jpg)

![electronics_18](../img/wearable-technology-final/electronics_18.jpg)

### 21.10.2020
Motor started exhibiting strange behaviours around 6pm. Spent the remainder of the next several hours attempting to debug the issue, via a combination of troubleshooting the code and the physical connections. 

### 22.10.2020
Exhibition day!

![exhibition_01](../img/wearable-technology-final/exhibition_01.jpg)

### 23.10.2020
Swapped out the motor, it seems to work better now. It may have been a hardware issue emanating directly from the motor, meaning the heatshrink protection I tried could have possibly had a negative effect on the motor connections. 

## Research
### Galvanic Skin Response

* [Arousal Effects on Pupil Size, Heart Rate, and Skin Conductance in an Emotional Face Task](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6287044/)
    <font size="2">

    * Galvanic skin response (GSR) is an independent index of sympathetic activity while heart rate (HR) is predominantly controlled by the parasympathetic nervous system (1–4). The sympathetic nervous system controls sweat gland activity, and increases in sympathetic activity produce corresponding increases in GSR (5). Although HR is predominantly linked to the parasympathetic system and parasympathetic activation decreases HR, it is antagonistically controlled by both sympathetic and parasympathetic activity which can produce increased or decreased HR, respectively (3, 4).
    </font>

* [Recommended GSR electrode locations](https://www.tobiipro.com/learn-and-support/learn/steps-in-an-eye-tracking-study/setup/gsr-electrode-locations/)
    <font size="2">

    * Palms, feet, fingers and shoulders are the most common locations to place the GSR electrodes because they have a high density of sweat glands
    </font>

### Physiological Synchrony - Heartbeat
* [Synchronized arousal between performers and related spectators in a fire-walking ritual](https://www.academia.edu/561950/Synchronized_arousal_between_performers_and_related_spectators_in_a_fire_walking_ritual?auto=download)
    <font size="2">

    * Here we show physiological effects of synchronized arousal in a Spanish fire-walking ritual, between active participants and related spectators, but not participants and other members of the audience.
    
    * We assessed arousal by heart rate dynamics
    </font>

* [When lovers touch, their breathing, heartbeat syncs, pain wanes, study shows](https://www.sciencedaily.com/releases/2017/06/170621125313.htm)
    <font size="2">

    * when an empathetic partner holds the hand of a woman in pain, their heart and respiratory rates sync and her pain dissipates.

    * The more empathic the partner and the stronger the analgesic effect, the higher the synchronization between the two when they are touching
    </font>

* [Two Hearts Beat As One, Literally: Our Heartbeats Synchronize With Those Of Our Loved Ones](https://www.lehmiller.com/blog/2015/2/6/two-hearts-beat-as-one-our-heartbeats-synchronize-with-those-of-our-loved-ones)
    <font size="2">
    
    * In this study, 32 heterosexual couples were hooked up to heart and respiration monitors while they sat a few feet apart looking into each other's eyes without having any physical contact. During this task, both partners' heart and breathing rates because more similar. However, no such effect was observed when participants were asked to gaze into the eyes of a stranger.

    * among people who share deep emotional attachments, they tend to share physiological states while in each other's presence
    </font>

    * [Assessing cross-partner associations in physiological responses via coupled oscillator models.](https://www.semanticscholar.org/paper/Assessing-cross-partner-associations-in-responses-Helm-Sbarra/381889918d39003a1b0d8d56170a6101c4119f59)
    <font size="2">

    * we use measures of respiration and heart rate from romantic partners recorded across three laboratory tasks
    
    * These models were used to capture oscillations in respiration and heart rate, and to examine interdependence in the physiological signals between both partners. Results show that associations were detectable within all three tasks, with different patterns of coupling within each task.
    </font>

* [Physical separation in adult attachment relationships](https://www.sciencedirect.com/science/article/pii/S2352250X18300836)
    <font size="2">

    * Physical separation may disrupt psychobiological linkage between romantic partners.
    
    * New forms of computer-mediated contact help couples sustain long-distance attachments.
    </font>

* [Strangers, Friends, and Lovers Show Different Physiological Synchrony in Different Emotional States](https://www.mdpi.com/2076-328X/10/1/11/htm)
    <font size="2">

    * an existing social relationship might reduce the predisposition to conform one’s autonomic responses to a friend or romantic partner during social situations that do not require direct interaction.

    </font>

### Pulse Oximetry
* [Does Pulse Oximetry Reliably Detect Aspiration in Dysphagic Stroke Patients?](https://www.ahajournals.org/doi/full/10.1161/01.str.28.9.1773)
    <font size="2">
    
    * Pulse oximetry reliably predicted aspiration or lack of it in 81.5% of cases.
   
    </font>

* [Pulse oximetry](https://en.wikipedia.org/wiki/Pulse_oximetry)
    <font size="2">

    * Reflectance pulse oximetry is a less common alternative to transmissive pulse oximetry. This method does not require a thin section of the person's body and is therefore well suited to a universal application such as the feet, forehead, and chest
  
    </font>

* [Reflectance pulse oximetry: Practical issues and limitations](https://www.sciencedirect.com/science/article/pii/S2405959516301205)
    <font size="2">

    *  it can be used at diverse measurement sites such as the feet, forehead, chest, and wrists.
 
    </font>

* [Forehead Pulse Oximetry: Headband Use Helps Alleviate False Low Readings Likely Related to Venous Pulsation Artifact](https://pubs.asahq.org/anesthesiology/article/105/6/1111/6844/Forehead-Pulse-OximetryHeadband-Use-Helps)
    <font size="2">

    * Application of up to 20 mmHg pressure on the forehead pulse oximetry sensor using an elastic tensioning headband significantly reduced reading errors and provided consistent performance when subjects were placed between supine and up to 15 degrees head-down incline (Trendelenburg position).
   
    </font>

## Related Products/Projects

* [Melo](https://www.theawellbeing.com/)
    <font size="2">

    * Melo is a breathing exercise device for wellness and mindfulness.

    * Melo uses expanding and contracting light rings alongside subtle vibration pulses to guide you through the breathing exercises.
    </font>

## Initial Plan: Wireless Gloves for Anxiety
### Concept

Bridging the distance between two people with wordless communication, sending and receiving signals through the air, disembodied/embodied interaction (50+ meters).

### Implementation
Two pairs of gloves, one hand of each pair is ‘wearable’ and the other is normal. One signifies anxiety (R), one offers reassurance (L). They mutually benefit when rejoined (handholding)

* Right-hand wearer experiences anxiety and/or other significant emotional changes, measured via Galvanic Skin Response (via sweat)

* Left-hand wearer receives the message via a vibration on the underside of their wrist

* Left-hand wearer sends a message back by pressing on the top of their wrist

* Right-hand wearer receives the message back via a vibration on the underside of their wrist

* When the hands are rejoined (handholding), the (sparsely) scattered red LEDs in the decoration will glow as red hearts

### Visual Material and Inspirations
Long gloves, with a flared end to stash electronics in. Leather top, soft bottom - permits flex and room for sensors to be embedded, since they usually require the pads of the fingers. Embroidery going up the arm to hide or emphasize conductive thread paths.

![gloves_01](../img/wearable-technology-final/gloves_01.jpg)

![gloves_02](../img/wearable-technology-final/gloves_02.jpg)

![gloves_03](../img/wearable-technology-final/gloves_03.jpg)

### Sketches

![gloves_04](../img/wearable-technology-final/gloves_04.jpg)

![gloves_05](../img/wearable-technology-final/gloves_05.jpg)

![gloves_06](../img/wearable-technology-final/gloves_06.jpg)

![gloves_07](../img/wearable-technology-final/gloves_07.jpg)

### Technologies and Materials

#### Sensors:
* Sweatiness sensor - DIY a Galvanic Skin Response sensor - can be made with two pieces of conductive material, though probably more conductive than fabric

* Pressure sensor and/or switch - fabric

* Potential for other fabric sensors such as bend

#### Other technology:
* Fabric electrodes - conductive fabric and thread

* Wireless communication - NRF24L01 2.4GHz wireless module, better than over wireless internet or bluetooth

* Potential for a switch via snap button

* Coin cell vibration motor

* Li-po batteries, 1 per glove

* Arduino (model? Depends on # of sensors used and pins needed)
