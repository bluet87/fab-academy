import adsk.core, adsk.fusion, traceback

def run(context):
    ui = None
    try:
        app = adsk.core.Application.get()
        ui = app.userInterface

        doc = app.documents.add(adsk.core.DocumentTypes.FusionDesignDocumentType)
        design = app.activeProduct

        # Get the root component of the active design.
        rootComp = design.rootComponent

        # Create a new sketch on the xy plane.
        sketch = rootComp.sketches.add(rootComp.xYConstructionPlane)

        outside = 75.0
        inside = 10.0
        offset = 2.0
        gap = 2.5
        numLines = (outside/2-inside/2)/offset
        shortNumLines =int(round(numLines/2))
        gapNumLines = int(numLines - shortNumLines)+1
        xGapLeft=[]
        xGapLeftMid=[]
        xGapRightMid=[]
        xGapRight=[]
        yGap=[]


        xShortLeft=[]
        xShortRight=[]
        yShort=[]
        # Create an object collection for the points.
        points = adsk.core.ObjectCollection.create()
        pointsShort = adsk.core.ObjectCollection.create()
        pointsGap = adsk.core.ObjectCollection.create()

        for i in range(gapNumLines):
            y=2*i*offset
            yGap.append(y)
            leftX=(2*i-1)*offset+gap
            if leftX < 0:
                leftX = 0
            xGapLeft.append(leftX)
            pointsGap.add(adsk.core.Point3D.create(leftX,y,0))
            leftMidX = outside/2-gap/2
            xGapLeftMid.append(leftMidX)
            pointsGap.add(adsk.core.Point3D.create(leftMidX,y,0))
            rightMidX = outside/2+gap/2
            xGapRightMid.append(rightMidX)
            pointsGap.add(adsk.core.Point3D.create(rightMidX,y,0))
            rightX=outside-leftX
            xGapRight.append(rightX)
            pointsGap.add(adsk.core.Point3D.create(rightX,y,0))

        #pt1=pointsGap.item(0)
       # pt2=pointsGap.item(1)
        #sketch.sketchCurves.sketchLines.addByTwoPoints(pt1,pt2)

        for i in range(int(pointsGap.count/2)):
            pt2=pointsGap.item(2*i+1)
            pt1=pointsGap.item(2*i)
            sketch.sketchCurves.sketchLines.addByTwoPoints(pt1,pt2)

        for i in range(shortNumLines):
            y=(2*i+1)*offset
            yShort.append(y)
            leftX=(2*i*offset)+gap+offset
            xShortLeft.append(leftX)
            pointsShort.add(adsk.core.Point3D.create(leftX,y,0))
            rightX=outside-leftX
            xShortRight.append(rightX)
            pointsShort.add(adsk.core.Point3D.create(rightX,y,0))

        for i in range(int(pointsShort.count/2)):
            pt1=pointsShort.item(2*i)
            pt2=pointsShort.item(2*i+1)
            sketch.sketchCurves.sketchLines.addByTwoPoints(pt1, pt2)



        # Create an object collection for the points.
        #points = adsk.core.ObjectCollection.create()
        #xGap = adsk.core.ObjectCollection.create()


    except:
        if ui:
            ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))