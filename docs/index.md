## Fab Academy 2020
[Documenting Jasmine Y. Xie's progress through the Fab Academy course of 2020 at Aalto University.](\assignments\week-01.md)

<img src="../img/final/casting_15.jpg" alt="Tentacles" width="400" />

## Squish That Cat!
[A pettable take on a glowing cat](\https://create.arduino.cc/projecthub/jyxie/squish-that-cat-c842c6)

<img src="../img/Cat01.jpg" alt="Cat" width="400" />

## Flung Out of Space
[A paired collar and cuff for creating intimacy across space](\FOS.md)

<img src="../img/wearable-technology-final/electronics_22.jpg" alt="Flung Out of Space" width="400" />

## Sonic Sweater
[A deceptively nice-looking sweater](\wearable-tech-2021.md)

<img src="../img/wearable-tech-2021/final_sweater_01.jpg" alt="Sonic Sweater" width="400" />
