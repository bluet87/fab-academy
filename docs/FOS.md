# Flung Out of Space (F.O.S.)

### Version 1
![electronics_19](../img/wearable-technology-final/electronics_19.jpg)

![electronics_20](../img/wearable-technology-final/electronics_20.jpg)

![electronics_21](../img/wearable-technology-final/electronics_21.jpg)

![electronics_22](../img/wearable-technology-final/electronics_22.jpg)

![electronics_23](../img/wearable-technology-final/electronics_23.jpg)

[Documentation and processes for version 1](wearable-technology-final.md)
### What:
F.O.S. [flung out of space] is a paired set of wearables, designed to foster an intimate bond between the paired wearers across a given space and distance by remotely transferring heartbeats between the partners. Gentle vibrations and pulsing lights provide physical feedback, while LED indicators on the neckpiece and cuff provide visual feedback on the wearers’ respective emotional states, enabling them to read each other at a glance, when they are unable to connect on a more physical basis. This particular project is intended to be a refinement and version 2 of the project, having already completed version 1.

The two-part system consists of:

* Neckpiece: primarily provides feedback to the Bracelet. Can transfer the wearer's heartbeat to the Bracelet in the form of a blinking light and vibrations. Also reflects the wearer's state of emotions via a 'mood ring' LED.

* Bracelet: primarily receives information from the Neckpiece, and can transfer the wearer's heartbeat to the neckpiece in the form of vibrations. Also reflects the wearer's state of emotions via a 'mood ring' LED.

### Why:
This started as a wearable project that was intended to act as an anxiety-soothing device. This evolved into something used to create a sense of intimacy, largely private to the wearers, across distance at say, a party.

### Who:
A pair of people who have some kind of relationship in which they would appreciate the intimacy of getting biofeedback from their partner.

### How:
A combination of input sensors and output devices, listed below. I have finished iteration 1, viewable here: [Flung Out of Space](../wearable-technology-final.md), and intend to refine the production process, use more digital fabrication techniques, and add a wireless communication feature to avoid tethering the wearers to each other. I also plan to try ordering PCBs, including possibly placing an order for a flexible PCB - tbd.

#### Input:

* [DIY GSR Sensor](http://ftmedia.eu/diy-gsr-sensor/)

* [SparkFun Pulse Oximeter and Heart Rate Monitor MAX30101](https://learn.sparkfun.com/tutorials/sparkfun-pulse-oximeter-and-heart-rate-monitor-hookup-guide)

* [DIY Soft Pressure Sensor](https://www.instructables.com/id/Flexible-Fabric-Pressure-Sensor/)

* [DIY Fabric Button](https://www.instructables.com/Three-Fabric-Buttons/)

#### Output

* Mood indicator LED: on both pieces, linked to the GSR sensor.

* Heart rate synchronizer: link to a vibrating motor in the Bracelet, beats along with the Neckpiece-wearer's heartrate

* Pressure indicator LEDs: indicates when the pressure sensor has been pressed, for button activation.

    * [NeoPixel Stick - 8 x 5050 RGB LED with Integrated Drivers](https://www.digikey.fi/product-detail/fi/adafruit-industries-llc/1426/1528-1354-ND/5395892)

#### Other

* [NRF24L01 modules (2)](https://www.ebay.com/itm/NRF24L01-2-4GHz-Antenna-Wireless-Transceiver-Module-For-Arduino/330897231575?hash=item4d0b030ed7:g:I9EAAOSwE7JacQ7d) ($9.80) to be used for two-way wireless communication between the wearables, as per this tutorial: [Arduino Communication With the nrf24L01](https://tutorial45.com/arduino-with-nrf24l01/)

* [Arduino Pro Mini 3.3v](https://store.arduino.cc/arduino-pro-mini) ([$9.95](https://www.digikey.com/product-detail/en/sparkfun-electronics/DEV-11114/1568-1054-ND/5140819)) for the microcontroller

    * Arduino Pro Mini: 13 x 33mm, 3.3v, 14 digital pins, 6 analog
