<style>
img[src*="#thumbnail"] {
    width:200px;
 }
</style>
# Final Project: Wearable Tentacles

### What:
Wearable tentacles! Specifically, I have an interest in making wearable tentacle prosthetics or accessories, for human augmentation. Current design proposals include either a single tentacle, that can be inflated from a flattened state, or a wearable piece composed of multiple tentacles. I intend them to respond to some sort of stimuli by inflating and unfurling, although I am currently unsure as to how best to handle touch reactivity. A faux-bioluminescent effect using LEDs could be very fun as well.

### Why:
I'm a fan of monstrousity, and expanding the definition of the human form.

### Who:
Anyone who is interested in having a bonus appendage (or multiple). This is not designed with serious functionality in mind - it is not meant to be a soft gripper, or act as a hand - it is an additional form of physical expression. For instance, it could be used to signify fear/anger/arousal, or be a sign of elation. It could solely reflect aspects of the wearer, or it could be the grounds for interaction between wearer and an interactor.

### How:
Key processes: CAD modeling, 3D printing, CNC milling, mold-making, casting, soft robotics, mechanical/machine design, PCB design

An approximation of the process is as follows:

* Design the mechanics, using the references listed below as well as other documentation on soft actuators

* Design the electronics, including what sensors need to be integrated (and therefore, what the PCB needs to support)

* Model the mold, referencing the structure of previous inflatables such as the soft gripper detailed [here](https://www.sciencebuddies.org/science-fair-projects/project-ideas/Robotics_p020/robotics/squishy-robots-build-an-air-powered-soft-robotic-gripper#procedure). and the [Trefoil Tentacle](https://learn.adafruit.com/silicone-robo-tentacle/a-little-background) and the [Soft Quadruped Robot](https://learn.adafruit.com/soft-quadruped-robot-glaucus?view=all) by [Matthew Borgatti](https://learn.adafruit.com/users/gianteye)., 

* CNC the external mold, 3D print the internal structures

* Cast the components in silicone

* Assemble

* Adjust code to add interactivity

### Using .stl in Fusion 360

Open a .stl file in [Meshmixer](http://www.meshmixer.com)

Export file as an .obj file

Open in Fusion 360, and right-click on the new Body (the imported object). Select "Mesh to BRep"

Tada!


### References - other projects:

* [http://fab.cba.mit.edu/classes/863.14/people/merav_gazit/project-16.html](http://fab.cba.mit.edu/classes/863.14/people/merav_gazit/project-16.html)

* [http://fab.academany.org/2019/labs/oshanghai/students/xiaofei-liu/17_wildcard_week.html](http://fab.academany.org/2019/labs/oshanghai/students/xiaofei-liu/17_wildcard_week.html)

* [http://fab.cba.mit.edu/classes/863.18/Harvard/people/lara/week7.html](http://fab.cba.mit.edu/classes/863.18/Harvard/people/lara/week7.html)


### References - documentation:

* [https://www.flowioplatform.com/workshop](https://www.flowioplatform.com/workshop)

* [https://www.sciencebuddies.org/science-fair-projects/project-ideas/Robotics_p020/robotics/squishy-robots-build-an-air-powered-soft-robotic-gripper#procedure](https://www.sciencebuddies.org/science-fair-projects/project-ideas/Robotics_p020/robotics/squishy-robots-build-an-air-powered-soft-robotic-gripper#procedure)

* [soft robotics toolkit](https://softroboticstoolkit.com/sofa/tutorial)

    * [soft robotics toolkit execution](http://fab.cba.mit.edu/classes/863.18/Harvard/people/lara/week7.html)



* [PNEUNETS BENDING ACTUATORS DESIGN](https://softroboticstoolkit.com/book/pneunets-design)

 <!-- <sub><sup>test</sup></sub> -->
### Research papers:

* [Articulated Hands: Force Control and Kinematic Issues](http://fab.cba.mit.edu/classes/865.15/classes/measurement/articulated-hands.pdf)
    <font size="2">
    <!-- * test -->
    </font>

* [Multigait soft robot](https://www.pnas.org/content/108/51/20400)

* [A Resilient, Untethered Soft Robot](https://www.researchgate.net/publication/275945257_A_Resilient_Untethered_Soft_Robot)

* [Design, fabrication and control of soft robots](https://www.researchgate.net/publication/277410991_Design_fabrication_and_control_of_soft_robots)
    <font size="2">
    <!-- * test -->
    </font>

* [Jellyfish-Inspired Soft Robot Driven by Fluid Electrode Dielectric Organic Robotic Actuators](https://www.frontiersin.org/articles/10.3389/frobt.2019.00126/full)

* [Soft Robotics: Review of Fluid-Driven Intrinsically Soft Devices; Manufacturing, Sensing, Control, and Applications in Human-Robot Interaction: Review of Fluid-Driven Intrinsically Soft Robots](https://www.researchgate.net/publication/318140258_Soft_Robotics_Review_of_Fluid-Driven_Intrinsically_Soft_Devices_Manufacturing_Sensing_Control_and_Applications_in_Human-Robot_Interaction_Review_of_Fluid-Driven_Intrinsically_Soft_Robots)

* [Translucent soft robots driven by frameless fluid electrode dielectric elastomer actuators](https://robotics.sciencemag.org/content/3/17/eaat1893)

* [Edible Soft Robotics](https://media.ccc.de/v/33c3-8113-edible_soft_robotics)

* [Glycerol as high-permittivity liquid filler in dielectric silicone elastomers](https://www.researchgate.net/publication/305461636_Glycerol_as_high-permittivity_liquid_filler_in_dielectric_silicone_elastomers)

* [Fabrication Process of Silicone-based Dielectric Elastomer Actuators](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4781713/)

* [Silicone elastomers with high dielectric permittivity and high dielectric breakdown strength based on tunable functionalized copolymers](https://www.semanticscholar.org/paper/Silicone-elastomers-with-high-dielectric-and-high-Madsen-Yu/2e0def710ba7656b844f9cd6314c5bdb4b46da0d)

* [Long-term Stretching of Silicone Dielectric Elastomers with Soft Fillers](https://www.advancedsciencenews.com/long-term-stretching-silicone-dielectric-elastomers-soft-fillers/)

* [Soft-hard hybrid filler for silicone based dielectric elastomer: Glycerol and titanium dioxide](https://aip.scitation.org/doi/10.1063/1.5066824)

* [Electromechanical response of silicone dielectric
elastomers](https://iopscience.iop.org/article/10.1088/1757-899X/147/1/012057/pdf)

* [The Current State of Silicone-Based Dielectric Elastomer Transducers](https://backend.orbit.dtu.dk/ws/files/132542416/marc.201500576_1_.pdf)

* [Silicone Elastomers with High‐Permittivity Ionic Liquids Loading](https://onlinelibrary.wiley.com/doi/abs/10.1002/adem.201900481)

* [Silicone elastomers with high dielectric permittivity and high dielectric breakdown strength based on dipolar copolymers](https://www.sciencedirect.com/science/article/abs/pii/S0032386114008623)

* 

### Sketches:
<!-- ![sketch01](img/final/sketch01.jpg#thumbnail) -->

<div style="display:flex">
     <div style="flex:1;padding-right:5px;">
          <img src="../img/final/sketch01.jpg">
     </div>
     <div style="flex:1;padding-left:5px;">
          <img src="../img/final/sketch02.jpg">
     </div>
</div>

### Inspiration:
[![Collapsible Funnel](img/final/funnel.jpg)]([https://www.alibaba.com/product-detail/Portable-food-grade-silicone-folding-funnel_60724683272.html)
[![Collapsible Funnel](img/final/water-bottle.jpg)]([https://www.lovethyplanet.shop/product-page/collapsible-silicone-water-bottle-550ml) 

## Options:

### electrically actuated silicone-ethanol elastomer

* NW/VW for high kV? Can this work? Why hasn't this been used before?

* After performing the research, I believe that the ethanol/silicone/ni-chrome combination would be interesting to test - especially in a multi-chamber application.

* However, I believe that the other forms of dielectric elastomers are not suitable for my project, as their form (needing electrodes on both ends) and high-voltage requirement would be impractical. I'd still be interested in seeing if a NW/VW could sub in as a source of voltage; however, I'm not sure what would serve as the second electrode.

* Research papers:

    * [Soft material for soft actuators](https://www.nature.com/articles/s41467-017-00685-3.pdf)
        <font size="2">
        
        * Describes the combination of ethanol and silicone to create a self-contained electrically-driven soft actuator
        
        * 20% ethanol to 80% platinum cure silicone
        </font>
        <font size="1">
        * We used platinum-catalyzed two-part silicone rubber Ecoflex 00-50 (Smooth-On, PA, USA) as a matrix material and ethanol ≥ 99.5% (Sigma Aldrich, MO, USA) as an active phase change material. Properties of the silicone rubber are shown in Table 1 below. 
        
        * Material preparation involves thorough hand-mixing of 20 vol% of ethanol with silicone elastomer (first with part A for about 2 min, then mixed with part B for about 2 min). The material is ready-to-cast and ready-toprint after the preparation. Room temperature curing of the cast or 3D-printed part takes up to 3 h. 
        
        * A commercially available 0.25 mm diamter Ni-chrome resistive wire was used for electrically driven heating of the artificial muscle (i.e., for the actuation). 
        
        * To comply with the expansion of the actuator material, a helical spiral shape was chosen for the Ni–Cr wire. The wire was hand-wound on an 8 mm screw driver shaft as shown in Supplementary Fig. 6
        </font>
    
    * [Functional properties of silicone/ethanol soft-actuator composites](https://www.researchgate.net/publication/323357056_Functional_properties_of_siliconeethanol_soft-actuator_composites)

    * [Rejuvenation of soft material–actuator](https://www.researchgate.net/publication/323758841_Rejuvenation_of_soft_material-actuator)
    
    <s>
    * [Stretchable, Transparent, Ionic Conductors](https://science.sciencemag.org/content/sci/341/6149/984.full.pdf)

        <font size="2">

        * Uses hydrogel - difficult to replicate, and only really interesting in large-strain, high-speed applications (therefore not this one)
        </font>

    * [A Clear Advance in Soft Actuators](https://citeseerx.ist.psu.edu/viewdoc/download;jsessionid=D041866AC0579CA1F677A6E481C9E286?doi=10.1.1.391.6604&rep=rep1&type=pdf)

        <font size="2">

        * Elaborates on the results of "Stretchable, Transparent, Ionic Conductors" (linked above).

        * "The authors formed DEAs... simply by laminating films of polyacrylamide hydrogels formed with salt water onto the surfaces of dielectric elastomers"
        </font>

    * [Dielectric elastomers (Wikipedia)](https://en.wikipedia.org/wiki/Dielectric_elastomers)

        <font size="2">

        * 
        </font>
    
    * [Hydraulically amplified self-healing electrostatic actuators with muscle-like performance](https://science.sciencemag.org/content/359/6371/61)

        <font size="2">

        * [HASEL actuators with muscle-like performance (YouTube)](https://www.youtube.com/watch?v=M4qcvTeN8k0)

        </font>
    </s>

    * [UCSD engineers developed electrically-controlled soft robot actuators](https://blog.arduino.cc/2019/10/12/ucsd-engineers-developed-electrically-controlled-soft-robot-actuators/)

### water-driven pump silicone vs. air driven?

* Big con - has to have a large external part for the pumps

* [Programmable Air Is an Arduino-Based Air Pump for Your Soft Robotics Projects](https://www.hackster.io/news/programmable-air-is-an-arduino-based-air-pump-for-your-soft-robotics-projects-9945c6ed96aa)


### flat layered mold vs. mold in the round?

* In the round:

    * Examples:

        * [Soft Quadruped Robot](https://learn.adafruit.com/soft-quadruped-robot-glaucus?view=all)

        * [Sculpted Soft Robotic Tentacle](https://www.thingiverse.com/thing:71978)

        * [Silicone Robo-Tentacle/Trefoil Tentacle](https://learn.adafruit.com/silicone-robo-tentacle?view=all#done)

        * [Trefoil Tentacle Quick Guide](https://cdn-learn.adafruit.com/assets/assets/000/011/356/original/tentacle_casting.pdf?1380506859)

    * Resources, etc.:

        * [Cleaning out candle wax](https://www.fcs.uga.edu/extension/stain-removal-candle-wax-paraffin)


* Flat layered:

    * Examples:

        * [Soft robotica: bending mechanism](https://www.youmagine.com/designs/soft-robotica-bending-mechanism)

    * Research papers:
        
        * [Starfish Soft Robot](http://fab.cba.mit.edu/classes/863.12/people/krista.palen/images/week1/StarFishSoftRobot2.pdf)

            <font size="2">
            
            * Five-chamber starfish, five air inlets
                
            * Demonstrates the effects of inflating different chambers

            * [Multigate soft robot](https://www.pnas.org/content/108/51/20400)

            * [Walking / Crawling Soft Robot](https://www.instructables.com/id/Walking-Crawling-Soft-Robot/)
            
            </font>


### collapsible tentacle

* How do you even make a mold for that? I guess just like making the cat?

    * Examples:

        * [Collapsible Cup](https://www.thingiverse.com/thing:3369639)

        * [Extending Soft Robotic Actuator](https://www.instructables.com/id/Extending-Soft-Actuator/) [[Thingiverse](https://www.thingiverse.com/thing:962889)]

### <s>twisted tentacle

* Examples: 

    * [Twisted Origami Box](https://www.youtube.com/watch?v=tVGH5FZHHIU)

    * [Origami Twist Box](https://bookzoompa.wordpress.com/tag/origami-twist-box/)

    * [FLXO](https://arnavwagh.work/flxo)

        * So promising, but unavailable files :C  - I have emailed the creator though so we'll see about that

</s>

# Planning for Execution

* Design a flat layered mold

    * Two chambered - tip and shaft

    * Test two chambered pattern

* Things to consider:
  
    * Microstructures for gripping

    * [Elastic Energy Storage Enables Rapid and Programmable Actuation in Soft Machines](https://onlinelibrary.wiley.com/doi/epdf/10.1002/adfm.201906603)

        * Connect a stretched, chambered silicone structure to a strain limiting layer (silicone infused paper, for example).

        * Allows for rapid expansion <i>and</i> recovery, contrary to normal models

        * Most suited to curled structures, like proboscis'.

        * Not suited for my project since I'm not unfurling the tentacle, but very cool - maybe a future project.


## Version 01: Basic flat air-powered tentacle
This is the first iteration of the tentacle. To make this, I:

### modeled new molds
I based the new models off of the design I had made in [week 02](https://bluet87.gitlab.io/fab-academy/assignments/week-02/). I adjusted the dimensions and generally improved the design. This was modeled in Fusion 360.

![fusion_01](../img/final/fusion_01.jpg)

![fusion_02](../img/final/fusion_02.jpg)

![fusion_03](../img/final/fusion_03.jpg)

As I was machining this, and casting it to be an inflatable, I filleted all internal edges and corners to allow for a more rounded internal bubble shape.

![fusion_04](../img/final/fusion_04.jpg)

![fusion_05](../img/final/fusion_05.jpg)

![fusion_06](../img/final/fusion_06.jpg)

![fusion_07](../img/final/fusion_07.jpg)

![fusion_08](../img/final/fusion_08.jpg)

![fusion_09](../img/final/fusion_09.jpg)

### 3D printed inserts using the Lulzbot Mini
I used the modeled inserts and printed them using the Lulzbot Mini with a .8mm nozzle, as that was the option that was free.

I had some failed prints:
![printing_02](../img/final/printing_02.jpg)

but eventually I had new inserts:
![printing_03](../img/final/printing_03.jpg)

I also redesigned the inserts to have registration keys, which helped increase accuracy when casting the silicone.
![printing_04](../img/final/printing_04.jpg)

### created toolpaths in Fusion 360
I then took my finished molds and passed them to the Manufactering (from the Design) option so I could make the toolpaths. Here, I defaulted to my instructor's expertise, as this was completely novel to me.

First, measure and cut your stock (material you're machining) to size:

![wax_01](../img/final/wax_01.jpg)

![wax_02](../img/final/wax_02.jpg)

Go to the Manufacturing tab and create a new setup for the model you're working on:

![manufactering_00](../img/final/manufacturing_00.jpg)

Set the measurements as the 'box size' on the Stock tab, mode "Fixed size box"

![manufactering_01](../img/final/manufacturing_01.jpg)

In the Setup tab, use the following settings:

![manufactering_02](../img/final/manufacturing_02.jpg)

and set the Model Point Box Point as the highlighted point on the model.

### prepped and CNC'd the molds out of machineable wax
We used the Roland MX-40 to mill the molds out of wax. 

First prep the wax to be attached to the milling plate:

![wax_03](../img/final/wax_03.jpg)

Then run each job in sequence - first do a face job, which levels out the surface of the wax, then do the rough cut followed by the finer cut to refine the details.

Face job:

![wax_06](../img/final/wax_06.jpg)

Milling:
<video controls muted height = "480">
    <source src="../../img/final/wax_04.mp4" type ="video/mp4">
    milling wax
</video>

![wax_05](../img/final/wax_05.jpg)

Finished mold (1 of 3):

![mold_01](../img/final/mold_01.jpg)

### cast the model in silicone and assembled
We unfortunately did not have a vacuum degassing chamber, so the casted silicone had a lot of air bubbles, but it was adequate for this first iteration. I used [Dragon Skin™ 10 FAST](https://www.smooth-on.com/products/dragon-skin-10-fast/) because it was on hand, which is a two-part platinum cure silicone. It is almost the softest silicone available from Smooth-on, which helped with ensuring that the resulting tentacle was adequately flexible.

First I measured the volume of silicone needed by filling the mold with water and dividing the resulting volume by two, as the silicone is two equal parts, A and B.

![casting_01](../img/final/casting_01.jpg)

![casting_05](../img/final/casting_05.jpg)

I thoroughly mixed the two parts together, then slowly poured it from a high height (maybe half a meter) above the mold into the lowest point of the model, in an attempt to minimize bubbles - unfortunately, using FAST, which has a shorter pot life (mixing time before it starts setting) and having such a small mold meant that there were still an abundance of bubbles.

![casting_02](../img/final/casting_02.jpg)

I didn't need to use release spray because the mold is wax, so it releases the silicone very readily.

The first cast came out really well!
![casting_03](../img/final/casting_03.jpg)

I then cast the middle layer, which separates the two air chambers and sides of the tentacle:

![casting_04](../img/final/casting_04.jpg)

![casting_06](../img/final/casting_06.jpg)

I then mixed a very small amount of silicone and applied it on the cured middle layer, and then used it to glue the first layer onto the middle layer. This effectively made the first part a balloon, meaning I could have stopped here and experimented with using this. 

However, I aspired to make a two-chamber tentacle, so I casted the second part and glued them together:

I tried a technique where you place a piece of acrylic over the poured silicone while it's curing, to force it to have a smooth flat surface. It was effective; however it was really easy to trap air bubbles - not sure how I will deal with this going forward.

![casting_08](../img/final/casting_08.jpg)

![casting_09](../img/final/casting_09.jpg)

I trimmed the flashing with wire snips, as they offered the closest cut.=

### tested
We had one air pump in the lab, so I plugged the hole in each chamber onto the outlet to test whether the air chambers inflated as expected.

![testing_01](../img/final/testing_01.jpg)

They worked really well! It was much less flexible than originally expected, due to the solid infill of the opposing side to the air chambers, but they nonetheless successfully inflated.
<video controls muted height = "480">
    <source src="../../img/final/testing_02.mp4" type ="video/mp4">
    inflating the air chambers
</video>

### Takeaways and Future Improvements
Successes:

* The air chambers successfully inflated, meaning that my initial idea for modeling them logically worked out. This was quite exciting, as I've never made a soft robot before, and it having two sides added to the complexity.

Improvements:

* Thinner walls - I had 3mm walls throughout the model, and they turned out to be surprisingly thick and stiff. I will reduce them in future iterations.

* Thinner interior middle layer - it was unnecessarily thick for its purpose at 3mm.

* Hollow out the sealed off air chambers - as each side of the model is designed to have a section that does not have air flow through it, I originally left this to be completely filled with silicone. This turned out to significantly affect the stiffness of the model, preventing the model from moving side to side. I will try having a hollow section that is just sealed off in future iterations.

* Use different silicone - we ordered [Dragon Skin 10 SLOW](https://www.smooth-on.com/products/dragon-skin-10-slow/) for the future interations, as well as [Silicone Thinner](https://www.smooth-on.com/products/silicone-thinner/), which combined yield a lower viscosity mixture that will give air bubbles more time to escape. 

* Use a vacuum degassing chamber - my instructor has placed an order for a vacuum chamber, so I can use it to eliminate the bubbles in the silicion to yield prettier casts.

## Version 02: Air-powered tentacle in the round, with suckers!
I:

### created a new model
This was really difficult and helped me practice a lot of new skills in Fusion 360. As I wanted a more organic shape for the tentacle, I started with using the Sculpt feature. This feature allows you to start with a solid shape and manipulate it, and offers much more organic shapes.

![modeling_01](../img/final/modeling_01.jpg)

I then projected half of the model into a sketch plane, and drew circular suckers.

![modeling_02](../img/final/modeling_02.jpg)

I then used a detailing technique described in this tutorial: [Adding Details While Sculpting in Fusion 360 [Part 2]](https://www.youtube.com/watch?v=5EIJ3re9DKk).

It roughly works like this - first, you extrude the sketched details until they all intersect with the body being modified.

![modeling_03](../img/final/modeling_03.jpg)

Copy the target body and hide the original. Then, use the Combine feature and the Intersect operation, on the target body (tentacle) and the tool bodies (extruded circles) keeping the tool bodies as well.

![modeling_04](../img/final/modeling_04.jpg)

Hide the leftover external tools, select the intersections, and Move them as far out as desired.

![modeling_05](../img/final/modeling_05.jpg)

I then modified each sucker, creating depressions using the Hole feature and Filleting lines until they were rounded to my satisfaction.
![modeling_06](../img/final/modeling_05.jpg)

I then combined all of the sucker-side bodies.

![modeling_07](../img/final/modeling_05.jpg)

And rescaled the entire model unevenly to change the shape, while making sure to not break the earlier sucker-creating actions.

![modeling_08](../img/final/modeling_08.jpg)

### created air pockets
I used a similar design as in my first tentacle, with the similar principle of sucker/bottom air chambers and back/top air chambers, to hopefully be able to manipulate the movement in two directions.

![modeling_09](../img/final/modeling_09.jpg)

First extrude the bubbles outside the modeled body:
![modeling_10](../img/final/modeling_10.jpg)

The tubes were modeled to be smaller and rounded, using the rotation feature:
![modeling_11](../img/final/modeling_11.jpg)

It gets complicated from here:
* Part 1: the half of the model with suckers

* Part 2: the half of the model without suckers (the back)

* Part 3: the half of the model with suckers side, but without the suckers

* Part 4: raw extruded air chambers, exceeding the limits of the original model

* Part 5: the shells of both sides, ignoring the suckers, using a 1.5mm wall

Use the shells (part 5) to Cut the model halves (parts 2 and 3), ignoring the suckers

I then took half of the model and made it into a shell with a 1.5mm wall thickness. 
![modeling_16](../img/final/modeling_16.jpg)

I used this to cut the original model to make the shape of the interior, where the air chambers would be (part 6).

I Intersected the extruded chambers (part 4) with the new guidelines of the interior of the tentacle (part 6).

As a result, I had the air chambers but they were shaped to fit inside the curved tentacle (part 7).
![modeling_18](../img/final/modeling_18.jpg)


### determined the five parts to be manufactured
The final parts to be cast are:
* Back half

* Sucker half

* Back air chambers

* Sucker air chambers

* <s>Middle dividing wall (2mm)</s>

### created toolpaths using the Manufacturing extension



### milled a mold out of machinable wax

### 3D printed an insert for my mold

### cast the sucker side in silicone
As preparation for casting, I first filled the mold with the insert and then water, to measure the volume of silicone needed:

![casting_01](../img/final/casting_01.jpg)

I made a support for the insert so it would consistently be aligned correctly within the mold and wouldn't float or otherwise move. For the back, I plan to improve this design by introducing holes for keying.

![casting_02](../img/final/casting_02.jpg)

The general process for casting was:
* measure out parts A and B

* using hot glue, attach the mixing cup into the vacuum degasser

* mix A and B thoroughly in the mixing cup

* degas the silicone, repeatedly creating and releasing the vacuum until no more bubbles are forming

* pour silicone slowly, in a thin stream, into the mold

* align and place the insert in

* place a piece of acrylic over the supports and place weights (full containers of silicone in this case)

* wait for ~2/3 of the recommended curing time, then unmold. The silicone is between 1.5-4mm thick, so it cures faster. An easy check is to see if the thickest bit of unused silicone in the mixing cup is cured.

#### iteration 1
Color: Translucent
Silicone Type: Dragon Skin FAST
Additives: N/A
Flaws: Air bubbles in suckers, on sides

First cast, used degassed Dragon Skin FAST without pigment. Somehow mixed up too little silicone, despite mixing a bit more than was anticipated from the water displacement test, so it was a little shallow:
![casting_03](../img/final/casting_03.jpg)

I discovered that I needed to remove the insert from the alignment structure to remove the whole thing from the mold.

![casting_04](../img/final/casting_04.jpg)

The silicone peeled off quite well; however, there was at least one imperfection as an air bubble had gotten trapped in one of the smaller suckers.

![casting_05](../img/final/casting_05.jpg)

<video controls muted height = "480">
    <source src="../../img/final/casting_06.mp4" type ="video/mp4">
    wiggling the first iteration of the tentacle
</video>

#### iteration 2
Color: Translucent deep purple
Silicone Type: Dragon Skin SLOW 
Additives: N/A
Flaws: Air bubbles in inside

Used pigmented Dragon Skin SLOW this time to see if the lowered viscousity and longer pot life would help remove bubbles.

Degassing:

![casting_07](../img/final/casting_07.jpg)

I mixed too much silicone this time when trying to compensate, so there was a lot of excess silicone after I pushed the insert in. I also mixed in the purple [Silc Pig](https://www.smooth-on.com/products/silc-pig/) pigment, adjusting the amount by dipping the end of a safety pin into the pigment to control the amount, as it is incredibly concentrated.

![casting_08](../img/final/casting_08.jpg)

It still had a few bubbles, but far less than the first iteration, so I went for another iteration.

#### iteration 3
Color: Opaque lavender 
Silicone Type: Dragon Skin FAST 
Additives: Thinner (very small amount)
Flaws: Air bubbles in suckers

I used pigmented Dragon Skin FAST mixed with [Silicone Thinner](https://www.smooth-on.com/products/silicone-thinner/), which lowers the viscosity, improves coverage of intricate details (such as the smaller suckers), and increases the pot life of mixed silicone. This all makes degassing easier. It also lowers the shore hardness of the cured silicone, making it softer, but easier to tear.

I put very little thinner in this time, as I was unfamiliar with it, and I tried degassing for a long period of time, but the very short pot life (8 min standard) of the FAST silicone meant that the increased degassing time allowed it to set up too much before I was able to pour it into the mold, so this one had more bubbles than either previous iteration.

![casting_09](../img/final/casting_09.jpg)

I used purple and white pigment this time to create a mostly opaque cast.

This is an image of iterations 1, 2, and 3 

![casting_10](../img/final/casting_10.jpg)

And the backs, where you can see all of the excess silicone (to be trimmed off with a very sharp pair of scissors) as well as the individual air chambers.

![casting_11](../img/final/casting_11.jpg)

#### iteration 4
Color: Translucent purple pink
Silicone Type: Dragon Skin FAST
Additives: Thinner (more that i3)
Flaws: Air bubble on suckers, on side, small tears when unmolding

This time, I used Dragon Skin FAST again, but a significantly more amount of thinner. I should have been measuring the thinner by weight, as it is advised to never exceed 10% of the weight of A and B together; however, I merely used a very small spoon when measuring into the cup for part A. At this scale of silicone (very little being used, very thin cured results), I suspected that factors such as residue left in the cups after scraping, or amount left on the stirring sticks, would skew the results.

I mixed this one using a very small amount of red, purple, and white pigment.

![casting_12](../img/final/casting_12.jpg)

Unfortunately, this one had less bubbles, but still had bubbles and also developed a very small tear while unmolding - not surprising because the amount of thinner was increased, but unfortunate.

#### iteration 5
Color: Translucent pale pink
Silicone Type: Dragon Skin SLOW
Additives: N/A
Flaws: Air bubbles in sides

This time, I switched back to Dragon Skin SLOW, no thinner, just a longer degassing period. After I degassed the original mixture in the mixing cup, I carefully poured it into the mold, and then placed the mold in the vacuum degasser to eliminate any remaining bubbles. SLOW has a pot life of 75 minutes, nearly 10x of FAST, so I had plenty of time to remove and/or pop bubbles.

I had previously not degassed in the mold because I was concerned about the initial signficant increase in volume that occurs in the beginning of degassing, as the walls of the mold were quite shallow so I suspected it would overflow the mold. However, since this had such a longer pot life, I could afford the time to degas twice.

![casting_13](../img/final/casting_13.jpg)

#### iteration 6
Color: Opaque pink
Silicone Type: Dragon Skin FAST 
Additives: Thinner (very small amount)

#### iteration 7
Color: Translucent blue
Silicone Type: Dragon Skin FAST 
Additives: Thinner (very small amount)
Flaws: Significant tear, too-thin walls, air bubbles in sides

#### iteration 8
Color: Opaque blue
Silicone Type: Dragon Skin FAST 
Additives: N/A
Flaws: Small unmolding blemish on a sucker

#### iteration 9
Color: Translucent blue and pink
Silicone Type: Dragon Skin SLOW
Additives:  N/A
Flaws: Air bubbles in sides

#### iteration 10
Color: Opaque blueish purple
Silicone Type: Dragon Skin FAST
Additives:  N/A
Flaws: Air bubbles in sides

#### iteration 11
Color: Tight marbled black and white
Silicone Type: Dragon Skin SLOW
Additives:  N/A
Flaws: Tear in side

### Current testing, Sucker side:

#### i3 + backing
Layers: 2 thick before glueing
Result: Not very flexible, top air chamber inflated too much.

#### i6 + backing
Layers: 0 thick before glueing
Result: Top air chamber inflated too much due to leaking inside the air channels, inflated fairly evenly on both sides.
Cut open channel to top chamber and glued shut with Sil-poxy for version 2

Layers: 1 cured on tentacle before glueing
Result:

### cast the back side in silicone
#### iteration 1
Color: Opaque light blue
Silicone Type: Dragon Skin FAST
Additives: N/A
Flaws: Air bubble in inside of head
Patched with Sil-poxy

### Current testing, Back side:

#### iteration 1
Color: Opaque light blue
Silicone Type: Dragon Skin FAST
Additives: N/A
Flaws: Air bubble in inside of head
Patched with Sil-poxy

#### iteration 2
Color: Opaque periwinkle
Silicone Type: Dragon Skin FAST
Additives: N/A
Flaws: N/A

#### iteration 3
Color: Marbled black and white
Silicone Type: Dragon Skin SLOW
Additives: N/A
Flaws: Shallow sides

#### iteration 4
Color: Opaque teal
Silicone Type: Dragon Skin FAST
Additives: N/A
Flaws: Cosmetic

### problems and room for future improvement
I took a long time to have a sense of mastery over the whole process, and as you can see, documented above, I had many problems with degassing my silicone adequately, adjusting the thickness/set time, and mold accuracy. I fixed them through repeated iterations, including things like trialing different times and materials to use, and recreated the inserts to have registration keys, which greatly improved mold accuracy.

### final outcomes:
![casting_16](../img/final/casting_16.jpg)

![casting_15](../img/final/casting_15.jpg)

<video controls muted height = "480">
    <source src="../../img/final/final_01.mp4" type ="video/mp4">
    Inflating one side
</video>

<video controls muted height = "480">
    <source src="../../img/final/final_02.mp4" type ="video/mp4">
    Inflating one side
</video>

### Files
[3D designs and Cura files, .f3d and .stl formats](../img/week15/files.zip)