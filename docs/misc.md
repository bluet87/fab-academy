# TEST

* [multi-part jellyfish silicone mold](http://fab.cba.mit.edu/classes/863.12/people/gbernal/week16.html)

* [casting and molding with wax](http://archive.fabacademy.org/2018/labs/barcelona/students/dorota-orlof/assignments/assignment09/)

* [Design of a Variable Stiffness Flexible Manipulator with Composite Granular Jamming and Membrane Coupling](http://fab.cba.mit.edu/classes/865.18/motion/jamming/jamming-varistiff.pdf)

* [Jamming Actuators](http://fab.cba.mit.edu/classes/865.18/motion/jamming/index.html)

* [SOFTlab](https://softlabnyc.com/)

* [dielectric actuators](http://fab.cba.mit.edu/classes/865.18/motion/dielectric/index.html)

* [ready to crawl printed robot](https://www.youtube.com/watch?time_continue=7&v=G0-lJKOgTuo&feature=emb_title)

* [Neri Oxman - Gemini cocoon chair](https://www.prnewswire.com/news-releases/neri-oxman-unveils-gemini-cocoon-like-acoustic-chaise-produced-with-stratasys-color-multi-material-3d-printing-252616981.html)

* [mediated matter MIT research group](https://www.media.mit.edu/groups/mediated-matter/projects/)

* [christine liu portfolio - cool wearables, jellyfish](http://alumni.media.mit.edu/~cml/portfolio/)

* [charlotte's FabA page](http://fabacademy.org/2020/labs/charlotte/students/elaine-liu/assignments/week05/)

* [Soft Computer Logic for Soft Robots](https://www.asme.org/topics-resources/content/soft-computer-logic-for-soft-robots)

* Instructables

    * [Soft Robots: Make an Artificial Muscle Arm and Gripper](https://www.instructables.com/id/Soft-Robots-Make-An-Artificial-Muscle-Arm-And-Gri/)

    * [Soft Robotic Mask](https://www.instructables.com/id/APOSEMA/)

        * [soft robotics expansion](https://www.adimeyer.com/aosematechnical)


    * [RE-Inflatable Vest](https://www.instructables.com/id/RE-Inflatable-Vest/)
     
    * [Laserweld Your Own Inflatables](https://www.instructables.com/id/Laserweld-Your-Own-Inflatables/)

    * [Bubble Wall | Interactive, Inflatable Panel ](https://www.instructables.com/id/Bubble-Wall-Interactive-Inflatable-Panel/)


* [InterFACE inflatable mask](http://archive.fabacademy.org/archives/2017/fablabbcn/students/51/proyecto.html)

* [Towards vision-based robotic skins: a data-driven, multi-camera tactile sensor](https://arxiv.org/pdf/1910.14526.pdf)

* [origami simulator](http://www.amandaghassaei.com/projects/origami_simulator/)

* [Flapping origami crane](http://fab.cba.mit.edu/classes/863.10/people/jie.qi/jieweek10.html)


