### Research papers:

* [(The promise of) Monstrous Kinship? Queer Reproduction and the Somatechnics of Sexual and Racial Difference](https://www.euppublishing.com/doi/abs/10.3366/soma.2018.0250)
    * Nope, focused on reproduction
    

* [Articulated Hands: Force Control and Kinematic Issues](http://fab.cba.mit.edu/classes/865.15/classes/measurement/articulated-hands.pdf)
    <font size="2">
    <!-- * test -->
    </font>

* [Multigait soft robot](https://www.pnas.org/content/108/51/20400)

* [A Resilient, Untethered Soft Robot](https://www.researchgate.net/publication/275945257_A_Resilient_Untethered_Soft_Robot)

* [Design, fabrication and control of soft robots](https://www.researchgate.net/publication/277410991_Design_fabrication_and_control_of_soft_robots)
    <font size="2">
    <!-- * test -->
    </font>

* [Jellyfish-Inspired Soft Robot Driven by Fluid Electrode Dielectric Organic Robotic Actuators](https://www.frontiersin.org/articles/10.3389/frobt.2019.00126/full)

* [Soft Robotics: Review of Fluid-Driven Intrinsically Soft Devices; Manufacturing, Sensing, Control, and Applications in Human-Robot Interaction: Review of Fluid-Driven Intrinsically Soft Robots](https://www.researchgate.net/publication/318140258_Soft_Robotics_Review_of_Fluid-Driven_Intrinsically_Soft_Devices_Manufacturing_Sensing_Control_and_Applications_in_Human-Robot_Interaction_Review_of_Fluid-Driven_Intrinsically_Soft_Robots)

* [Translucent soft robots driven by frameless fluid electrode dielectric elastomer actuators](https://robotics.sciencemag.org/content/3/17/eaat1893)

* [Edible Soft Robotics](https://media.ccc.de/v/33c3-8113-edible_soft_robotics)

* [Glycerol as high-permittivity liquid filler in dielectric silicone elastomers](https://www.researchgate.net/publication/305461636_Glycerol_as_high-permittivity_liquid_filler_in_dielectric_silicone_elastomers)

* [Fabrication Process of Silicone-based Dielectric Elastomer Actuators](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4781713/)

* [Silicone elastomers with high dielectric permittivity and high dielectric breakdown strength based on tunable functionalized copolymers](https://www.semanticscholar.org/paper/Silicone-elastomers-with-high-dielectric-and-high-Madsen-Yu/2e0def710ba7656b844f9cd6314c5bdb4b46da0d)

* [Long-term Stretching of Silicone Dielectric Elastomers with Soft Fillers](https://www.advancedsciencenews.com/long-term-stretching-silicone-dielectric-elastomers-soft-fillers/)

* [Soft-hard hybrid filler for silicone based dielectric elastomer: Glycerol and titanium dioxide](https://aip.scitation.org/doi/10.1063/1.5066824)

* [Electromechanical response of silicone dielectric
elastomers](https://iopscience.iop.org/article/10.1088/1757-899X/147/1/012057/pdf)

* [The Current State of Silicone-Based Dielectric Elastomer Transducers](https://backend.orbit.dtu.dk/ws/files/132542416/marc.201500576_1_.pdf)

* [Silicone Elastomers with High‐Permittivity Ionic Liquids Loading](https://onlinelibrary.wiley.com/doi/abs/10.1002/adem.201900481)

* [Silicone elastomers with high dielectric permittivity and high dielectric breakdown strength based on dipolar copolymers](https://www.sciencedirect.com/science/article/abs/pii/S0032386114008623)

* 
