# Sonic Sweater

<iframe width="560" height="315" src="https://www.youtube.com/embed/cBrt7TKU5Q0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Learning Diary: Classes
### Week 13: Introduction to the course and guest lectures (29.3-31.3)
This week, we were introduced to the course, had guest lecturers, saw previous coursework examples, and learned how to make paper speakers and sonify them via Arduino Unos.

Of particular interest was the organorgan project by Liisa Pesonen, the performance/costumes with the living sprouts in the pieces.

We made paper speakers by coiling copper wire and sandwiching it between two pieces of tape - I used this tutorial: [Paper Speakers](https://www.instructables.com/Paper-Speakers-1/). We had to test a hypothesis by changing one variable, so I used the same amount of wire and tape for each speaker, but coiled them tighter or looser.

![paper_speakers_01](../img/wearable-tech-2021/paper_speakers_01.jpg)

We hooked them up to the Arduinos, and they worked (!!) but were too quiet to detect whether the two speakers sounded different from each other. Valtteri said something about using an amplifier, but we didn't for this preliminary try.

I'd really like to try something with folding or open-closed structures, some sort of 3D textile, like what Emmi showed us.

We also made simple light-up gloves with a simple LED-Resistor-Switch-Battery circuit. I ran out of conductive thread partway through the project, so I started using copper wire. Rather tricky to use, I wouldn't suggest it, but it was interesting to try. Mine lit up if you made the V/Victory/Peace hand sign, as demonstrated in the last video.

![led_gloves_01](../img/wearable-tech-2021/led_gloves_01.jpg)

![led_gloves_02](../img/wearable-tech-2021/led_gloves_02.jpg)

![led_gloves_03](../img/wearable-tech-2021/led_gloves_03.jpg)

![led_gloves_04](../img/wearable-tech-2021/led_gloves_04.jpg)

![led_gloves_05](../img/wearable-tech-2021/led_gloves_05.jpg)

<video controls muted height = "480">
    <source src="../img/wearable-tech-2021/LED_gloves_06.mp4" type ="video/mp4">
    A video showing closeups of the components.
</video>

<video controls muted height = "480">
    <source src="../img/wearable-tech-2021/LED_gloves_07.mp4" type ="video/mp4">
    A video demonstrating the interaction.
</video>

I am intimidated by the prospects of the final project - I'm familiar with interactive wearables, but to make a performative concept alongside that is more new to me.

Our project group is: Ellen Virman (sound designer), Elisa Avikainen (costume designer), and me (new media, minor in costume design). 

### Week 14: Starting the sweater (7.4-9.4)

#### 7.4 
Today was mostly ideation - our current proposal is to make a sweater that listens to what the wearer is saying and modifies it before repeating it back via a speaker. There will be soft sensors on the sweater that affect the effects distorting the statement, and they aren't necessarily going to be predictably/linearly mapped to reactions - we're trying to avoid turning the sweater into a keyboard/button interface. 

#### 8.4
Today we experimented with making soft sensors using tutorials from [Kobakant](https://www.kobakant.at/DIY/?cat=26).

Elisa made a [pom-pom tilt sensor](https://www.kobakant.at/DIY/?p=2409) and a [stroke sensor](https://www.kobakant.at/DIY/?p=792):

![pompom_tilt_sensor_01](../img/wearable-tech-2021/pompom_tilt_sensor_01.jpg)

<video controls muted height = "720">
    <source src="../img/wearable-tech-2021/pompom_tilt_sensor_02.mp4" type ="video/mp4">
    A video demonstrating the interaction.
</video>

<video controls muted height = "480">
    <source src="../img/wearable-tech-2021/stroke_sensor_01.mp4" type ="video/mp4">
    A video demonstrating the interaction.
</video>

I worked on a [fabric potentiometer](https://www.kobakant.at/DIY/?p=543), which used stretchy conductive fabric that changed resistance when stretched for the circle and normal woven conductive fabric for the 'dial':

![potentiometer_01](../img/wearable-tech-2021/potentiometer_01.jpg)

Ellen worked on a [neoprene bend sensor](https://www.kobakant.at/DIY/?p=20) and looked into what boards we can use - current verdict is on a [Teensy](https://www.pjrc.com/store/teensy35.html) with a special [audio shield](https://www.pjrc.com/store/teensy3_audio.html) meant to go with it, but we can't find one right now. Will probably have to order one. The Teensy supports audio much better than the Arduino, and has a really good [sound library](https://www.pjrc.com/store/audio_tutorial_kit.html) to go with it so it seems like a good choice.

#### 9.4
Elisa got us a sweater today! We chose this one because it already has a design on it, and it fits the cozy aesthetic we want for this sweater.

![sweater_01](../img/wearable-tech-2021/sweater_01.jpg)

Valtteri led a workshop on networking using an [Arduino Nano 33 BLE](https://store.arduino.cc/arduino-nano-33-ble) and Max on a separate computer. This was clearly taught, but it was still super confusing for me, since it was my first run-in with networking. It was super cool though, using IP addresses and Wi-fi to connect wirelessly and transmit data from sensors into Max to generate sound. I hope I can use wireless communication in a future project, although probably not with Max.

Using a bend sensor to modify the brightness of an LED:

<video controls muted height = "480">
    <source src="../img/wearable-tech-2021/bend_sensor_led_01.mp4" type ="video/mp4">
    A video demonstrating the interaction.
</video>

Using the sensors we made, connected to the arduino, to control the values in Max which was running on a different computer.

<video controls height = "480">
    <source src="../img/wearable-tech-2021/networking_01.mp4" type ="video/mp4">
    A video demonstrating the interaction.
</video>

<video controls height = "480">
    <source src="../img/wearable-tech-2021/networking_02.mp4" type ="video/mp4">
    A video demonstrating the interaction.
</video>

![max_01](../img/wearable-tech-2021/max_01.jpg)

### Week 15: Continuing the sweater (12.4-16.4)

#### 12.4
I made a bunch of conductive [pom-poms](http://www.inklingsandyarns.com/2012/05/how-to-make-the-perfect-pom-pom/) today by using normal yarn mixed with conductive thread, following Elisa's example.

![pompom_01](../img/wearable-tech-2021/pompom_01.jpg)

<video controls muted height = "480">
    <source src="../img/wearable-tech-2021/pompom_test_01.mp4" type ="video/mp4">
    A video demonstrating the interaction.
</video>

It was mostly a crafting and experimenting day - I worked on helping Elisa with making sensors and Ellen with Teensy troubleshooting.

Stroke sensor construction:

![stroke_01](../img/wearable-tech-2021/stroke_01.jpg)

Elisa hard at work embellishing the sweater:

![elisa_01](../img/wearable-tech-2021/elisa_01.jpg)

Ellen hard at work programming the Teensy:

![ellen_01](../img/wearable-tech-2021/ellen_01.jpg)

We found out that you need to connect all of the pins between the audio shield and the Teensy, not just the ones that appeared to be in use in the example - something I should've caught, but totally missed. Good to know though!

The sweater by the end of the day - notice all of the additions in pink, blue, and purple!

![sweater_02](../img/wearable-tech-2021/sweater_02.jpg)

#### 13.4
Today, we learned from Liisa how to make [Sugru](https://sugru.com/buy/sugru-mouldable-glue?msclkid=575254eeaa791f35cae0346c1c5892c2&utm_source=bing&utm_medium=cpc&utm_campaign=Sugru-BA-Global-Brand-Search%20EM&utm_term=sugru&utm_content=Global-Brand%20%5B1%3A1%5D)-reinforced snap button wire connectors to make detachable, durable, and soft connections. 

First, you strip the ends of a silicone-coated stranded wire. Twist one end into a circle a little smaller than the size of your button snap, and twist the other into a smaller circle. Using a separate copper wire, 'sew' the larger circle onto the back of the snap. 

![button_connector_01](../img/wearable-tech-2021/button_connector_01.jpg)

![button_connector_02](../img/wearable-tech-2021/button_connector_02.jpg)

Then reinforce the ends of the wires, before the circles, with Sugru to secure the twisted connections and protect them from bending. This is better than soldering, since that is brittle when flexed, and better than heatshrink over solder because that is a significantly stiffer connection (and less secure on the button snap end).

![button_connector_03](../img/wearable-tech-2021/button_connector_03.jpg)

We finished the day by assembly-line making all of the sensor-side connector wires!

![making_connections_01](../img/wearable-tech-2021/making_connections_01.jpg)

We also struggled a lot with the Teensy to Audio shield connection, as we did not have the correct headers. We improvised connections between the audio shield and the Teensy with a mess of jumper wires:

![teensy_monster_01](../img/wearable-tech-2021/teensy_monster_01.jpg)

Unfortunately, after a lot of testing, we discovered that while the connections between boards was stable, the connection to the Teensy itself was shaky, which is why our sensor readings were super erratic.

![testing_connections_01](../img/wearable-tech-2021/testing_connections_01.jpg)

We also tested the mic and speaker that we borrowed, and Elisa crocheted a little cover for the mic so it would blend into the rest of the sweater (the pink pom-pom at the top):

![testing_mic_hat_01](../img/wearable-tech-2021/testing_mic_hat_01.jpg)


#### 14.4
We started the day with installing the special headers for the Teensy and the Audio shield at Fab Lab, which took a lot of time because we kept on getting cold solder joints. We figured out that changing the soldering iron to a newer one with a smaller tip, changing the solder from .2mm to 1mm thickness, and adding lots of flux helped fix our soldering joints.

![soldering_01](../img/wearable-tech-2021/soldering_01.jpg)

I also made all of the Teensy-side snap wire connections using the same method as yesterday.
Testing the sensors on the sweater - the pom-pom was half of a button connection, switching the output in the Serial Monitor on the screen between 0 and 1.

<video controls muted height = "480">
    <source src="../img/wearable-tech-2021/testing_connections_02.mp4" type ="video/mp4">
    A video showing the pom-pom button working.
</video>

We finished the day with experimenting with the audio connection after testing the sensors on the sweater. Ellen led the programming side of this with the assistance of Valtteri, and successfully started using the Teensy Audio programming interface with the Teensy and the mic! Here's the simple button circuit that we were using to test different effects:

![testing_buttons_01](../img/wearable-tech-2021/testing_buttons_01.jpg)

#### 15.4
Today we finished the extra cables for the bend sensor, Elisa knit a special sleeve for the sensor, and Ellen and I worked on coding for all of the button sensors!

Cable management:

![cables_01](../img/wearable-tech-2021/cables_01.jpg)

We first drew out what sensors were where on the sweater, and what they would do:

![sweater_diagram_01](../img/wearable-tech-2021/sweater_diagram_01.jpg)

![sweater_diagram_02](../img/wearable-tech-2021/sweater_diagram_02.jpg)

Then we made a tree to figure out the coding logic:

![coding_diagram_01](../img/wearable-tech-2021/coding_diagram_01.jpg)

And then we started working out the logic on paper, before transferring it to the computer - I find it easier to do some pseudocoding first, with simplified syntax, to sort out logic before editing and cleaning it up.

![pseudocode_01](../img/wearable-tech-2021/pseudocode_01.jpg)

We made some pretty significant progress - we coded delay and speed effects for one pom-pom sensor, and we coded the stroke sensor to act as a switch - tomorrow, we will add the other sensors!

<video controls height = "720">
    <source src="../img/wearable-tech-2021/sweater_testing_01.mp4" type ="video/mp4">
    A video demonstrating the interaction.
</video>

<video controls height = "480">
    <source src="../img/wearable-tech-2021/sweater_testing_02.mp4" type ="video/mp4">
    A video demonstrating the interaction.
</video>

#### 16.4
Today we finished adding sensors and cables to the sweater, soldered the snap leads to the Teensy, and worked more on the code.

The sweater looking good:

![sweater_03](../img/wearable-tech-2021/sweater_03.jpg)

The inside:

![sweater_04](../img/wearable-tech-2021/sweater_04.jpg)

More pseudocode - we implemented all of the pompom buttons, and worked on getting the stroke sensor to act as a switch between the sides of the sweater. We also added the code for the bend sensor.

![pseudocode_02](../img/wearable-tech-2021/pseudocode_02.jpg)

Last time on the breadboard:

![teensy_monster_02](../img/wearable-tech-2021/teensy_monster_02.jpg)

And adding the audio jack and the snap leads to the Teensy:

![teensy_final_01](../img/wearable-tech-2021/teensy_final_01.jpg)

Last things for Monday: fixing an issue in the code and adding shoulderpads!

### Week 16: Finishing and presenting the sweater! (19.4)

#### 19.4
Fixed the code! There needed to be more if statements and another save-state to preserve the state of the switch, but I managed it in time.

Elisa finished installing the Teensy and the battery inside the shoulderpads - no small feat, the Teensy was quite bulky by then and it involved careful design by her, but it got installed, taking the sweater from an ordinary sweater to one with very protective padding.

![shoulderpads_01](../img/wearable-tech-2021/shoulderpads_01.jpg)

![final_sweater_01](../img/wearable-tech-2021/final_sweater_01.jpg)

There were last second demo problems of course, we think due to the speaker running out of battery, but we recharged it and it appeared to function fine for the demo.

And the final presentation of all of the features!

<iframe width="560" height="315" src="https://www.youtube.com/embed/cBrt7TKU5Q0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### References:
Materials List:

    Soft parts:
    Woolen sweater

    Soft Sensors:
    Woolen yarn
    Conductive yarn
    Sewing yarn
    Velostat plastic
    Neoprene

    Wiring:
    White silicone wire
    Sugru
    Snap buttons

    Audio: 
    Lavalier microphone
    Portable speaker
    Miniplug cable
    Miniplug female connector

    Teensy parts:
    Teensy 3.5 microcontroller
    Teensy audio shield
    Connector headers
    USB-wire
    Portable power source

    Software:
    Arduino
    Teensyduino
    For testing:
    Breadboard
    Buttons
    Light sensors
    Jump wires
    10k Ohm resistors

    Tools:
    Sewing needle
    Crochet needle
    Knitting needles
    Soldering iron
    Solder
    Flux
    Wire strippers
    Wire cutters
    Scissors

[Final code, .ino file](../img/wearable-tech-2021/wearable-sweater-2021.ino)

