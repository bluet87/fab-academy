# Week 03: Computer-Controlled Cutting
_Group assignment: characterize your lasercutter's focus, power, speed, rate, kerf, and joint clearance_

_Personal assignment: cut something on the vinylcutter, design, lasercut, and document a parametric construction kit, accounting for the lasercutter kerf, which can be assembled in multiple ways,and for extra credit include elements that aren't flat_

This week, I:

### characterized the laser cutter's with my group:
We have an Epilog Laser 36T, 60W laser cutter in our Fab Lab. The manual is available [here](https://www.epiloglaser.com/downloads/pdf/ext_4.22.10.pdf)

* The type of laser is CO2 Laser 60W.

* The cutting area is 914 x 610mm.

* The maximum height is 305mm.

* It supports the following file formats: .ai, .cdr, .pdf, and .svg.

It supports the following line weights when engraving:

![engraving01](../img/week03/engraving_01.jpg)

The typical workflow for using the laser cutter  to cut is:

Software: 

1. Save your vectorized image as an .ai file and bring to the laser cutter
2. Select all lines to be cut and set the stroke weight to 0.01mm and the color to R:255 (true red).
3. Open the print dialog, go to Setup, and go to Preferences to open the Epilog Dashboard
4. Select the Job Type, set the settings according to your local [guide](https://wiki.aalto.fi/display/AF/Laser+Cutter+Settings), correct the Piece Size, select Auto Focus, and Print. 
5. Select Print on the Preferences window, tick the "Ignore artboard" checkbox, ensure that the preview reflects a good place to print from, and select print.

At the laser:

* Follow this step-by-step [guide](https://wiki.aalto.fi/display/AF/Laser+Cutter+Epilog+Legend+36EXT). 

#### focus
You can set the laser cutter to automatically focus the laser by ticking this box on the Epilog Dashboard:

![focus01](../img/week03/focus_01.jpg)

It focuses at the first point of cutting. There is also an option to manually focus, which is available in the manual.

#### kerf
[Oskar](https://fabacademy.koli.io/) modeled a test for measuring kerf in [OpenSCAD](https://www.openscad.org/).

![kerf01](../img/week03/kerf_01.jpg)

which included nine 20mm squares. We cut the file and measured each square (on both edges) and averaged the results, to come up with a measurement of 19.875mm. Subtracting that from the original spec of 20mm yielded a <b>kerf</b> of <b>0.125mm</b> - this means that when you're cutting a shape out, you need to add 0.125mm to the exterior of the shape to have an accurate cutout, and when you're cutting a hole out, you need to make the hole 0.125mm smaller all around to have an accurate cutout.

#### joint clearance
Oskar performed a joint clearance test using this file, cut out of 4mm plywood:

![joint01](../img/week03/joint_01.jpg)

The settings were 20 speed/75 power/500 frequency.

These were his findings:

* 0.15mm = Attachable by hand. Very loose.

* 0.1mm = Attachable by hand. Stays together but not permanent

* 0.07mm = Attachable by hand with difficulty. Stays together, hard to take apart.

* 0.01mm = Attachable by hand with difficulty. Stays together, hard to take apart.

He mentioned that a low negative clearance could be used to get a permanent press-fit joint that would need to be hammered together.

### made a 2D design to be cut on the vinyl cutter
I used an existing .png file from the internet, as my illustration skills leave much to be desired, and then used [Illustrator](https://www.adobe.com/products/illustrator.html) to vectorize it.

[![octopus01](../img/week03/octopus_01.jpg)](https://www.cleanpng.com/png-octopus-free-content-cuteness-clip-art-free-octopu-581138/preview.html)

I started by importing the image into Illustrator. I then went to Window > Image Trace, and changed the Mode to Color and the Colors to 2 (number of colors in the image). 

![octopus02](../img/week03/octopus_02.jpg)

I then went to Object > Image Trace > Expand to reveal the new paths:

![octopus03](../img/week03/octopus_03.jpg)

![octopus04](../img/week03/octopus_04.jpg)

and saved it as an .ai file.

### cut my design on the vinyl cutter
The instructor guided us through using the vinyl cutter - I don't think I could replicate it at this time, but with a bit more practice I'm sure it'd be fine.

<video controls muted height = "480">
    <source src="../../img/week03/octopus_05.mp4" type ="video/mp4">
   A video of the vinyl cutter at work!
</video>

### used a heatpress for pressing vinyl
Here is the vinyl, fresh out of the cutter. Good lighting is imperative for seeing the super-fine cuts. 

![octopus06](../img/week03/octopus_06.jpg)

Pull away the vinyl gently, at a diagonal angle and keeping the vinyl very close to the surface to minimize accidents.

![octopus07](../img/week03/octopus_07.jpg)
![octopus08](../img/week03/octopus_08.jpg)

There was excess vinyl on the sheet, so we moved the home/origin of the cutter and cut the design out again.

![octopus09](../img/week03/octopus_09.jpg)

The two designs:

![octopus10](../img/week03/octopus_10.jpg)

Putting the design on the heatpress - first, you have to let it warm up for quite a few minutes. A temperature gun can be helpful for checking in on it. You first lay the material down, press it with the heatpress for a few seconds, then lift it up to check if it's flat. Straighten as needed, test again, and then lay the vinyl protective-side-up.

![octopus11](../img/week03/octopus_11.jpg)

You peel the protective layer the same way that you peel excess vinyl off.

![octopus12](../img/week03/octopus_12.jpg)

The finished shirt!

![octopus13](../img/week03/octopus_13.jpg)

### modeled a press-fit parametric construction kit to be cut on the laser cutter
I used parametric modeling for creating the sketch, and was sure to have only three set measurements - base square size, thickness of material, and kerf of laser. Every other measurement was based off of these three variables to ensure that it would handle being modified well.

I sketched it first:

![pressfit01](../img/week03/pressfit_01.jpg)

Extruded it by the thickness and filleted the corners:

![pressfit02](../img/week03/pressfit_02.jpg)

Projected it to a new sketch, and offset it all by the kerf:

![pressfit03](../img/week03/pressfit_03.jpg)

![pressfit04](../img/week03/pressfit_04.jpg)

Used the rectangular pattern tool to duplicate the design:

![pressfit05](../img/week03/pressfit_05.jpg)

Modified it in Illustrator for the laser cutter (stroke width to .01mm, color to R:255):

![pressfit07](../img/week03/pressfit_07.jpg)

End result:

![pressfit08](../img/week03/pressfit_08.jpg)

![pressfit09](../img/week03/pressfit_09.jpg)

In the end, something was off - I think I offset in the wrong direction somewhere, so some of the press-fits worked and some didn't, but it was an interesting exercise nonetheless.

### <b>Bonus:</b> modeled a 2D living hinge to be cut on the laser cutter
For this assignment, I was interested in making a variation on a living hinge, and in researching living hinges, I discovered illustrator templates, which were interesting but not parameterized (and not made in-house). Therefore, I decided to make my own!

Example:

![livingHinge01](../img/week03/livingHinge_01.jpg)

I decided to make a square version of this cut, as a triangle seemed too complex for me, geometry-wise.

#### Fusion 360 User Interface
This went poorly. I was fairly confident in setting parameters, using them to dimension lines, and using them in formulas, but when I tried to re-size the parameters, everything failed.

Creating 1/8th of the design, using parameters:
![fusion01](../img/week03/fusion_01.jpg)

Mirroring the design:
![fusion02](../img/week03/fusion_02.jpg)

Using the Circular Pattern tool to create a full square:
![fusion03](../img/week03/fusion_03.jpg)


<b>Note:</b> while going through the files for documentation again, I've discovered that the sketch I made responded fairly well to parameter changes - not sure anymore what made me switch to Python, but at least I've started a new skill?

![fusion04](../img/week03/fusion_04.jpg)

![fusion05](../img/week03/fusion_05.jpg)


#### Fusion 360, scripting in Python
This has been incredibly challenging, as I have no experience with scripting for Fusion 360 and nearly no experience with Python, but I followed this tutorial: [Getting Started with Python Scripting and the Fusion API](https://www.youtube.com/watch?v=Roe2TRBY6Ko), which was very helpful. That said, a lot of getting to where I am involved Googling and experimentation when faced with opaque error messages.

I used [PyScripter](https://sourceforge.net/projects/pyscripter/), at the recommendation of the tutorial, and found it quite useful. It was easier than using, say, Visual Studio since it let you run scripts real-time, so testing the initial calculations was very clear.

![pyscripter01](../img/week03/pyscripter_01.jpg)

Although I found the math to be tricky, it wasn't terribly complicated, and I eventually worked it out through repeated hand calculations and sketches. When I converted my initial code into code for Autodesk, however, I kept on running into issues.

This one in particular was difficult for me to figure out, but it turned out that it's essentially an ArrayOutOfBounds error. 

![pyscripter02](../img/week03/pyscripter_02.jpg)

My final code looks like this:

![pyscripter03](../img/week03/pyscripter_03.jpg)

I found out, unfortunately, that [the Fusion 360 API does not support the CircularPattern command for 2D sketches - only for 3D models](https://forums.autodesk.com/t5/fusion-360-api-and-scripts/how-do-i-create-circular-pattern-in-sketch-using-api/td-p/5794949).

Although the provided link had a solution in the form of transformations, I couldn't figure out how to modify my code accordingly, so it currently only generates 1/4 of the square, which then needs to be CircularPatterned in the Fusion 360 UI.

![fusion06](../img/week03/fusion_06.jpg)

### cut my design on the laser cutter
I cut my design out of a scrap piece of material - I initially thought it was very sturdy cardboard, but on reflection it seems to be MDF.
I used the guidelines for 2mm cardboard, as this was 1.7mm thick, and it didn't cut through. I increased the speed, it still didn't cut through. Finally, at the advice of the instructor, I used the settings for 2mm plywood (38/60/500), which succeeded (although there are toast marks on the back of the material).


<video controls muted height = "480">
    <source src="../../img/week03/lasercuts_01.mp4" type ="video/mp4">
    A video demonstrating its flexibility
</video>

Here you can see me iterating through different parameters - too small and too inflexible, too large and still inflexible, and just right :)

![lasercuts02](../img/week03/lasercuts_02.jpg)

Here's the 'just right' version in action! Making this involved a bit of fiddling in the Fusion 360 UI after generating the initial quarter - CircularPattern, and making sure that the outside edge was solid. I'd say that this attempt was mostly worth it, but the final result needs working on. If I have time, I can continue improving the program, but for now this is where I'll leave it.

<video controls muted height = "480">
    <source src="../../img/week03/lasercuts_03.mp4" type ="video/mp4">
    A video demonstrating its flexibility
</video>

### Files
[PressFit file in Fusion 360, .f3d format](../img/week03/PressFit.f3d)

[PressFit file for the laser cutter, .ai format (note: this does not perfectly pressfit)](../img/week03/PressFit.ai)

[Vectorized 2D image for the vinyl cutter, .ai format](../img/week03/Octopus.ai)

[Inspiration file for living hinge, .dxf format](../img/week03/TriangleHinge.dxf)

[Calculations for the square, .py format](../img/week03/SquareHingeCalculations.py)

[Final script for Fusion 360, .py format](../img/week03/SquareHinge.py)

[Cleaned result of the scripted hinge, .f3d format](../img/week03/SquareHingeSimulation.f3d)

[Parametric square hinge in Fusion 360, .f3d format](../img/week03/SquareLivingHinge.f3d)