# Week 02: Computer-Aided Design
_Assignment: model (raster, vector, 2D, 3D, render, animate, simulate, ...) a possible final project, compress your images and videos, and post it on your class page_

This week, I:

### explored using Illustrator as a vector software
In week 03, I used Illustrator to vectorize an image to be cut on a vinyl cutter. That can be found [here](week-03.md#made-a-2d-design-to-be-cut-on-the-vinyl-cutter).

### sketched and scanned in more designs for my [final project](../final.md) idea
![sketch02](../img/week02/sketch_02.jpg)

![sketch03](../img/week02/sketch_03.jpg)

### processed scanned images using image manipulation software
I used [GIMP](https://www.gimp.org/), an image editor that I had no experience with, to process and clean up the sketches I made. While I have no prior history of image manipulation, I was able to perform simple manipulations to update the sketches.

First, I desaturated the photos to bring them from color to grayscale:
![gimp01](../img/week02/gimp_01.jpg)

Then, I adjusted the brightness/contrast to sharpen the images:
![gimp02](../img/week02/gimp_02.jpg)

I then straightened and cropped them, and ran them through [ImageMagick](https://imagemagick.org/) to compress the images. I used some settings from my experience in Week 01, and just adjusted resolution depending on how large the original image was:

![imagemagick01](../img/week02/imagemagick_01.jpg)
![gimp03](../img/week02/gimp_03.jpg)
![gimp04](../img/week02/gimp_04.jpg)
![gimp05](../img/week02/gimp_05.jpg)
![gimp06](../img/week02/gimp_06.jpg)
![gimp07](../img/week02/gimp_07.jpg)

I have not created a batch command for ImageMagick yet, as I haven't decided how I want to determine what end resolution I want images to be based off of their original size, but it would be helpful to determine this in the future.

I also had a little fun with editing the color curves for a screenshot I took - to be determined whether the color scheme persists, but it was an interesting exercise in how altering the curve for different color channels affected the image.
From:
![gimp08](../img/week02/gimp_08.jpg)

To:
![gimp09](../img/week02/gimp_09.jpg)
![gimp10](../img/week02/gimp_10.jpg)

Since, for the most part (outside of rendering) the colors used don't matter that much for my model, it might be fun to color my experience with an alternative color scheme.

### created a 2D base model 
I used [Autodesk Fusion 360](https://www.autodesk.com/products/fusion-360/overview) to make a digital, vectorized version of my initial sketch. I was able to approximate measurements from my initial design and transfer them into Fusion by using the Modify->Change Parameters tool.

![fusion01](../img/week02/fusion_01.jpg)

![fusion02](../img/week02/fusion_02.jpg)

![fusion04](../img/week02/fusion_04.jpg)

![illustrator02](../img/week02/illustrator_02.jpg)

### created a model using 3D modeling software
I have primarily used [Autodesk Meshmixer](http://www.meshmixer.com/) to edit 3D models in the past; however, I had almost no experience with creating 3D models from the ground-up, so this assignment was very challenging for me. 

#### Blender
I initially chose to use [Blender](https://www.blender.org/) to sculpt a tentacle, as it offered sculpting tools similar to [Sculptris](https://pixologic.com/sculptris/) and [ZBrush](http://pixologic.com/features/about-zbrush.php), which I thought would be helpful for creating the more organic shape of a tentacle. I had no prior experience with sculpting softwares, and found it difficult to control symmetry/the actual molding of the outside - here is an example of me experimenting with the tools in Blender.
![blender](../img/week02/blender.jpg)

I would have continued working in Blender, as I'm sure that the controls would become more intuitive as I adjusted to it; however, I went back to my research on soft robots and found that the design of them was much more optimal in CAD softwares like Fusion 360, due to the very parametric, non-organic structure of the insides. Thus, I changed plans and switched to [Autodesk Fusion 360](https://www.autodesk.com/products/fusion-360/overview), which I had a little experience with from a previous class.

#### Autodesk Fusion 360
Fusion 360 supports parametric modeling, which was very helpful for setting the size and shape of the model I wanted to create. 

I created a sketch using the parameters I set, then used the Mirror tool to create a symmetric version of the sketch.
![fusion03](../img/week02/fusion_03.jpg)

I then offset the sketch to create the walls, rounded off the corners with **fillet**, finished the sketch and extruded it.
![fusion05](../img/week02/fusion_05.jpg)

This completes the mold for the middle layer of my tentacle.

To be continued with molds for the other layers...

A mold for casting a mold for casting the 'bubble' layer for the top section of the tentacle, with a parameterized tube for the internal tubing:
![fusion06](../img/week02/fusion_06.jpg)

A mold for casting a mold for casting the 'bubble' layer for the bottom section of the tentacle, with a parameterized tube for the internal tubing:
![fusion07](../img/week02/fusion_07.jpg)

### Files
[Illustrator Vector](../img/week02/illustrator_01.dxf)

[Fusion 360 Model, .f3d format](../img/week02/TentacleLayersv7.f3d)

[Fusion 360 Model, .iges format](../img/week02/TentacleLayersv7.iges)