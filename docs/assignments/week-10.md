# Week 10: Applications and Implications 

My final project is going to be a fun, wearable soft robotic tentacle. 

### What will it do?
It will move via expansion and contraction in response to sensors attached to the wearer.

### Who's done what beforehand?
I have found very little prior research on this particular method of actuating soft robots. I have only found these two papers, which are by the same team:

* [Soft material for soft actuators](https://www.nature.com/articles/s41467-017-00685-3.pdf)
        <font size="2">
        
      * Describes the combination of ethanol and silicone to create a self-contained electrically-driven soft actuator
        
        * 20% ethanol to 80% platinum cure silicone
        </font>
        <font size="1">
        * We used platinum-catalyzed two-part silicone rubber Ecoflex 00-50 (Smooth-On, PA, USA) as a matrix material and ethanol ≥ 99.5% (Sigma Aldrich, MO, USA) as an active phase change material. Properties of the silicone rubber are shown in Table 1 below. 
        
        * Material preparation involves thorough hand-mixing of 20 vol% of ethanol with silicone elastomer (first with part A for about 2 min, then mixed with part B for about 2 min). The material is ready-to-cast and ready-toprint after the preparation. Room temperature curing of the cast or 3D-printed part takes up to 3 h. 
        
        * A commercially available 0.25 mm diamter Ni-chrome resistive wire was used for electrically driven heating of the artificial muscle (i.e., for the actuation). 
        
        * To comply with the expansion of the actuator material, a helical spiral shape was chosen for the Ni–Cr wire. The wire was hand-wound on an 8 mm screw driver shaft as shown in Supplementary Fig. 6
        </font>
    
    * [Functional properties of silicone/ethanol soft-actuator composites](https://www.researchgate.net/publication/323357056_Functional_properties_of_siliconeethanol_soft-actuator_composites)

There has been a lot of other work with specifically dielectric elastomers, which are somewhat similar in that they're not pump-powered; however, I believe that other forms of dielectric elastomers are not suitable for my project, as their form (needing electrodes on both ends) and high-voltage requirement would be impractical. I'd still be interested in seeing if a negative ion generator could sub in as a source of voltage; however, I'm not sure what would serve as the second electrode.

### What will you design?
I will design the mold for casting the tentacle in, a PCB for driving the nichrome wire, and a case for containing the PCB and the battery.

### What materials and components will be used?
AND 

### Where will they come from?
AND

### How much will they cost?

| <b>Part</b>      | <b>Component</b>                                                       | <b>Price</b>   | <b>Source</b>                                                                                                                                                                    |
|----------------|------------------------------------------------------------|---------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Mold:          | Machinable wax, or possibly 3D printed PLA                 | 10      | Aalto Fab Lab                                                                                                                                                             |
| Tentacle body: | Platinum cure Silicone: Ecoflex 00-50 (Smooth-On, PA, USA) | 44      | https://www.kaupo.de/shop/en/SILICONE-RUBBER-Platinum-Cure/ECOFLEX-SERIES/Ecoflex-00-30/                                                                                  |
|                | Ethanol (≥ 99.5% purity)                                   | Unknown | Aalto Biofilea Lab                                                                                                                                                        |
|                | Ni-chrome resistive wire (0.25 mm diamter)                 | 10      | https://www.amazon.com/IIVVERR-Nichrome-0-25mm-Heater-calentador/dp/B07ZCXN29Q/ref=sr_1_2?dchild=1&keywords=nichrome+wire+.25&qid=1587148773&s=electronics&sr=1-2-catcorr |
| PCB:           | Microcontroller - possibly an ATtiny45, tbd                | 2       | https://www.digikey.com/product-detail/en/microchip-technology/ATTINY45-20SU/ATTINY45-20SU-ND/735466                                                                      |
|                | FR-1                                                       | 5       | Aalto Fab Lab                                                                                                                                                             |
| Battery:       | Li-po battery (size tbd)                                   | 30      | internet, price is very dependent on capacity                                                                                                                             |
| Sensor:        | Pressure-sensitive resistive sensor (tbd) OR               | 5       | internet, price depends on sensor                                                                                                                                         |
|                | Temperature sensor (tbd) OR                                | 5       | internet, price depends on sensor                                                                                                                                         |
|                | Some other fun sensor, depends on how the motion turns out | 5       | internet, price depends on sensor                                                                                                                                         |
| Case:          | Fabric OR                                                  | 15      | Eurokangas                                                                                                                                                                |
|                | PLA OR                                                     | 5       | Aalto Fab Lab                                                                                                                                                             |/
|                | Acrylic                                                    | 10      | Aalto Fab Lab                                                                                                                                                             |

<b>Sources:</b> [Fab-lab inventory](https://fablab.aalto.fi/machines/materials), [biofilia lab](https://www.aalto.fi/en/biofilia), [Eurokangas](https://www.eurokangas.fi/), [Digi-Key](https://www.digikey.com/)

### What parts and systems will be made?
Every 'part' listed above.

### What processes will be used?
CNC milling, 3D printing (potentially), mold making and casting, 2D design, embedded programming, electronic design.

### What questions need to be answered?
* How do we actually work with the silicone-ethanol mixture? 

* How do we keep the circuit small, discrete, and wearable?

* How do we design the tentacle mold?

* How will we source certain materials (ethanol)?

### How will it be evaluated?
* Functionality - does it work?

* Construction quality - is it well constructed? 

* Finishing - does it look nice?