# Week 18: Project Development
_Personal assignment: Complete your final project, tracking your progress_

For my final project, I made a two-part set of wearable accessories, in the form of a necklace and a bracelet. The entire process and further details, including notes on the other class that this project was involved in, are provided on the [Wearable Technology page](../wearable-technology-final.md).

![electronics_19](../img/wearable-technology-final/electronics_19.jpg)

![electronics_20](../img/wearable-technology-final/electronics_20.jpg)

![electronics_21](../img/wearable-technology-final/electronics_21.jpg)

![electronics_22](../img/wearable-technology-final/electronics_22.jpg)

![electronics_23](../img/wearable-technology-final/electronics_23.jpg)

### Concept
A two-part set of wearables, F.O.S. is designed to increase intimacy between two people by adding an element of synchrony to their biology, without needing the touch or immediate presence of the other party.

The two-part system consists of:

* Neckpiece: primarily provides feedback to the Bracelet. Can transfer the wearer's heartbeat to the Bracelet in the form of a blinking light and vibrations.

* Bracelet: primarily receives information from the Neckpiece.

## A timeline:
Please refer to the [Project Diary](../wearable-technology-final.md#project-diary) for comprehensive documentation of my final project and the progress I made on it.