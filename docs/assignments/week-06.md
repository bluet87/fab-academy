# Week 06: Electronics Design
_Group assignment: use the test equipment in your lab to observe the operation of a microcontroller circuit board_

_Personal assignment: redraw an echo hello-world board, add (at least) a button and LED (with current-limiting resistor), check the design rules, make it, and test it, extra credit: simulate its operation_

This week, I:

### re-designed an echo hello-world board using KiCad
[KiCad EDA](https://www.kicad-pcb.org/) is a "Cross Platform and Open Source Electronics Design Automation Suite". We used the schematic editor and the PCB layout designer to design our boards.

This is what the schematic editor looks like:

![kicad01](../img/week06/kicad_01.jpg)

You begin by first putting all components into the schematic, by clicking this symbol on the right bar:

![kicad02](../img/week06/kicad_02.jpg)

After you've placed all of the symbols, representing components, you must label all of the connections. When the schematic is ported over to the PCB, it will connect all matching labels with each other - so you would designate one pin on the microcontroller (here, pin 7 on an ATtiny412-SS) as 'LED', and label a wire coming off of the LED symbol as 'LED'. 

![kicad03](../img/week06/kicad_03.jpg)

After placing and labeling all of your components, you must run the Annotation tool:

![kicad04](../img/week06/kicad_04.jpg)

![kicad05](../img/week06/kicad_05.jpg)

and then the Electrical Rules Checker

![kicad06](../img/week06/kicad_06.jpg)

to ensure that all connections are valid. 

You then generate a netlist by clicking on these two buttons, which will be saved as a separate file.

![kicad07](../img/week06/kicad_07.jpg)

![kicad08](../img/week06/kicad_08.jpg)

Lastly, you click the PCB button to move onto the next step.

![kicad09](../img/week06/kicad_09.jpg)

### designed a double-sided PCB using KiCad
When you enter the PCB editor, it is blank.

![kicad10](../img/week06/kicad_10.jpg)

You then import the netlist that you generated earlier and Update PCB.

![kicad11](../img/week06/kicad_11.jpg)

It will first appear as an absolute mess. That's okay.

![kicad12](../img/week06/kicad_12.jpg)

By dragging the components apart, you will start to see some sense. Each white line marks where a trace needs to connect the two component parts.

![kicad13](../img/week06/kicad_13.jpg)

I eventually connected all of my parts; however, as an extra challenge, we made our PCB designs double-sided.

Top side:

![kicad14](../img/week06/kicad_14.jpg)

With the bottom side added - the circles are 'vias', which are a copper path cutting through the board so it can form connections between the two sides.

![kicad15](../img/week06/kicad_15.jpg)

We then flooded the bottom side, so there would be a very large 'ground' plate.

![kicad16](../img/week06/kicad_16.jpg)

We added an Edge Cut to mark out the borders of the PCB design, and then added a Dwg (Drawing) layer on top of it - this is the file that is used to designate the actual boundaries of the board when it is milled.

![kicad17](../img/week06/kicad_17.jpg)

![kicad18](../img/week06/kicad_18.jpg)

Lastly, we added margins. The margin needed to by symmetrical across one axis - I chose the Y-axis - so when the board is milled once and flipped, the designs will still match up. The margins are also important for when importing the designs into mods.

![kicad19](../img/week06/kicad_19.jpg)

We then exported the design as SVG files

![kicad20](../img/week06/kicad_20.jpg)

![kicad21](../img/week06/kicad_21.jpg)

![kicad22](../img/week06/kicad_22.jpg) 

And passed them to mods. To get the vias to show up for mods, you need to take the file for the front copper traces, import it into Inkscape

![mods01](../img/week06/mods_01.jpg) 

Select any point on the black traces and delete them. When you highlight over the (seemingly) blank image, you will see the holes designated for the vias.

![mods02](../img/week06/mods_02.jpg) 

Fill them in with black, then save as a separate SVG file.

![mods03](../img/week06/mods_03.jpg) 

### milled and stuffed a double-sided PCB
The milling process has remained the same as specified in [week 04](week-04.md); however, the order has slightly changed to accomodate for the vias. First mill the traces(.4mm bit). Then mill the vias(.8mm bit). Then mill the outline(.8mm bit).

![milling01](../img/week06/milling_01.jpg) 

![milling02](../img/week06/milling_02.jpg)

After completing the top side, carefully remove the board, flip it along the axis you previously used (Y-axis for me), and place it back in the empty spot, centering it as best as you can. Then you may run the file for the back traces.

![milling03](../img/week06/milling_03.jpg)

To stuff the board, first start with the vias. They're very tiny copper tubes with a flared end on one side, and are gently tapped into each hole and subsequently deformed and soldered to ensure that they connect both sides of the board effectively.

![stuffing01](../img/week06/stuffing_01.jpg)

Gently but firmly first deform the back end of the via with the pointed tool and a rubber mallet, and then

![stuffing02](../img/week06/stuffing_02.jpg)

flatten it with the other tool.

![stuffing03](../img/week06/stuffing_03.jpg)

Reinforce each via with a bit of solder, test the connections, and you are on to stuffing with the components!

![stuffing04](../img/week06/stuffing_04.jpg)

This was my completed board:

![stuffing05](../img/week06/stuffing_05.jpg)

### tested my new board
My board had a button and three LEDs, building off of the original echo hello-world design. 

To test the board, I first plugged the FTDI cable from a USB port to the connector on the board. I then used my USB to UPDI programmer board to connect the UPDI pins on my new board.

![programming01](../img/week06/programming_01.jpg)

To program my board, I needed to do several installations. 

I first installed [Python 3.7.7](https://www.python.org/downloads/windows/). I then installed [pyserial](https://pypi.org/project/pyserial/) using this command:

            pip install pyserial

I then installed [pyupdi](https://github.com/mraardvark/pyupdi)

            pip install https://github.com/mraardvark/pyupdi/archive/master.zip

I also installed the [megaTinyCore](https://github.com/SpenceKonde/megaTinyCore) boards for the Arduino IDE. I used the [blink program](http://academy.cba.mit.edu/classes/embedded_programming/t412/hello.t412.blink) and brought it into the Arduino IDE. In the IDE, you need to change the board to the ATtiny412 board:

![programming02](../img/week06/programming_02.jpg)

Then Verify/compile the code, but do not upload it anywhere. We want the .hex file that is generated, which we will upload to the hello-board via our USB to UPDI programmer.

            pyupdi.py -d tiny412 -b 19200 -c COM7 -f C:\Users\bluet87\Documents\Arduino\build\412blink.ino.hex -v

However, this is where everything failed. I received the following error messages:

![programming03](../img/week06/programming_03.jpg)

After some debugging, my teacher spotted my mistake - I had stuffed the board incorrectly, hence why I couldn't program my board. 

![programming04](../img/week06/programming_04.jpg)

### did not fix my board in favor of milling a new one for future assignments
At the time of this assignment, I didn't have the adequate tools to resolder my board. As a result, I resolved to improve my design (and be more careful) for future boards, but this one I will be leaving unfinished. See [week 11: Output Devices](week-11.md) for my latest attempt at having a programmable board.

### used a multimeter to determine the resistance of a hand-made variable resistor (week 16)
Please refer to [week 16](week-16.md#created-a-pressure-sensor) to see a video of me using a multimeter to determine the resistance of a pressure-sensitive variable resistor. 


### Files
[PCB design, .kicad_pcb format](../img/week06/embedded_programming.kicad_pcb)

[Schematic, .sch format](../img/week06/embedded_programming.sch)

[Front traces, .svg format](../img/week06/embedded_programming-F_Cu.svg)

[Back traces, .svg format](../img/week06/embedded_programming-B_Cu.svg)

[Vias, .svg format](../img/week06/embedded_programming-Via.svg)

[Margin, .svg format](../img/week06/embedded_programming-Dwgs_User.svg)

[All KiCad and Mods files, .zip format](../img/week06/Double-sided_board.zip)

* Note: the .rml files are functionally meaningless unless you use the same bit size as I did, but I thought I'd include them anyway