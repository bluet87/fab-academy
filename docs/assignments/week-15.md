# Week 15: Molding and Casting
_Group assignment: review the safety data sheets for each of your molding and casting materials, then make and compare test casts with each of them_

_Personal assignment: design a mold around the stock and tooling that you'll be using, mill it (rough cut + (at least) three-axis finish cut), and use it to cast parts_

This week, I:

### reviewed safety data sheets for multiple molding and casting materials
TBD WITH ANSSI

### designed a positive of the desired model
I used this week to work towards my final project, and designed a new model.

Please refer to [here](../final.md#created-a-new-model) and [here](../final.md#created-air-pockets) to find the details on how I designed the model.

### designed a (negative) mold to be milled out of machineable wax
This was fairly simple - I measured the block of machineable wax we were using, then created a block in Fusion 360 to those dimensions. I then centered my model on the surface, and used the Combine->Cut function to cut the model out of the block, thereby creating a negative mold of my model.

### made rough cut and finishing toolpaths for the Roland MDX-40A
Please refer to [here](../final.md#created-toolpaths-in-Fusion-360) 

### milled the mold using the CNC
Please refer to [here](../final.md#prepped-and-CNC'd-the molds-out-of-machineable-wax)

### 3D-printed an insert for the mold to stay true to my design
Please refer to [here](../final.md#3D-printed-inserts-using-the-Lulzbot-Mini)

### cast the desired model in different versions of silicone
Please refer to [here](../final.md#cast-the-sucker-side-in-silicone)

### problems and room for future improvement
Please refer to [here](../final.md#problems-and-room-for-future-improvement)

### final outcomes:
![casting_16](../img/final/casting_16.jpg)

![casting_15](../img/final/casting_15.jpg)

<video controls muted height = "480">
    <source src="../../img/final/final_01.mp4" type ="video/mp4">
    Inflating one side
</video>

<video controls muted height = "480">
    <source src="../../img/final/final_02.mp4" type ="video/mp4">
    Inflating one side
</video>

### Files
[3D designs and Cura files, .f3d and .stl formats](../img/week15/files.zip)