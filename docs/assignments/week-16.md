# Week 16: Wildcard Week
_Personal assignment: Design and produce something with a digital fabrication process (incorporating computer-aided design and manufacturing) not covered in another assignment, documenting the requirements that your assignment meets, and including everything necessary to reproduce it. Possibilities include (but are not limited to) composites, textiles, biotechnology, robotics, folding, and cooking._

This week, I:
focused on using E-textiles and other flexible materials to create sensors.

### Created a pressure sensor
Following this tutorial [DIY Soft Pressure Sensor](https://www.instructables.com/id/Flexible-Fabric-Pressure-Sensor/),  I made a variable resistor, in this case a pressure sensor, out of non-conductive fabric, 3D mesh, conductive fabric, Velostat, and snap buttons. 

Velostat is a pressure-sensitive, piezo-resistive plastic - when it is pressed, it reduces the resistance of the material. It's often used in applications where a flexible resistor is desired, and is very easy to work with - it can be cut with scissors, and can also be heat-sealed.

Here are the components laid out - as you can see, the outside layer (bright orange, top two) are the largest, followed by the mesh (paler orange, bottom right) and the Velostat (black plastic, bottom left), and then conductive fabric (silvery, top two). 
![pressure_01](../img/week16/pressure_01.jpg)

They were assembled in this order: Outer - conductive - Velostat - mesh - conductive - Outer, and then sewn together around the edges, being careful to not sew over the velostat, into this sensor:
![pressure_02](../img/week16/pressure_02.jpg)

I added the mesh to the instructions after reading this paper:

* [Flexible and stretchable fabric-based tactile sensor](https://www.sciencedirect.com/science/article/pii/S0921889014001821)
    <font size="2">

    * "we decided on a design that uses 4 layers of different plain and conductive fabrics, which ensured good elasticity of the compound sensor"

    * "<b>an additional non-conductive meshed layer was added between the middle piezoresistive layer and one of the electrode layers</b>. Sensor sensitivity was found to depend on the thickness of the meshed layer and on the size of the mesh openings, with larger openings and thinner layers producing better sensitivity to first touch"

    </font>

which had the effect of increasing sensitivity of the sensor.

To connect the sensor to removable leads, I used button snaps - one half to pierce through the conductive fabric, and one half assembled and soldered to wire leads. This is a method of attaching wires to conductive fabric, as you cannot solder to conductive fabric, so a mechanical join is quite effective and ideal.
![pressure_03](../img/week16/pressure_03.jpg)

I used a multimeter to measure the pressure sensitivity, which determined that it was working!

<video controls muted height = "480">
    <source src="../../img/week16/multimeter_01.mp4" type ="video/mp4">
   Resistance varying as pressure is applied to the sensor
</video>

### Created a Galvanic Skin Response sensor
"A GSR sensor allows us to measure sweat gland activity, which is related to emotional arousal. To measure GSR, we take advantage of the electrical properties of the skin. Specifically, how the skin resistance varies with sweat gland activity, i.e. the greater sweat gland activity, the more perspiration, and thus, less skin resistance." [How does a GSR sensor work?](https://www.tobiipro.com/learn-and-support/learn/GSR-essentials/how-does-a-gsr-sensor-work/). You do this by attaching two electrodes to the skin, and then measure conductance by applying a very low constant voltage to the two electrodes, and then measuring the current that flows through. 

I unfortunately forgot to document this very well, but I essentially followed the tutorial here: [DIY GSR Sensor](http://ftmedia.eu/diy-gsr-sensor/). The link now appears to be semi-functional, so I'll detail how it is created here:

First, I created two electrodes by wrapping US pennies in conductive fabric, and then attaching button snaps and wire leads to them. I then assembled the following circuit:

![gsr_02](../img/week16/gsr_02.jpg)

to create the sensors, which are visible in the pink box here:

![gsr_01](../img/week16/gsr_01.jpg)

### Takeaways
While I enjoy the flexibility of being able to create my own sensors, I found working with e-textiles a little difficult - the button snap system, while effective, adds bulk to the sensor, and can be fragile if the wire is not secured very well. I fixed the wire by twisting it around the half of a snap before assembling it, and then soldering it to itself, but you can't solder directly to a snap. The connection can also be less stable if it is subject to movement, as a classmate found when she used many snaps on a flexible accessory where it depended on the snaps being able to rotate. I wish it was possible to solder directly to conductive fabric, but that is currently not a thing. It can also be subject to fraying around the edges if it is not sealed by attaching it to an iron-on adhesive, which can lead to tricky debugging if a stray fiber causes a short.

Conductive thread, while not detailed here, has also been difficult to work with - it is a little more fussy than normal thread, due to the metal in the thread, and can have much more resistance than normal solid-core or stranded wire. While this is fine for shorter connections, it can severely interfere with longer connections as the resistance increases over distance. It is also subject to the fraying issue.

I will likely use E-textiles again, as I like to work with flexible materials and being able to sew a sensor on is very useful; however, there are definitely caveats that make me prefer using very small sensors. That said, they are often washable! Which is a very big benefit when making wearables.