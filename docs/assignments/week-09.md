# Week 09: Input Devices
_Group assignment: probe an input device's analog levels and digital signals_

_Personal assignment: measure something: add a sensor to a microcontroller board that you have designed and read it_

This week, I:

### read a datasheet for a sensor
Our instructor provided us with a list of sensors available, and I decided to use the Capacitive pressure sensor ([DPS310XTSA1CT-ND](https://www.digikey.com/products/en?keywords=DPS310XTSA1CT-ND)). [Data Sheet](https://www.infineon.com/dgdl/Infineon-DPS310-DS-v01_00-EN.pdf?fileId=5546d462576f34750157750826c42242).

I realized that the supply voltage for this sensor was 3.6V, which was at odds with how I was powering the ATtiny412-SS via a 5V USB to FTDI cable.

![datasheet01](../img/week09/datasheet_01.jpg)

I got the pinout diagram and opted for the SPI 4-wire connection, which was helpfully detailed here:

![datasheet02](../img/week09/datasheet_02.jpg)

![datasheet03](../img/week09/datasheet_03.jpg)

And got the footprint and symbol [here](https://app.ultralibrarian.com/details/0C6C6944-CACA-11E9-B85E-0AD2C9526B44/Infineon/DPS310XTSA1?ref=digikey).

### added a sensor to my hello-echo board schematic
This is my final schematic:

![schematic01](../img/week09/schematic_01.jpg)

This was super difficult, due some wiring errors and a lack of understanding regarding the intricacies of the power flags/how power input vs output worked in KiCad. I had to put in five 0 OHM resistors as well, to act as jumpers because we were trying to mill this as a one-sided board.

### designed a new PCB with a sensor
This was incredibly painful, and involved lots and lots of back-and-forth. In the end, I had to add the five 0 ohm resistors mentioned above to the design, and had to place each one by experimenting, having a design fail, editing the schematic, importing the new netlist and testing the new design again, and so on until I finally solved the design.

![pcb01](../img/week09/pcb_01.jpg)

### milled my PCB using a Roland MDX-40A
See [week 11](week-11.md#milled-the-board-using-the-roland-mdx-40a) for details on milling the board. 

![milling01](../img/week09/milling_01.jpg)

![milling02](../img/week09/milling_02.jpg)

![milling03](../img/week09/milling_03.jpg)

### used reflow soldering to attach the sensor
My instructor kindly worked very hard to use the reflow solder technique to attach my sensor to the board, since it required it - meaning, it didn't have any legs, it just had contact pads underneath the sensor.

To do the reflow technique, you need a flux pen, solder, tweezers, a microscope, and a heat gun with a narrow nozzle (and tons of patience!)

* First, flux and tin the board contacts

* Tin the pads underneath the sensor

* Carefully align the sensor on top of the contacts and use tweezers to hold it in place

* Direct the airflow from the heat gun from a small distance (5-8cm, for instance) onto the sensor, making sure to keep the sensor in place and gently sweep the air back and forth, to minimize potential melting/heat damage.

This video demonstrates the process, as well as the difficulty of getting it to work. In the end, he succeeded, but it took many, many attempts.

You will also notice that the sensor looks quite deformed from the heat by the end - if the board had been successfully completed, I would've had to check if the sensor was too damaged to use or not.

<video controls muted height = "480">
    <source src="../../img/week09/reflow_01.mp4" type ="video/mp4">
   Reflow soldering
</video>

### partially stuffed the board
Once again, the construction of the board was foiled by unfortunate circumstances. I attached a few resistors at first, and then realized a trace was never completely milled. 

![stuffing01](../img/week09/stuffing_01.jpg)

![stuffing02](../img/week09/stuffing_02.jpg)

The circled part is where I tried using a blade to clean up the trace, but I couldn't manage it with the (house) supplies that I had, and couldn't access the lab again. As a result, I stopped progress on this board, with the resolve to get all of this working on the output board, as it will also contain an input factor. See [week 11: Output Devices](week-11.md) for my latest attempt at having a programmable board.

In addition to this, I suspect the sensor may have been too damaged by the repeated re-heating, based off of the cosmetic damage. Hence, I decided to use a different input sensor going forward.

### Files
[PCB design, .kicad_pcb format](../img/week09/input_sensor.kicad_pcb)

[Schematic, .sch format](../img/week09/input_sensor.sch)

[Traces, .svg format](../img/week09/input_sensor-F_Cu.svg)

[Margin, .svg format](../img/week09/input_sensor-Dwgs_User.svg)

[All KiCad and Mods files, .zip format](../img/week09/input_sensor.zip)

* Note: the .rml files are functionally meaningless unless you use the same bit size as I did, but I thought I'd include them anyway