# Week 05: 3D Scanning and Printing
_Group assignment: test the design rules for your 3D printer(s)_

_Personal assignment: design and 3D print an object (small, few cm3, limited by printer time)that could not be made subtractively, 3D scan an object (and optionally print it)_

This week, I:

### designed an object to be 3D printed
I read about print-in-place ball joints from [this website](https://pinshape.com/blog/learn-to-add-ball-joints-to-your-3d-printed-parts/) and found them really intriguing, as well as possibly a good test of the intricacies of the 3D printer capabilities, so I designed two small models based off of the example.

First, I used parameters, as parametric modeling seems to be the way to go:

![balljoint01](../img/week05/balljoint_01.jpg)

I then used them to make a parametric sketch:

![balljoint02](../img/week05/balljoint_02.jpg)

Which I then copied to another sketch, and altered it to work well with the Revolve tool by slicing it in half:

![balljoint03](../img/week05/balljoint_03.jpg)

I then used the Revolve tool to generate a 3D model from my sketch:

![balljoint04](../img/week05/balljoint_04.jpg)

And exported the models as .stl files using Fusion 360's Export function.

### sliced the models for printing using Cura LulzBot Edition
To print a 3D model, the file must first be 'sliced' and converted into a file format that the printer can interpret. In the case of the printer that we were using ([Lulzbot Mini](https://www.lulzbot.com/store/printers/lulzbot-mini)), we used [Cura LulzBot Edition](https://www.lulzbot.com/cura) to slice the .stl files into .gcode files.

The interface for Cura looks like this:

![cura01](../img/week05/cura_01.jpg)

I checked 'Generate Support', which generates filler plastic under overhangs to prevent them from collapsing onto the printer bed, then switched views from Solid View to Layer View to preview the sliced model. By using the vertical slider, you can view how the printer will print the model, layer by layer. By using the horizontal slider, you can watch how the printer head will 'travel' while depositing the material. 

<video controls muted height = "480">
    <source src="../../img/week05/cura_02.mp4" type ="video/mp4">
   A video of the sliced model in Cura
</video>

The software helpfully provides a time estimate of how long printing will take - this was a very small model, only 13.8 x 13.8 x 15.9mm, and using about 1g of plastic, and it took 18 minutes, as a demonstration of how long printing takes. I then saved the model to be exported to the printer!

![cura03](../img/week05/cura_03.jpg)

### printed the modeled objects using a LulzBot Mini
Krisjanis showed us how to use the Lulzbot Mini using __________________

This is what the printer looks like in action:

<video controls muted height = "480">
    <source src="../../img/week05/lulzbot_01.mp4" type ="video/mp4">
   A video of the Lulzbot Mini in action!
</video>

And here's the model, after the support had been removed with a pair of pliers! As you can see, the ball joint action was preserved in the print, which I was very delighted to find. 

<video controls muted height = "480">
    <source src="../../img/week05/print_01.mp4" type ="video/mp4">
   A video of the printed ball joint in action!
</video>

### scanned a figurine using an Artec Eva 3D-Scanner
I used an [Artec Eva 3D-scanner](https://www.artec3d.com/portable-3d-scanners/artec-eva?utm_source=google&utm_medium=cpc&utm_campaign=2030888541&utm_term=%2Bartec%20%2Beva||kwd-57271721806&utm_content=71494265683||&keyword=%2Bartec%20%2Beva&gclid=CjwKCAjwmKLzBRBeEiwACCVihr7BPxgiTX4g3RIJKZEcer6szcQyAEFgK-phUsXAaAIM9NcQi4i8wBoC5vIQAvD_BwE), which is a 'structured light [handheld] 3D scanner', to scan a figurine that I found in the lab. Not sure why it's here, and not sure about the message that it sends, but it was a conveniently detailed, small model to test the resolution of the scanner.

![waifu01](../img/week05/waifu_01.jpg)

The software and scanner are fairly easy to use - I set these settings:

![artec01](../img/week05/artec_01.jpg)

and you just push the 'play' icon button on the scanner to start. You then focus the scanner on the 'ground' that the object is on, press 'play' again to set the stage, then scan the item by walking around it. You then press the 'stop' icon button to end the scan.

The resulting scan was concerning - I'm not sure why it had difficulty interpreting the model, but it appeared to read it in triplicate:

![artec02](../img/week05/artec_02.jpg)

![artec03](../img/week05/artec_03.jpg)

![artec04](../img/week05/artec_04.jpg)

I used the edit function to automatically remove the base from the model, then exported it as .obj, .stl, and .ply files for editing.

### cleaned up a model mesh using Meshmixer
For some reason, the .obj file crashed when opened, so it is not included in the files below.

I opened the .ply file in Meshmixer to try to clean up the model. This was an exercise in futility, as the model was so incomplete that little in the way of productive 3D printing can come out of it, but I did my best - for instance, the face of the figurine could be extracted to use in other models.

Here's what the file originally looked like:

![meshmixer01](../img/week05/meshmixer_01.jpg)

![meshmixer02](../img/week05/meshmixer_02.jpg)

![meshmixer03](../img/week05/meshmixer_03.jpg)

As you can see in the third photo, the mesh is incomplete - it is not a solid, and exposes the inside. To correct this, I used the Inspector tool to fill the (considerable) amount of holes in the model:

![meshmixer04](../img/week05/meshmixer_04.jpg)

![meshmixer05](../img/week05/meshmixer_05.jpg)

The model is now solid, but I wouldn't use it for anything.

From this, I have a somewhat mixed experience with the 3D scanner - I helped a friend scan his face, and it produced a quite good model of the front of his face, but this model turned out very badly. I hypothesize that this is partially due to the small size of the model - other small things (pens, candy, bracelets) that I tried to scan also did not pick up well.

[This](https://anzibar.gitlab.io/fab-academy/week05.html) is the model of my friend's face, which as you can see, turned out quite nicely!

### Files
[Parametric ball joint file in Fusion 360, .f3d format](../img/week05/ParametricBallJointv0.f3d)

[Parametric ball joint file, solid top, .stl format](../img/week05/ParametricBallJointv3NoHole.stl)

[Parametric ball joint file for the printer, solid top, .gcode format](../img/week05/LM_ParametricBallJointv3NoHole.gcode)

[Parametric ball joint file, with hole, .stl format](../img/week05/ParametricBallJointv2WithHole.stl)

[Parametric ball joint file for the printer, with hole, .gcode format](../img/week05/LM_ParametricBallJointv2WithHole.gcode)

[Exported mesh of the figurine, .ply format](../img/week05/waifu_01.ply)

[Exported mesh of the figurine, .stl format](../img/week05/waifu_01.stl)

[Processed mesh of the figurine, .mix format](../img/week05/waifu_02.mix)

[Processed mesh of the figurine, .stl format](../img/week05/waifu_02.stl)

### printed a test piece for our Lulzbot mini with a 0.8mm nozzle
I downloaded a copy of the [*MICRO* All In One 3D printer test](https://www.thingiverse.com/thing:2975429/) designed by majda107 on thingiverse to test our Lulzbot mini, using the 0.8mm nozzle which was the printer that I used the most.

The test is composed of these sections:

Overall:

![tester_01](../img/week05/tester_01.jpg)

Overhang:

![tester_02](../img/week05/tester_02.jpg)

Bridging:

![tester_03](../img/week05/tester_03.jpg)

and Tolerance:

![tester_04](../img/week05/tester_04.jpg)

Here is the print:

![tester_05](../img/week05/tester_05.jpg)

![tester_06](../img/week05/tester_06.jpg)

I printed it using the standard settings for the 0.25mm layer height profile; however, I printed using 0.2mm layer height and I added a 1-line skirt with a 100mm minimum length, as the test print advised using the 0.2mm height and I wanted to demonstrate how the printer had difficulties with the skirt. 

### evaluated the inconsistencies present in the design rules test

* Skirt: the skirt did not initially stick - it is supposed to be approximately 55x55mm, but as you can see in the overhead shot of the test print, it did not begin to stick to the print bed until halfway through the second side. I would therefore advise using either a generous brim or a generous minimum skirt length, to more likely ensure adhesion to the build plate - in the past, I had issues where very small pieces would not adhere in the beginning, so the print would fail.

* Stringing test: surprisingly little stringing.

* Sharp corners test: not so great. The larger pyramid fared slightly better, but it had noticeable distortion and a certain flatness to the tip, while the smaller one had what looked like a truncated tip. I would recommend rotating a print if possible to avoid having points be reliant on the printer.

* Scale/diameter test: expected: 4mm inner, 8mm outer - actual: 3.6mm inner, 7.81mm outer. Expected: 10mm inner, 14mm outer - actual: 9.58mm inner, 13.90mm outer. In short, expect everything to be a little bit smaller than modeled, and do a test print if possible for printing to scale.

* Overhang test: it survived up to 80 degrees of overhang, which was quite impressive, but there is noticeable distortion along the bottom and slight warping at the top of the print. 

![tester_07](../img/week05/tester_07.jpg)

![tester_08](../img/week05/tester_08.jpg)

* Bridging test: it performed admirably on the bridging test - no stringing or drooping.

![tester_09](../img/week05/tester_09.jpg)

* Tolerance test: I believe it has a tolerance of 0.1mm - at 0.05mm, the walls appear to be bonded together.

![tester_10](../img/week05/tester_10.jpg)


