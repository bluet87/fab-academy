# Week 11: Output Devices
_Group assignment: measure the power consumption of an output device_

_Personal assignment: add an output device to a microcontroller board you've designed, and program it to do something_

This week, I:

## NOTICE: Please refer to Section II: Input/Output Redux for the updated board

### Re-designed a new board with a new input sensor and an output device
Due to unfortunate circumstances surrounding my previous attempt at an input board (one of the traces did not mill correctly, leading to a short circuit), I decided to re-design my board, using a much simpler sensor to avoid having to use the reflow method of soldering again (too complex, prone to errors, difficult to do). 

My new sensor is a [NHQ103B375T10 THERMISTOR NTC 10KOHM 3750K 1206](https://www.digikey.com/product-detail/en/amphenol-advanced-sensors/NHQ103B375T10/235-1109-1-ND/374827). 

[Datasheet](../img/week11/NHQ103B375T10_datasheet.pdf)

#### What is a thermistor?
Thermistors are composed of sintered ceramics consisting of highly sensitive material with consistently reproducible properties of resistance versus temperature... NTC Thermistors are <b>non-linear resistors, which alter their resistance characteristics with temperature</b>. The resistance of NTC will decrease as the temperature increases.[1](https://www.ametherm.com/thermistor/what-is-an-ntc-thermistor)

Thankfully, as a thermistor is a type of resistor, it presents much easier than the pressure sensor when constructing a circuit.

#### Adding the output device, a matrix of WS2812B LEDs
I happened to have an 8x8 matrix of WS2812B LEDs, so I decided to use them as my output device. A WS2812B LED is "a intelligent control LED light source that the control circuit and RGB chip are integrated in a package of 5050 components."

[Datasheet](../img/week11/WS2812B_datasheet.pdf)

What was key for my schematic was that the matrix has three inputs - 5V power, DataIn, Ground. To accomodate for this, I added three pins to my board, and connected the data pin to pin 4.

#### Choosing the microcontroller
For this iteration of the board, I chose to use the [ATTINY1614](https://www.digikey.fi/product-detail/en/microchip-technology/ATTINY1614-SSFR/ATTINY1614-SSFRCT-ND/7354422), under the advice of my instructor. This was in part to add more pins, in part to increase the memory available (16KB vs. the ATTINY412's 4KB), especially since I was planning on programming (at least part of) a matrix of LEDs.

[Datasheet](../img/week11/ATTINY1614_datasheet.pdf)

![pinout01](../img/week11/pinout_01.jpg)

#### Designing the schematic and PCB
At this point, I feel a bit more confident using KiCad, which is really exciting! 

![schematic01](../img/week11/schematic_01.jpg)

The PCB was a bit more messy, and I used .3mm spacing because I thought that the footprint of the ATTINY1614 was smaller, but that turned out to be beneficial as I had difficulty getting the traces to work out without using vias.

![pcb01](../img/week11/pcb_01.jpg)

### Milled the board using the Roland MDX-40A
I first processed the traces and margin using mods (note the difference in <b>cut depth</b> from using the SRM-20):

![mods_01](../img/week11/mods_01.jpg)

![mods_02](../img/week11/mods_02.jpg)

My instructor guided me through using the Roland MDX-40A instead of using the SRM-20 that we used previously, as it was a bit more stable when using a .3mm bit and less prone to breaking them.

![roland01](../img/week11/roland_01.jpg)

![roland02](../img/week11/roland_02.jpg)

I didn't document the process as closely as I should have, but I can describe it to some degree:

* install the drill bit - unlike using hex keys for the SRM-20, we use two wrenches moving in opposing directions (together to loosen, apart to tighten).

* set the capacitive sensor - this is very different, as it uses a special capacitive sensor to detect the Z origin. You first take a cylindrical sensor, located in the front left wall, and center it under the bit. It's really important to put it under the bit, otherwise the calibration will fail and the bit will (probably) break. 

* set the Z origin - once the sensor is in place, close the lid, and select the "Set Z origin using sensor", click Detect, click OK (check the pop-up window statement first), then wait for the magic to happen. 

* set the XY origin - you do this via the Roland VPanel software, very similarly to how you set it for the SRM-20. Move the bit towards the center of the PCB blank using the arrow buttons, then use the 'Set' [XY Origin] from the dropdown in the 'Set Origin Point' section.

* set the rpm - to 7500rpm

* set speed - under the arrow buttons, select the Low Speed option

I then milled the board!

![milled01](../img/week11/milled_01.jpg)

### stuffed the board
I used the following components:

| Quantity | Part                               |
|----------|------------------------------------|
| 1        | 6-pin male headers (surface mount) |
| 2        | 2-pin male headers (through-hole)  |
| 1        | 3-pin male headers (through-hole)  |
| 1        | LED (smd)                          |
| 1        | 499Ω resistor (smd)                |
| 3        | 10kΩ resistors (smd)               |
| 1        | 1μF capacitor (smd)                |
| 1        | Thermistor NTC (smd)               |
| 1        | ATTiny1614 (smd)                   |

and stuffed it:

![milled02](../img/week11/milled_02.jpg)

### failed to program it
After failed attempts to connect my board to my computer, I asked my instructor to help me debug.

![programming01](../img/week11/programming_01.jpg)

Over the course of debugging, we discovered that:

* My USB-UPDI programmer had an incorrectly high-valued resistor, which we replaced.

![debug02](../img/week11/debug_02.jpg)

* My USB-FTDI board was not providing power - this was fixed by re-soldering the vias.

* My input/output board didn't work - this was unfixable. At first we discovered a short, which was eventually fixed by removing and resoldering various parts to find a miniscule, microscopic copper trace short where the machine didn't cleanly mill a trace. We then reversed the LED, replaced the ATTINY1614, and replaced the capacitor, to try to figure out whether there were incorrect values or whether the microcontroller was burnt out. Finally, when all of these were ruled out or fixed, it still didn't connect. As a last effort, my instructor attached jumpers to see if there was inadequate power being supplied to the microcontroller by connecting the FDTI VCC and GND pins directly to the VCC and GND pins of the microcontroller. When this didn't work, we decided it was better for me to redesign the board and re-mill it to try again.

![debug01](../img/week11/debug_01.jpg)

As you can see here, we eventually had a weird configuration as the UPDI connector on my board lifted during debugging. Interesting, but still didn't work unfortunately.

![debug03](../img/week11/debug_03.jpg)

![debug04](../img/week11/debug_04.jpg)

## INPUT/OUTPUT BOARD REDUX: Thermistor to RGB LED
After extensive troubleshooting of my previously designed board with my instructor, it was determined that it did not work for some unknown reason. As a result, I re-designed my board to use an RGB LED for simplicity.

### Re-designed a new board with a new input sensor and an output device

#### Input: Thermistor
I used the same thermistor as for the previous board described above.

#### Output: RGB LED
I used a CLV1A-FKB model RGB smd LED, which "offer[s] highintensity light output and a wide viewing angle in an industry-standard package." The LED is composed of three separate LEDs, Red, Green, and Blue, which are each attached to their own pin and programmed independently. I used the info from the datasheet to determine the ideal resistor values for each pin.

[Datasheet](../img/week11/CLV1A-FKB_datasheet.pdf)

![rgbLED01](../img/week11/rgbLED_01.jpg)

Using the typical and max ratings for forward current, I found the following values: 
R: 60-150ohm
G: 72-90ohm
B: 72-90ohm

This is the package diagram for the LED.

![rgbLED02](../img/week11/rgbLED_02.jpg)

#### Choosing the microcontroller
I kept the same microcontroller, an ATTINY1614, as I needed four pins to cover input/output, and I needed some memory to process the input/output readings/values.

#### Designing the schematic and PCB
Once again, designed in KiCad, except this time the PCB was easier and designed using .4mm spacing, as my instructor advised that the .3mm drills were prone to breakage during the milling process. I also updated the footprints and symbols to use an adapted version of the [Fab Academy library](https://gitlab.fabcloud.org/pub/libraries/kicad) that my instructor kindly cleaned up for us to use.

![schematic02](../img/week11/schematic_02.jpg)

![pcb02](../img/week11/pcb_02.jpg)

### Milled the board using the Roland SRM-20
Same process as with previous weeks.

### stuffed the board
I used the following components:

| Quantity | Part                                        |
|----------|---------------------------------------------|
| 1        | 1μF capacitor (smd)                         |
| 1        | CLV1A-FKB RGB LED (smd)                     |
| 1        | LED (smd)                                   |
| 1        | 1x6 pin male header for FDTI (through-hole) |
| 1        | 1x2 pin male header for UPDI (through-hole) |
| 1        | 150Ω resistor (smd)                         |
| 2        | 90Ω resistor (smd)                          |
| 3        | 10kΩ resistor (smd)                         |
| 1        | 320Ω resistor (smd)                         |
| 1        | Thermistor NTC (smd)                        |
| 1        | ATTiny1614 (smd)                            |

and stuffed it:

![milled_03](../img/week11/milled_03.jpg)

My instructor used soldering paste to apply the RGB LED. First, you use a syringe to lay out solder paste on the desired pads.

![stuffing_01](../img/week11/stuffing_01.jpg)

Then you carefully place the component on top - the solder paste helps adhere it to the board.

![stuffing_02](../img/week11/stuffing_02.jpg)

![stuffing_03](../img/week11/stuffing_03.jpg)

Then, using a heat gun (our lab got a fancy new, temperature/air pressure controlled one!):

![stuffing_04](../img/week11/stuffing_04.jpg)

When heat is applied, the soldering paste melts, adhering the component to the board:

*STROBE WARNING* This was filmed under fluorescent lighting at 50Hz, which causes a slight strobe effect.

<video controls muted height = "480">
    <source src="../img/week11/stuffing_05.mp4" type ="video/mp4">
    Using a heat gun to melt solder paste to attach an LED
</video>

### programmed the board 
So this was weird. At first, we couldn't get the LED to light up at all. After some fiddling with the code, correcting the pin designations, etc. we re-referenced the datasheet and found out that this was a common-anode LED, not a common-cathode. This means that what we thought was Ground was meant to be attached to VCC. As a result, my instructor cut the trace to GND, attached that pin of the LED to VCC, and reconnected GND circumventing the LED. This also meant that we turned off a pin by setting it to 255, and turned on it by setting it to 0.

![debugging_01](../img/week11/debugging_01.jpg)

We then had some very nice code to test the LED that flickered it through different colors by changing the RGB values.

            #define LEDB 10
            #define LEDG 7
            #define LEDR 6

            float R1 = 10000;
            float logR2, R2, T;
            float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;

            int thermistor = 2;
            void setup() {
            Serial.begin(19200);
            pinMode(thermistor, INPUT);
            pinMode(LEDR, OUTPUT);
            pinMode(LEDG, OUTPUT);
            pinMode(LEDB, OUTPUT);
            delay(1000);

            }
            int Vo;
            int r = 0;
            int g = 45;
            int b = 30;

            void loop() {
            Vo = analogRead(thermistor);
            R2 = R1 * (1023.0 / (float)Vo - 1.0);
            logR2 = log(R2);
            T = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));
            T = T - 273.15;
            T = (T * 9.0)/ 5.0 + 32.0; 
            Serial.println(T);

            analogWrite(LEDR,r);
            analogWrite(LEDG,g);
            analogWrite(LEDB,b);
            r--;
            g--;
            b--;

            if(r <=0){
                r=255;
            }

            if(g<=0){
                g=255;
            }

            if(b<=0){
                b=255;
            }
            delay(1000);
            }

![programming02](../img/week11/programming_02.jpg)

This worked great! We then fiddled more with the code to change the behavior of the LED and read the thermistor values to serial monitor, but then something really weird happened. The LED stopped responding to code, and fixed on blue (RGB: 255, 255, 0). This never fixed, despite various attempts at debugging. I even ran code that did not initialize any of the LED-related pins, and it still stayed as blue. 

            void setup() {
            Serial.begin(9600);
            delay(5000);
            }
            void loop() {
            delay(1000);
            Serial.println("6");
            }

![debugging_02](../img/week11/debugging_02.jpg)

![debugging_03](../img/week11/debugging_03.jpg)

I could also not get my board to print to Serial, no matter what I tried. This was also very confusing.

![debugging_04](../img/week11/debugging_04.jpg)

### programmed the board round 2
IT WORKS! Mostly. Turns out, I had my RX/TX pins flipped on my board - turns out that you connect RX to TX, and TX to RX when connecting a microcontroller to a FDTI for serial communication. I resolved this by using the [SoftwareSerial](https://www.arduino.cc/en/Reference/SoftwareSerial) library to digitally flip the pins. After some testing, when the board still didn't work, we tested my FTDI board and found that there was a short - my instructor's board worked for communication, whereas mine did not. This short was rectified with a scalpel.

![serial_01](../img/week11/serial_01.jpg)

My RGB LED also started responding to code - no idea what was wrong, but exciting!

<video controls muted height = "480">
    <source src="../../img/week11/STANDIN.mp4" type ="video/mp4">
    working LED
</video>

I then tested my thermistor, which was giving readings as expected:

![serial_02](../img/week11/serial_02.jpg)

### testing a current draw using a variable power supply

We tested how much current a motorized pump consumed by using a variable power supply, and applied stress to the 12v motor by using it to inflate a tentacle prototype.

<video controls muted height = "480">
    <source src="../../img/final/multimeter_01.mp4" type ="video/mp4">
    Inflating one side
</video>

### Files
[PCB design, .kicad_pcb format](../img/week11/ThermistorRGBLED.kicad_pcb)

[Schematic, .sch format](../img/week11/ThermistorRGBLED.sch)

[Traces, .svg format](../img/week11/ThermistorRGBLED-F_Cu.svg)

[Drills, .svg format](../img/week11/ThermistorRGBLED-Drills.svg)

[Margin, .svg format](../img/week11/ThermistorRGBLED-Dwgs_User.svg)

[Drills, .svg format](../img/week11/ThermistorRGBLED-Drills.svg)

[All KiCad files, .zip format](../img/week11/ThermistorRGBLED.zip)

[RGB LED test code, .ino format](../img/week11/thermistortest.ino)

[Turning off the LED test code (unsuccessful), .ino format](../img/week11/thermistortest2.ino)
