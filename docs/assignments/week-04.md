# Week 04: Electronics Production
_Group assignment: characterize the design rules for your PCB production process_

_Personal assignment: make an in-circuit programmer by milling and stuffing the PCB, test it, then optionally try other PCB processes_

This week, I:

### generated toolpaths for milling using [mods](http://mods.cba.mit.edu)

The example used for milling is a [hello.USB-UPDI.FT230X](http://academy.cba.mit.edu/classes/embedded_programming/index.html#programmers) board.

To process a PNG file for milling a board, first download the traces and the outline:

![traces01](../img/week04/mods_01.png)

![outline01](../img/week04/mods_02.png)

Open [mods](http://mods.cba.mit.edu/), then:

* right-click -> programs -> open server program -> Roland SRM-20 <b>PCB png</b>

* delete websockets

![mods03](../img/week04/mods_03.jpg)

* right-click -> modules -> open server module -> file <b>save</b>

* left-click on <b>file</b>, and then left-click on <b>file (object)</b> to connect the two

![mods04](../img/week04/mods_04.jpg)

#### Traces

* load the image of the traces via <b> select png file</b>

* select <b>mill traces (1/64)</b>

* Set the following settings

![mods05](../img/week04/mods_05.jpg)

| tool diameter   | .3mm       |
|-----------------|------------|
| cut depth       | .1mm       |
| max depth       | .1mm       |
| offset number   | 4          |
| offset stepover | .5         |
| direction       | climb      |
| speed           | 1.0 (mm/s) |
| origin: x       | 0          |
| origin: y       | 0          |
| origin: z       | 0          |

* select <b>calculate</b> to generate the toolpath and save it to your computer

![mods06](../img/week04/mods_06.jpg)

#### Outline

* load the image of the outline via <b> select png file</b>

* select <b>mill outline (1/32)</b>

* Set the following settings

![mods05](../img/week04/mods_07.jpg)

| tool diameter   | .8mm       |
|-----------------|------------|
| cut depth       | .6mm       |
| max depth       | 1.8mm       |
| offset number   | 1          |
| offset stepover | .5         |
| direction       | climb      |
| speed           | 2.0 (mm/s) |
| origin: x       | 0          |
| origin: y       | 0          |
| origin: z       | 0          |

* select <b>calculate</b> to generate the toolpath and save it to your computer

### milled two PCBs using a Roland SRM-20
We used a Roland SRM-20 machine to mill our PCBs. The interface looks like this:

![roland01](../img/week04/roland_01.jpg)

To use the machine, you first insert the bit that you are going to use, and insert it very high up into the collet. 

Using double-sided tape, stick your PCB material (we used FR1) to the plate of the machine, copper-side-up.

Then, using the +-X, +-Y arrow buttons, set the bit to be above the 'origin' point on your board - unlike a laser cutter, the origin is the bottom left of the design, rather than the top left. Switch the Cursor Step from 'Continue' to 'x100' or smaller to refine the movements of the arrow buttons.

Press the X/Y button under 'Set Origin Point' and confirm.

Then, using the -Z arrow button, bring the bit down until it is as low as it will go. Be careful and ensure that the bit remains hovering above the board - if it appears to be approaching the board too closely, re-screw it in at a higher point. Once you have reached the bottom -Z option, press the +Z button with Cursor Step 'x100' 2-3 times to raise the bit. 

Now, carefully unscrew the bit and bring it down until it just barely rests on the board's surface. Screw it in place. Press the Z button under 'Set Origin Point' and confirm. 

Use the +Z button to raise the bit a few millimeters, then close the glass on the machine. In the Roland interface, adjust the Speed to be 50% and the Spindle Speed to be Medium, then press Cut. Select the cut file generated from mods, then confirm to begin the milling process. First mill the traces, then mill the interior/outline - this leads to a more exact PCB.

Once the milling process has begun and you have confirmed that the bit is in the right place and height, you may adjust the Speed to 100% and the Spindle Speed to High.

<video controls muted height = "480">
    <source src="../../img/week04/mill_01.mp4" type ="video/mp4">
    The milling process
</video>

When finished, click 'View' to bring the board forward. Open up the glass, vacuum off the dust, and evaluate your traces. Without re-setting the X/Y origin, replace the bit to use the bit size for cutting, and reset the Z origin using the technique described above. Repeat with the 'interior' cut file.

![mill02](../img/week04/mill_02.jpg)

![mill03](../img/week04/mill_03.jpg)

When all is done, vacuum any remaining dust, remove the PCB, and remove the FR1 board unless someone else plans to use it.

### assembled the PCBs

To assemble a PCB, follow whatever diagram needed for determining what components go where. When working with SMD (Surface Mounted Device) -style components, it is easiest to place them by first melting solder on one pad, then placing the component and 'tacking' one pad in place by re-heating the pre-applied solder. Then, solder remaining contact points as the piece will now stay in place, and finish by re-soldering the first 'tacked' place as that is otherwise a cold joint. 

![hero01](../img/week04/hero_01.jpg)

Solder wick is especially helpful for cleaning up the excess solder, especially when attaching chips.

<s>My second board met an unfortunate fate and had one of the connectors accidentally rip off, removing the traces it was attached to as well. I have attempted to fix it for now by improvising a bit with some solder, but I may re-mill it at another time.</s>

This is the second board, which I used a lot of solder wick for to repair bridging between the legs of the chip. 

![stuff01](../img/week04/stuff_01.jpg)

![hero02](../img/week04/hero_02.jpg)

### tested the boards

I tested the USB to UPDI programmer board (the second one, more complex board) together with Krisjanis, our instructor. He demonstrated how to use Terminal on a Mac to discover the board's designation by first requesting a list of devices, then attaching the programmer and requesting a new list of the devices:

![test01](../img/week04/test_01.jpg)

He then used the Arduino IDE to compile a program, and uploaded it using Terminal. The test was to re-program an LED to blink every 100ms, changed from every 1000ms. It worked!

![test02](../img/week04/test_02.jpg)

Note: we discovered that the plain PCB was too flat to be detected by the USB cable, so we attached a piece of thick paper to the back with double-sided tape (as recommended), which fixed the problem. We also had to slightly modify the PCB by trimming the area near the USB connector (~2x2mm) to allow it to push deeper into the cable. This was done using a metal saw.