# Week 17: Mechanical Design and Machine Design
_Group assignment: design a machine that includes mechanism+actuation+automation, build the mechanical parts and operate it manually, actuate and automate your machine, document the group project and your individual contribution_

_Personal assignment: N/A_

This week, I:

### handled the 3D Printing / CNC milling of the parts for our machine
In coordination with Anssi, I (Jasmine) was able to successfully manufacture the analog part of the machine, through many iterations.

#### Designing the teeth

When designing the large gear, Anssi had to figure out the correct scale for the pulley/gear teeth, which are trapezoidal and require an uncommon gear generator. We used the laser to rapidly test different teeth sizes, and Anssi was able to determine that he needed to use the generated teeth at a 96% scale.

So close, yet so far!

![lasercut01](../img/week17/lasercut_01.jpg)

#### 3D printing

This was the bulk of the manufacturing aspect - we initially chose 3D printing as our manufacturing process because we had a somewhat complex shape for our gears, as one had to have a groove inside for the bearings to rest in and both had to have guide walls on the outside, so we started with designing the big gear.

##### Big gear

Our first printed gear was 'close but not quite', teeth-alignment-wise, so Anssi redesigned the gear and I printed it, using a [LulzBot Mini](https://www.lulzbot.com/store/printers/lulzbot-mini) with a .8mm nozzle (chosen because the other printers with .4mm nozzles were already in use). The first gear had issues with the overhangs from the outer guides on the gear, so this one was printed with auto-generated supports. The path for the gear was wider than necessary, but the teeth fit the pulley very well.

![print01](../img/week17/print_01.jpg)

![print02](../img/week17/print_02.jpg)

![testing02](../img/week17/testing_02.jpg)

##### Small gear and spacers

This was where we found out that our prints using the .4mm nozzle were exhibiting signs of shrinkage, when our spacers (essentially cylinders/thick washers) kept on printing with too-small measurements and not fitting on our screws, and the smaller gear design, meant to be installed on the shaft of the motor, consistently printed too small to fit. They also printed with a mysterious little brim, regardless of changes to the build plate adhesion settings in Cura.

I did not use the .8mm nozzle because it had such poor build plate adhesion for small prints that it consistently failed by not extruding but not sticking to the print bed, creating abstract blobs of plastic rather than useful spacers.

These are some test prints I did to test scaling - I made 3mm versions of the shaft and the spacers at 1x, 1.05x, 1.10x, and 1.10x scale, and had the frustrating experience of the spacers being adequately sized at 1.05x, while the shaft hole didn't fit until scaled to about 1.09x.

![print03](../img/week17/print_03.jpg)

Another test print, where you can see the difficulty that our LulzBot .8mm has with prints sticking to the print bed, resulting in stringiness.
![print04](../img/week17/print_04.jpg)

Krisjanis ended up doing some tests and found that, on our LulzBot Mini 1, scaling to 105% and reducing first layer flow to 90% remedied the printer issues, but the LulzBot Mini 4 needed even less flow. demonstrating the individual nature of different printers, despite being the same model/nozzle/build and using the same software.

I also printed some spacers and the motor gear on the [Ultimaker 2+ Extended](https://ultimaker.com/3d-printers/ultimaker-2-plus), but found similar shrinkage problems.

Going forward, I now know that each individual printer has its own quirks, so for very precise prints like our spacers, one should do some test prints to check for variations in accuracy.

We finally decided to print using the [Ultimaker 2+](https://ultimaker.com/3d-printers/ultimaker-2-plus)'s at Väre's 3D print workshop, as we suspected it would be more precise than the printers at our lab.

We had very successful prints; however, we have opted to use our laser cut gears instead as they are by default more precise (the prints still exhibited signs of very tiny shrinkage). The print files are included below.

#### Laser cutting

A while after discovering the scaling problems, Anssi redesigned the gears yet again so they could be laser cut as different layers and glued together, aligned by alignment holes and tabs that he built into the design. The files are linked below.

![lasercut02](../img/week17/lasercut_02.jpg)

The resulting gears are currently being glued together, but some quick testing has shown them to be very promising, especially for accuracy, unlike the 3D prints.

![lasercut03](../img/week17/lasercut_03.jpg)

#### Assembly

Here are some cross sections of the construction order:

Large gear + plate

![LargeGearAssembly](../img/week17/LargeGearAssembly.jpg)

Motor gear

![MotorGearAssembly](../img/week17/MotorGearAssembly.jpg)

Table

![TableMockup](../img/week17/TableMockup.jpg)

Plate

![PlateAssembly](../img/week17/PlateAssembly.jpg)

After some experimentation with washers and nuts to adjust heights, we were able to fully assemble the machine with minimal excess length on the screws and with secure connections.

#### Complete

Side view

![complete_01](../img/week17/complete_01.jpg)

Top view

![complete_02](../img/week17/complete_02.jpg)

### Files

[Gear 3D printer files, .stl format](../img/week17/Gear_Prints.zip)

[Gear laser files, .dxf format](../img/week17/Gears.zip)

[Table laser files, .dxf format](../img/week17/Table.zip)

### worked on the group assignment
This is documented on our [group assignment page](https://aaltofablab.gitlab.io/fab-academy-2020/machine-building/).