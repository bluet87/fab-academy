# Week 01: Project Management
_Assignment: plan and sketch a potential final project, work through a git tutorial, build a personal site in the class archive describing you and your final project_

This week, I:

#### read the [Fab Charter](http://fab.cba.mit.edu/about/charter/)

Note: I appreciate the spirit behind the Fab Charter - the community-first approach to growth, in particular, is critical to a more societally relevant way of creating, rather than staying within the bounds of the field. A concern that I have sometimes is whether my work benefits others, so having a way to develop a local communal resource is very welcome.

#### set up my [Gitlab account](https://gitlab.com/bluet87)


#### set up git for version control 

* Install [Git](https://git-scm.com/downloads)

* Note that I'm using Git Bash since I'm using Windows (it installs by default when installing git, and the interface can be seen below) - also, when you open it, it may be in the wrong directory. The syntax for changing directories is slightly different from command line - here is an example for navigating to my fab-academy folder:

        cd /c/users/userName/fab-academy

* Add your username and email
        
        git config --global user.name "username"
    
        git config --global user.email "emailaddress"

* Generate your SSH key

        ssh-keygen -t rsa -C "$emailaddress"

* Copy your key
    
        clip <~/.ssh/id_rsa.pub

* Add to your Gitlab account 

        User icon > Settings > SSH Keys (left bar) > paste and > Add key

* Use the following git commands to update your repository:

        git status
        
        git add -A //adds all listed files

        git commit -m 'enterCommentHere'

        git push

* A quick look at what the workflow looks like in Git Bash (Windows):
![Git Workflow](../img/Git01.jpg)

* If your repo is out of sync, correct it with: 

        git pull

#### created and deployed my documentation website using MkDocs and Markdown.

This site was created using [MkDocs](https://www.mkdocs.org/), a site generator "geared towards building project documentation". All pages are written using Markdown. I chose this approach as Markdown is easier to read when coding than HTML, and MkDocs provides a clean template to use Markdown in. 

Markdown looks like this:

![Markdown Code](../img/MkDocs02.jpg)

and renders like this:

![Markdown Code](../img/MkDocs03.jpg)

I had difficulty at first with understanding the important role that tabs and line breaks played in formatting pages, as I was used to other programming languages that ignored whitespaces. Multiple line breaks are needed to start a new line on the page, and tabs are used to adjust indentation.

I also was surprised at the slight difficulty I faced when trying to manipulate images. To present a raw image, it is fairly easy - the code looks like:

    ![AltName](imageFilePath/imageName.jpg)

but when I wanted to, for instance, move the image to the right of the screen, I had to switch to HTML, or use CSS, which I don't prefer. The HTML looked like this:

    <img style="float: right" src="../img/cryptid2.jpg" alt="Profile Image" width="200" />

which I found oddly clunky for an otherwise easy-to-read language. It is nice, however, to be able to use HTML within Markdown, as Markdown is built on HTML, so you can do more complex things if needed.

My theme was [Cinder](https://github.com/chrissimpkins/cinder), from the [MkDocs Wiki](https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes).

To import the theme to my website, I cloned the Cinder repository:

![Cloning Cinder](../img/MkDocs01.jpg)

to my site's home directory (C:\Users\userName\fab-academy). Note: Be sure to clone it to the base directory, instead of putting it in a second folder inside fab-academy - that will break the website.


Use the following to launch a [locally hosted page](http://localhost:8000) that is immediately responsive to changes made to saved files. Very useful for testing changes to your pages.

    mkdocs serve

    CTRL+C to close the server

#### sketched my [final project](../final.md) idea

#### used [ImageMagick](https://imagemagick.org/) to compress and optimize images for web hosting

I used the following command line entry as a catch-all form of optimization; however, more documentation can be found on the website for customizing the way that the image is modified. 

Note: I had to use the 'magick' command instead of the 'convert' path due to an 'Invalid Parameter' error - it appears that Windows may have its own 'convert' command already, so I would have to modify the Path to prioritize ImageMagick. I chose to not take that route, as 'magick' worked just as well and was less trouble.

    magick filePath/fileName.jpg -resize 900x900 -quality 70 -density 72 -units pixelsperinch newPath/newFileName.jpg

The commands are fairly obvious, but the -resize element does not change the scale of the image - instead, if it is given a WxH value, it scales it down so the _maximum_ dimension is the greater of W or H. However, given a single value, it scales the image so the _minimum_ dimension is that value. 

With this command, I was able to compress the initial sketch on my [final project page](../final.md) from 1.81 MB to 65.9 KB - approximately an 27x reduction in size.

### used [HandBrake](https://handbrake.fr/) to compress a video
HandBrake is a free "tool for converting video from nearly any format to a selection of modern, widely supported codecs." It was recommended to me as something similar to ffmpeg with a GUI, and it was less intimidating to navigate.

The UI has a few tabs that you can go through to convert the video. I was able to take a file from 4.51 MB to 789 KB, with some quality loss but not a sizable amount. I used these settings:

![handbrake01](../img/week03/handbrake_01.jpg)

In the future, I plan to learn how to use ffmeg, but for now this works quite well.

Note: I had to rename the file format from .m4v to .mp4 for my browser, Chrome, to support the video. Not sure why HandBrake outputs their 'mp4' format as an .m4v file, but renaming it works just fine.